const functions = require("firebase-functions")
const app = require('./app-function');
// not as clean, but a better endpoint to consume
const api3 = functions.https.onRequest((request, response) => {
    if (!request.path) {
        request.url = `/${request.url}` // prepend '/' to keep query params if any
    }
    return app(request, response)
})

module.exports = {
    //api1,
    //api2,
    rtv: api3
}
