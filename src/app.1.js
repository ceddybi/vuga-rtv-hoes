var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var db = require('./build/db');

var utils = require('./src/auth');
var config = require('./src/config');

var index = require('./src/routes/index');
var api = require('./src/routes/api');
var ads = require('./src/routes/advert');
var artist = require('./src/routes/artist');
var music = require('./src/routes/music');
var radios = require('./src/routes/radios');
var tv = require('./src/routes/tv');
var video = require('./src/routes/video');
var videoGrp = require('./src/routes/videoGrp');
var user = require('./src/routes/user');
var push = require('./src/routes/push');
var dash = require('./src/routes/dash');
var chat = require('./src/routes/chat');


// Newer routers

var apiPOST = require('./sec/api/post');

var debug = require('debug')('server');
var http = require('http');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/post', apiPOST);
app.use('/v1', api);
app.use('/user', user);
app.use('/', index,  utils.basicAuth(config.user['username'], config.user['password']));


app.use('/advert', ads,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/artist', artist,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/music', music,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/radios', radios,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/tv', tv,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/video', video,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/videoGrp', videoGrp,  utils.basicAuth(config.user['username'], config.user['password']));

app.use('/push', push,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/chat', chat,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/dash', dash,  utils.basicAuth(config.user['username'], config.user['password']));

/**app.use('/', routes,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/coupons', coupons, utils.basicAuth(config.user['username'], config.user['password']));
app.use('/on-show', onShow, utils.basicAuth(config.user['username'], config.user['password']));
app.use('/section', section, utils.basicAuth(config.user['username'], config.user['password']));
app.use('/whats-happening', whatsHappening, utils.basicAuth(config.user['username'], config.user['password'])); **/


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



var port = normalizePort(process.env.PORT || '3050');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
