var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('babel-polyfill');
var db = require('./sec/db');
var utils = require('./sec/auth');
var config = require('./sec/config');
var debug = require('debug')('server');
var http = require('http');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, './sec/v1/views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, './sec/v1/public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(awsServerlessExpressMiddleware.eventContext())
app.use(bodyParser.urlencoded({limit: '30mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './sec/v1/public')));


app.options("/*", function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  //res.send(200);
  res.status(200).json({});
});

 
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
      //respond with 200
      //res.send(200);
        res.status(200).json({});
    }
    else {
    //move on
      next();
    }
}); 

// Newer routers for app
var apiPOST = require('./sec/api/post/post-index');
var apiUser = require('./sec/api/user/user-index');

//app.use('/v2/post', apiPOST);
//app.use('/v2/user', apiUser);

// v2 App
var appAPI = require('./sec/api/appAPI');
app.use('/v2', appAPI);


// For Admin 2 UI, content
var adminAPI = require('./sec/api/adminAPI');
app.use('/v2admin', adminAPI);

// For Admin V2
var adminV2 = require('./sec/api/adminV2');
app.use('/v2/admin', adminV2);


//OLD ROUTES
var Oindex = require('./sec/v1/routes/index');
var Oapi = require('./sec/v1/routes/api');
var Oads = require('./sec/v1/routes/advert');
var Oartist = require('./sec/v1/routes/artist');
var Omusic = require('./sec/v1/routes/music');
var Oradios = require('./sec/v1/routes/radios');
var Otv = require('./sec/v1/routes/tv');
var Ovideo = require('./sec/v1/routes/video');
var OvideoGrp = require('./sec/v1/routes/videoGrp');
var Ouser = require('./sec/v1/routes/user');
var Opush = require('./sec/v1/routes/push');
var Odash = require('./sec/v1/routes/dash');
var Ochat = require('./sec/v1/routes/chat');

//Routes
app.use('/v1', Oapi);
app.use('/user', Ouser);
app.use('/push', Opush);
app.use('/', Oindex,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/advert', Oads,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/artist', Oartist,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/music', Omusic,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/radios', Oradios,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/tv', Otv,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/video', Ovideo,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/videoGrp', OvideoGrp,  utils.basicAuth(config.user['username'], config.user['password']));
//app.use('/push', Opush,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/chat', Ochat,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/dash', Odash,  utils.basicAuth(config.user['username'], config.user['password']));


// OLD END==============================


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
/** 
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
**/

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  console.log(err);
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



var port = normalizePort(process.env.PORT || '3050');
app.set('port', port);

/**
 * Create HTTP server.
 */

//var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

//server.listen(port);
//server.on('error', onError);
//server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}


module.exports = app;