var express = require('express');
var router = express.Router();
var CB =  require('./static/cb');
var _ = require('lodash');

var config = require('./static/variables');

var news = require('./news/app-scrapper');
var film = require('./film/app-film');
var live = require('./live/app-live');
var user = require('./user/app-user');
var chat = require('./chat/app-chat');
var ads = require('./ads/app-ads');
var omega = require('./omega/app-omega');
var device = require('./device/app-device');
var userSettings = require('./settings/app-settings');
var slave = require('./slave/app-slave');
var webClient = require('./web/web-app');
var emailClient = require('./email/email-app');


// Film create
router.use('/film',film);


// Serie Creator
router.use('/live',live);

// User routes
router.use('/user',user);

// User settings routes
router.use('/settings',userSettings);

// User routes
router.use('/chat',chat);

// Ad routes
router.use('/ads',ads);


//Omega routes
router.use('/omega',omega);


//Device routes
router.use('/device',device);


//Slave routes
router.use('/slave',slave);

//Slave routes
router.use('/news',news);

//for web client
router.use('/web',webClient);

//for email client
router.use('/email',emailClient);


module.exports = router;