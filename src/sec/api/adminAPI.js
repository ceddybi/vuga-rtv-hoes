var express = require('express');
var router = express.Router();
var CB =  require('./static/cb');
var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   FilmModel = mongoose.model('Film');
var _ = require('lodash');

var config = require('./static/variables');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + config.api_public_folder
});


var filmCreator = require('./film/film-admin-film');
var filmSerie = require('./film/film-admin-serie');
var Tv = require('./live/tv-admin');
var Radio = require('./live/radio-admin');

// Film create
router.use('/film',filmCreator);

// Serie Creator
router.use('/serie',filmSerie);

// Radio
router.use('/radio',Radio);

//TV
router.use('/tv', Tv);

module.exports = router;