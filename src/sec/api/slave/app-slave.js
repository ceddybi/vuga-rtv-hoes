const config = require('../static/variables');
const express = require('express');

const router = express.Router();
const CB = require('../static/cb');

const _ = require('lodash');

const mongoose = require('mongoose');
const User = mongoose.model('UserProfile');
const PwReset = mongoose.model('PwReset');
const RadioModel = mongoose.model('Radio');
const TvModel = mongoose.model('Tv');
const FilmModel = mongoose.model('Film');
const AdCount = mongoose.model('AdCount');
const WatchCount = mongoose.model('WatchCount');
const Ad = mongoose.model('Ad');

const adUtil = require('../ads/ads-utils');
const TRAW = require('../traw/traw');
const chatApp = require('../chat/chat-app');
const adminUtil = require('../admin/admin-utils');

const articleUtil = require('../news/article/article');

function getModel(typo) {
  switch (typo) {
    case 'tv':
      return TvModel;
      break;
    case 'radio':
      return RadioModel;
      break;
    case 'film':
      return FilmModel;
      break;
    default:
  }
}

function updateDayTo(typo, id, userid, duration) {
  // TRAW.noteRaw(typo, id, userid, duration);
}

async function syncSlave(obj, req) {
  let waA;
  let apX;
  let waC;
  let content;
  let ax;
  const typo = obj.typo;
  let data = obj.data;

  try {
    switch (typo) {
      case 'view_article':
        console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ $ article view ${data}`);

        articleUtil.articleAddView(data);
        break;

      // send message
      case 'send_msg':
        const x = JSON.parse(data);
        chatApp.sendMsgCallback(x.userid, x.userto, x.msg, x.chatid, dd => {});
        break;

      // Read user message
      case 'read_msg':
        chatApp.readMsgUser(obj.user.info.userid);
        break;

      case 'ad':
        adUtil.addClick(data, obj.user.info.userid);

        break;

      case 'watch':
        data = JSON.parse(data);
        ax = {
          typo: data.typo,
          cid: data.cid,
          duration: data.duration,
          user: obj.user.info,
          ut: config.getUt(new Date()),
        };

        waC = new WatchCount(ax);

        /**
                content = await getModel(data.typo).findOne({"_id":data.cid}).exec();

                // Save the watch
                if(!_.isEmpty(content)){
                    waC.save(function (er,sa) {
                        if(!er){
                            console.log("Saved watch");

                            // Update Dayto
                            updateDayTo(data.typo, data.cid, obj.user.info.userid, data.duration);
                        }
                        else{
                            console.log("Failed to save watch");
                        }
                    })
                }
                * */

        // Get
        break;

      case 'app':
        data = JSON.parse(data);
        apX = {
          typo: data.typo,
          cid: data.cid,
          duration: req.body.apptime,
          user: obj.user.info,
          ut: config.getUt(new Date()),
        };
        waA = new WatchCount(apX);
        waA.save((er, sa) => {
          if (!er) {
            console.log('Saved App usage');
            updateDayTo('app', data.cid, obj.user.info.userid, req.body.apptime);
          } else {
            console.log('Failed to save App usage');
          }
        });

        break;
      default:
    }
    // return {};
  } catch (error) {
    console.log(error);
  }
}

// Change user info from the app
router.post('/sync', (req, res) => {
  const userid = req.body.userid;
  const data = req.body.data;
  const typo = req.body.typo;

  console.log('SLAVE SYNC -- -START');
  console.log(req.body);
  console.log('SLAVE SYNC -- -END');

  CB.getUserDocByUid(userid, user => {
    // found user
    if (user) {
      syncSlave(
        {
          user,
          typo,
          data,
        },
        req
      );
      // res.status(200).json({status: "200", msg:""});
      adminUtil.addActiveUser();
    } else {
      // Going anonymous
      syncSlave(
        {
          user: config.getAnonymousUser(),
          typo,
          data,
        },
        req
      );
      // res.status(200).json({status: "200", msg:""});
      adminUtil.addActiveUser();

      console.log('User not found Slave node going anonymous');
    }

    // return status
    return res.status(200).json({ status: '200', msg: '' });
  });
});

// Check if user uptodate
router.post('/version', (req, res) => {
  console.log('version update');
  console.log(
    'version update^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
  );
  console.log(req.body);

  const userid = req.body.userid;
  const appversion = +req.body.appversion;

  // check if is uptodate
  if (parseInt(appversion) >= parseInt(config.a_cur)) {
    // Updated app
    console.log('Updated user');
    res.status(200).json({ status: '200', force: false, msg: config.a_changelog });
  } else {
    // Check is it's below minimum
    if (appversion >= config.a_min) {
      console.log('Old user but not force');
      res.json({ status: '400', force: false, msg: config.a_changelog });
    } else {
      // Force the bitch to update
      console.log('Outdated user, force the bitch');
      res.json({ status: '400', force: true, msg: config.a_changelog });
    }
  }
});

// mongorestore --db rtv --collection user back2/user.bson

module.exports = router;
