var express = require('express');
var router = express.Router();
var CB =  require('./static/cb');
var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   FilmModel = mongoose.model('Film'),
   TvModel = mongoose.model('Tv'),
   Ad = mongoose.model('Ad'),
   RadioModel = mongoose.model('Radio');
var _ = require('lodash');

var config = require('./static/variables');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + config.api_public_folder
});







// Film create
router.get('/get/content/:path',function(req, res){

var path = req.params.path;
console.log(path + " get content");

switch(path){
    case "tv":
    var tv = CB.syncGetMany(TvModel, {});
    res.status(200).json(tv);
    break;

    case "radio":
    var radio = CB.syncGetMany(RadioModel, {});
    res.status(200).json(radio);
    break;

    case "film":
    var film = CB.syncGetMany(FilmModel, {});
    res.status(200).json(film);
    break;
}


});


// for ads
router.post('/ad/:path',function(req, res){

// two path edit and create
var path = req.params.path;
var typo = req.body.typo;

console.log(req.body);


if(path ==="create"){
    createAd(req,res);
}

else if(path ==="edit"){
    editAd(req,res);
}



switch(path){
    case "tv":
    var tv = CB.syncGetMany(TvModel, {});
    res.status(200).json(tv);
    break;

    case "radio":
    var radio = CB.syncGetMany(RadioModel, {});
    res.status(200).json(radio);
    break;

    case "film":
    var film = CB.syncGetMany(FilmModel, {});
    res.status(200).json(film);
    break;
}  


});



module.exports = router;