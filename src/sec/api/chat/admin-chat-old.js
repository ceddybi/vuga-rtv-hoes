const express = require('express');
const _ = require('lodash');

const router = express.Router();
const CB = require('../../api/static/cb');

const mailgun = require('mailgun-js')({
  apiKey: 'key-221954a88b1022ec333cc775908f360a',
  domain: 'mail.vugapay.com',
});
// var EmailTemplate = require('email-templates').EmailTemplate;
const path = require('path');
// var wellcome_templateDir = path.join(__dirname, 'email_templates', 'wellcome');
// var wellcome_email = new EmailTemplate(wellcome_templateDir);

let mongoose = require('mongoose'),
  Chat = mongoose.model('Chat'),
  User = mongoose.model('User'),
  Admin = mongoose.model('Admin');

function getOldUsers(callback) {
  const vc = [];
  let c;

  // initialize l
  let l = 1;

  let lastItem = new Date().getTime();
  for (c = 0; c < 6; ++c) {
    console.log(`Found ${  c  } last = ${  lastItem}`);
    const allConvo = CB.syncGetManyLean(
      User,
      {
        firstname: { $ne: '' },
        chat_unix: { $lt: lastItem },
      },
      { chat_unix: -1 },
      1000
    );

    // set the l
    l = allConvo.length - 1;
    console.log(`L =${  l  } size${  allConvo.length}`);

    // Nullify th mf
    if (l < 0) {
      l = 0;
    }

    lastItem = allConvo[l].unix_time;

    // vc = vc.concat(allConvo);
    allConvo.map((d) => {
      vc.push(d);
    });
  }

  callback(vc);
}

function pushFCM(du) {
  const reg = du.id;
  const message = du.msg;
  let key = 'AIzaSyAnrVE3xfJceR2CbEGzsKNrBBaOF1_h5cU';

  const payload = JSON.stringify({
    registration_ids: [reg], // required
    collapse_key: 'your_collapse_key',
    notification: {
      title: 'RwandaTV team',
      body: message,
      sound: 'zing_msg',
      click_action: 'CHAT',
      icon: 'ic_l',
      color: '#4cb5ab',
    },
    data: {},
  });

  const headers = {
    Host: 'fcm.googleapis.com',
    Authorization: `key=${key}`,
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(payload),
  };

  const post_options = {
    host: 'fcm.googleapis.com',
    port: 443,
    path: '/fcm/send',
    method: 'POST',
    headers,
  };

  let post_req = http.request(post_options, (response) => {
    response.setEncoding('utf8');

    let respo;
    response.on('data', function(namedata) {
      respo = String(namedata);
    });
    response.on('end', function() {
      console.log(respo);
      // callback(respo);
    });

    response.on('error', function(err) {
      // respo.error = err;
      // callback(respo);
      console.log('On error ' + err);
    });
  });

  post_req.on('error', (e) => {
    // respo.error = e;
    // callback(respo);
    console.log('On error ' + e);
  });

  post_req.write(payload);
  post_req.end();
}

router.post('/get/convo/all', (req, res) => {
  // updateAdmin();
  console.log('Chat get');
  console.log(req.query);
  let uid = req.query.id;
  let items = {};
  let da = [
    { time: '2  min', msg: 'Sample User message', admin: false },
    { time: '5 min', msg: 'Sample Admin message', admin: true },
    { time: '9 min', msg: 'Sample User message', admin: false },
    { time: '30 sec', msg: 'Sample User message', admin: false },
    { time: '50 sec', msg: 'Sample User message', admin: false },
  ];

  let ua = {
    phone: 'Sample phone',
    useid: 'Sample useid',
    loc: 'Sample Location',
    firstname: 'Sample Firstname',
    lastname: 'Sample Lastname',
    fcmToken: 'Sample fcmToken',
    un: 'Sample username',
    unix_time: '' + new Date().getTime(),
  };

  const vc = [];
  console.log('Fallback chats');
  getOldUsers(function(data) {
    if (data) {
      // got the chats
      items.user = da;
      items.allchats = data;
    } else {
      // no chats
      items.user = da;
      items.allchats = vc;
    }
  });

  items.og = {};
  items.og = ua;
  // console.log(items.og);
  let fff = JSON.stringify(items);

  res.status(200).json({ items });
});

router.post('/get/chats/id', async (req, res) => {
  // console.log("get chats");
  const uid = req.body.userid;
  let userObj;
  let chatsU;

  try {
    userObj = await User.findOne({ userid: uid }).exec();
    chatU = await Chat.find({ userid: uid })
      .sort({ created_at: 1 })
      .lean()
      .exec();

    const u = {
      user: userObj,
      msg: chatU,
    };

    return res.status(200).json(u);
  } catch (error) {
    console.log(error.message);
    // return null
    return res.status(200).json({
      user: {},
      msg: [],
    });
  }
});

router.post('/send/msg', async (req, res) => {
  // Update user's chat_int to zero
  // Update chat_unix to newer dates
  // send a push notification

  // save the new message

  const userid = req.body.userid;
  const msg = req.body.msg;
  let result;

  try {
    result = await User({ userid: userid }).exec();

    if (!result) {
      throw new Error({ message: 'Error getting user' });
    }

    let chat = {
      title: 'Hello',
      typo: 'admin',
      isAdmin: true,
      msg,
      phone: '' + result.phone,
      firstname: result.firstname,
      lastname: result.lastname,
      userid: result.userid,
      unique: result.unique,
      device: result.device,
      unix_time: '' + new Date().getTime(),
    };

    let v = new Chat(chat);

    v.save(function(error, sult) {
      if (error) {
        console.error(error);
        throw error;
      }
    });

    result.chat_at = new Date();
    result.chat_unix = '' + new Date().getTime();

    // Nullfy the counter
    result.chat_str = '0';
    result.chat_int = 0;

    result.active_str = result.active_int + 1 + '';
    result.active_int += 1;
    result.read = true;
    result.save(function(error, sult) {
      if (error) {
        console.error(error);
        throw error;
      }
    });

    return res.status(200).send({ status: '200', msg: 'Message sent' });
  } catch (error) {
    console.log(error.message);
    return res.status(200).send({ status: '400', msg: 'User not found' });
  }
});

module.exports = router;
