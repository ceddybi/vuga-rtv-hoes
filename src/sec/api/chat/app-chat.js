const config = require('../static/variables');
const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
const _ = require('lodash');

const mongoose = require('mongoose');

const UserProfile = mongoose.model('UserProfile');
const Convo = mongoose.model('Convo');
const ChatItem = mongoose.model('ChatItem');

const chatApp = require('./chat-app');

/**
 *
 * ALL GETS MUST RETURN ARRAYS
 *
 */
// GET Chats by convoid
router.post('/chats/get/convoid', (req, res) => {
  res.status(200).send({ status: '200', msg: 'Good' });
});

// Get chats by userid's
// Params userid, userto
router.post('/chats/get/userid', (req, res) => {
  console.log('REAd Chats from Admin');
  console.log(req.body);
  const userid = req.body.userid;
  const userto = req.body.userto;

  chatApp.readMsgCallback(userid, userto, (data) => {
    if (data.status === '200') {
      res.status(200).json(data.data);
    } else {
      res.status(200).json([]);
    }
  });
});

// Get all conversations from user
// Params userid
router.post('/convo/get', (req, res) => {
  res.status(200).send({ status: '200', msg: 'Good' });
});

// Send message
// params userid, userto,msg, chatid.
router.post('/send/msg', (req, res) => {
  console.log('Send message');
  console.log(req.body);

  const userid = req.body.userid;
  const userto = req.body.userto;
  const message = req.body.msg;
  const chatid = req.body.chatid;

  chatApp.sendMsgCallback(userid, userto, message, chatid, (data) => {
    res.status(200).send(data);
  });
});

// Read messages
// Params userid, convid
router.post('/read', (req, res) => {
  res.status(200).send({ status: '200', msg: 'Good' });
});

// Read messages
// Params userid, convid
router.post('/create/admin', (req, res) => {
  chatApp.addAdmin();
  res.status(200).send({ status: '200', msg: 'Good' });
});

router.post('/time', (req, res) => {
  // chatApp.addAdmin();
  const time = new Date();
  res.status(200).send({ time: config.getUt(time) });
});

module.exports = router;
