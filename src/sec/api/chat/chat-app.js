const config = require('../static/variables');
const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
const _ = require('lodash');

const emailUtil = require('../email/email-utils');
const pushUtil = require('../push/push');

const adminWelcomeMsg =
  "Welcome to Rwanda TV your VugaTV, ijwi ryawe. we're always here to help you with anything";

const moment = require('moment');

// var pushUtil = require('../push/push');

const mongoose = require('mongoose');
const UserProfile = mongoose.model('UserProfile');
const UserDevice = mongoose.model('UserDevice');
const ChatItem = mongoose.model('ChatItem');
const Convo = mongoose.model('Convo');

// Admin checker now
async function adminChecker() {
  const admin = await UserProfile.findOne({ 'info.userid': 'vs' });
  if (_.isEmpty(admin)) {
    addAdmin();
  } else {
    console.log('Admin check is perfect');
  }
}
adminChecker();

// pass userid
function addChatCount(x) {
  CB.getUserDocByUid(x, uz => {
    if (uz) {
      const int = parseInt(uz.chatcount.cnum) + 1;
      uz.chatcount.cstr = `${int}`;
      uz.chatcount.cnum = int;

      uz.save((error, result) => {
        if (error) {
          console.log(error);
        } else {
          // saved
          console.log('User Chat count added');
        }
      });
    } else {
      // error user not found
      console.log('User not found for Chat count');
    }
  });
}

// Pass userid
function removeChatCount(x) {
  CB.getUserDocByUid(x, uz => {
    if (uz) {
      // var int = parseInt(uz.chat_int) + 1;
      uz.chatcount.cstr = '0';
      uz.chatcount.cnum = 0;

      uz.save((error, result) => {
        if (error) {
          console.log(error);
        } else {
          // saved
          console.log('User Chat count Removed');
        }
      });
    } else {
      // error user not found
      console.log('User not found for Chat Removed');
    }
  });
}

function addAdmin() {
  const vg = new UserProfile({
    info: {
      userid: 'vs',
      firstname: 'Vuga',
      lastname: 'Team',
      username: 'vuga',
      phone: '80',
      email: 'support@vuga.io',
      // a: config.pw('thebiggerthebill'),

      image_path: 'https://vugapay.com/Images/apple-icon-144x144.png',

      // counter: 100,
    },
  });
  vg.save((err, doc) => {
    // d = doc;
    console.log('Created Admin Account');
  });
}

function updateLastConvo(convoObj, last) {
  convoObj.last = last;
  convoObj.ut = config.getUt(new Date());

  convoObj.save((error, saved) => {
    if (!error) {
      console.log('Updated last object and time');
    } else {
      console.log('Failed to save last Object and time');
    }
  });
}

async function toEmailNotify(msg, userto, userid) {
  let user;

  try {
    // The user sending the message
    user = await UserProfile.findOne({ 'info.userid': userid }).exec();

    // If receiver is Vuga support
    if (_.eq(userto, 'vs')) {
      // Send notice email to admin
      emailUtil.sendEmail({
        msg,
        phone: user.info.phone,
        loc: user.location,
        name: `${user.info.firstname} ${user.info.lastname}`,
      });
    } else {
      // Send email to the user-receiver now
      return pushUtil.sendCustomPush(
        // Query for userto device
        { 'user.userid': userto },
        // Push message
        {
          title: `Message from ${user.info.firstname} ${user.info.lastname}`,
          msg: `${msg.substring(0, 15)}...`,
          sound: 'vuga_zing',
          click_action: 'APP',
          data: {
            typo: 'chat',
            userid,
          },
        }
      );
    }
  } catch (error) {
    console.log(error);
  }
}

async function sendMsg(x, callback) {
  let chat;
  let convoObj;

  try {
    convoObj = await Convo.findOne({ user_id: { $all: [x.userid, x.userto] } }).exec();

    if (!_.isEmpty(convoObj)) {
      // Add count to User/Operator in convo
      if (x.userid === convoObj.operator.userid) {
        addCounterToConvo(true, convoObj);
      } else {
        addCounterToConvo(false, convoObj);
      }

      // Add count to user
      addChatCount(x.userto);

      // save the chat message
      chat = new ChatItem({
        convoid: convoObj.convoid,
        msg: x.msg,
        chatid: x.chatid,
      });
      chat.ut = config.getUt(new Date());
      chat.from.userid = x.userid;
      chat.to.userid = x.userto;

      // UPDATE CONVO LAST MSG
      updateLastConvo(convoObj, { userid: x.userid, msg: x.msg });

      chat.save((error, saved) => {
        if (saved) {
          console.log('Saved chat item');
          // console.log(chat);

          // Email notifier or app Notifier
          toEmailNotify(x.msg, x.userto, x.userid);
          return callback({ status: '200', msg: 'Success' });
        }
        console.log(error);
        return callback({ status: '400', msg: 'Error saving message' });
      });

      // TODO PUSH NOTIFICATION
      // pushMessage(x.userto, x.msg,"New Message");
    } else {
      // console.log("Conversation not found, failed to add first admin message");
      newConvo(x.userid, x.userto, x.msg, x.chatid);
      return callback({ status: '200', msg: 'Success' });
    }
  } catch (error) {
    console.log(error);
    return callback({ status: '400', msg: 'Error sending message' });
  }
}

// Create first admin message
async function firstAdminMsg(userid, convoid) {
  let chat;
  let convoObj;

  try {
    convoObj = await Convo.findOne({ convoid }).exec();

    if (!_.isEmpty(convoObj)) {
      // Add count to User/Operator in convo
      addCounterToConvo(true, convoObj);

      // Add count to user
      addChatCount(userid);

      // save the chat message
      chat = new ChatItem({
        chatid: config.rand(20),
        convoid,
        msg: adminWelcomeMsg,
      });
      chat.ut = config.getUt(new Date());
      chat.from.userid = 'vs';
      chat.save((error, saved) => {});

      // UPDATE CONVO LAST MSG
      updateLastConvo(convoObj, { userid: 'vs', msg: adminWelcomeMsg });

      // TODO PUSH NOTIFICATION
    } else {
      console.log('Conversation not found, failed to add first admin message');
    }
  } catch (error) {
    console.log(error);
  }
}

function addCounterToConvo(isOperator, convoObj) {
  let addCount;

  if (isOperator) {
    addCount = convoObj.operator.count.cnum + 1;
    convoObj.operator.count.cnum = addCount;
    convoObj.operator.count.cstr = `${addCount}`;
  } else {
    addCount = convoObj.creator.count.cnum + 1;
    convoObj.creator.count.cnum = addCount;
    convoObj.creator.count.cstr = `${addCount}`;
  }

  convoObj.save((error, saved) => {});
}

async function readMsgUser(userid) {
  let user;
  if (!_.isEmpty(userid)) {
    try {
      user = await CB.asyncGetUser(userid);
      if (!_.isEmpty(user)) {
        user.chatcount.cnum = 0;
        user.chatcount.cstr = '0';
        user.save((err, saved) => {});
      }
    } catch (error) {
      console.log(error);
    }
  }
}

function removeCounterToConvo(isOperator, convoObj) {
  let addCount;

  // TODO INVERSE, IF=OPERATOR, then=Remove from CREATOR
  if (isOperator) {
    addCount = 0;
    convoObj.creator.count.cnum = addCount;
    convoObj.creator.count.cstr = `${addCount}`;
  } else {
    addCount = 0;
    convoObj.operator.count.cnum = addCount;
    convoObj.operator.count.cstr = `${addCount}`;
  }

  convoObj.save((error, saved) => {});
}

async function readMsgCallback(userid, userto, callback) {
  let messages;
  let convoObj;
  try {
    convoObj = await Convo.findOne({ user_id: { $all: [userid, userto] } }).exec();
    if (!_.isEmpty(convoObj)) {
      // Remove covo unread messages
      // TODO READ/SEEN STATUS
      if (convoObj.operator.userid === userid) {
        removeCounterToConvo(true, convoObj);
      } else {
        removeCounterToConvo(false, convoObj);
      }

      messages = await ChatItem.find({ convoid: convoObj.convoid }).exec();

      return callback({ status: '200', data: messages });
    }

    return callback({ status: '400', data: [], msg: 'No messages found' });
  } catch (error) {
    console.log(error);
  }
}

// Create first Friendly message
async function firstFriendlyMsg(userid, userto, msg, chatid) {
  let chat;
  let convoObj;

  try {
    convoObj = await Convo.findOne({ user_id: { $all: [userid, userto] } }).exec();

    if (!_.isEmpty(convoObj)) {
      // Add count to User/Operator in convo
      addCounterToConvo(true, convoObj);

      // Add count to user
      addChatCount(userto);

      // save the chat message
      chat = new ChatItem({
        chatid,
        convoid: convoObj.convoid,
        msg,
      });
      chat.ut = config.getUt(new Date());
      chat.from.userid = userid;
      chat.to.userid = userto;
      chat.save((error, saved) => {});

      // TODO PUSH NOTIFICATION
    } else {
      console.log('Conversation not found, failed to add first admin message');
    }
  } catch (error) {
    console.log(error);
  }
}

/** From new user, create new convo and add users
 * @param userid * */
async function newConvo(creator, operator, msg, chatid) {
  let convo;
  let userCreator;
  let userOperator;

  const convoid = config.rand(30);

  try {
    userCreator = await CB.asyncGetUser(creator);
    userOperator = await CB.asyncGetUser(operator);
    if (!_.isEmpty(userCreator) && !_.isEmpty(userOperator)) {
      console.log(userCreator);
      console.log(userOperator);

      convo = new Convo({
        user_id: [userCreator.info.userid, userOperator.info.userid],
        convoid,
        creator: {
          userid: userCreator.info.userid,
          firstname: userCreator.info.firstname,
          lastname: userCreator.info.lastname,
          username: userCreator.info.username,
          obj: userCreator,
        },
        operator: {
          userid: userOperator.info.userid,
          firstname: userOperator.info.firstname,
          lastname: userOperator.info.lastname,
          username: userOperator.info.username,
          obj: userOperator,
        },
      });

      convo.save((error, saved) => {
        if (!error) {
          console.log('Convo added');
          // check if convo is start by admin as a welcom message
          if (creator === 'vs') {
            // send first admin message
            firstAdminMsg(operator, convoid);
          } else {
            firstFriendlyMsg(creator, operator, msg, chatid);
          }
        } else {
          console.log('Failed to save Convo');
        }
      });
    } else {
      console.log('Error users empty');
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  readMsgCallback,
  readMsgUser,
  newConvo,
  addAdmin,
  addChatCount,
  removeChatCount,

  /** **
   * @param userid
   * @param userto
   * @param chatid
   * @param msg
   */
  sendMsgCallback(userid, userto, msg, chatid, callback) {
    return sendMsg(
      {
        userid,
        userto,
        msg,
        chatid,
      },
      data => {
        callback(data);
      }
    );
  },

  // check from old conversations and make a new version ov conversation.
  // @params userid
  loopOld(creator, operator) {},
};
