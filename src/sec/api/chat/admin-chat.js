var config = require('../static/variables');
var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var _ = require('lodash');


var mongoose = require('mongoose'),
   UserProfile = mongoose.model('UserProfile'),
   Convo = mongoose.model('Convo'),
   ChatItem = mongoose.model('ChatItem');

   var chatApp = require('./chat-app');


/**
 * 
 * ALL GETS MUST RETURN ARRAYS
 * 
 */
// GET Chats by convoid
router.post('/get/all/convo', function(req, res){

    console.log("GET ADMIN CONVO BY");
    //console.log(req.body);


    var convo = CB.syncGetMany(Convo, {});

    if(!_.isEmpty(convo)){
        //var chats = CB.syncGetMany(ChatItem, {"convoid": convo.convoid});
        console.log("Convo " + convo.length);
        res.status(200).send(convo);
    }
    else{
        console.log("Convo Null");
        res.status(200).send([]);
    }

});


/**
 * **/
// Params userid, userto
router.post('/read/msg', function(req, res){

  console.log("REAd Chats from Admin");
  console.log(req.body);
  var userid = req.body.userid;
  var userto = req.body.userto;


  chatApp.readMsgCallback(userid, userto, function (data){
      if(data.status ==="200"){
          console.log("SENT = "+data.data.length+" from Admin");
          res.status(200).json(data.data);
      }
      else{
          res.status(200).json([]);
      }
  });
   
});



// Send message
// params userid, userto,msg, chatid.
router.post('/send/msg', function(req, res){
    console.log("Send message");
    console.log(req.body);

    var userid = req.body.userid;
    var userto = req.body.userto;
    var message = req.body.msg;
    var chatid = config.rand(20);

    console.log(chatid);
    chatApp.sendMsgCallback(userid, userto, message, chatid, function(data){
      res.status(200).json(data);
    });
   
});



router.post('/time', function(req, res){

   //chatApp.addAdmin();
   var time = new Date();
   res.status(200).send({time:config.getUt(time)});
});








module.exports = router;