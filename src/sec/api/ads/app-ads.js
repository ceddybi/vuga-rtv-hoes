const express = require('express');
const router = express.Router();
const CB = require('../static/cb');
const mongoose = require('mongoose');

const PostModel = mongoose.model('Post');
const FilmModel = mongoose.model('Film');
const TvModel = mongoose.model('Tv');
const Ad = mongoose.model('Ad');
const AdCount = mongoose.model('AdCount');
const UserProfile = mongoose.model('UserProfile');
const AdSettings = mongoose.model('AdSettings');
const RadioModel = mongoose.model('Radio');
const _ = require('lodash');

const config = require('../static/variables');
const homeAway = require('../location/location-home-away');
const locationUtil = require('../location/location-util');

const adUtil = require('./ads-utils');

function checkTarget(cc, userCode) {
  switch (cc) {
    case 'HOME':
      break;
    case 'AWAY':
      break;
    case 'ALL':
      break;
    default:
  }
}

function isHome(cc) {
  return _.includes(homeAway.home, cc);
}

// Get ads
/**
 * @param userid
 */
router.post('/get/settings', async (req, res) => {
  const userid = req.body.userid;
  let adsSettings;
  let user;
  try {
    adsSettings = await AdSettings.findOne({ typo: 'ad2' }).exec();
    console.log(adsSettings);
    user = await UserProfile.findOne({ 'info.userid': userid }).exec();
    return res.status(200).json({ ads: adsSettings, user: CB.returnUser(user) });
  } catch (error) {
    return res.status(200).json({ ads: adsSettings, user: {} });
  }
});

// Get ads
/**
 * @param userid
 */
router.post('/get/featured', async (req, res) => {
  console.log('App get Featured ads');
  const { userid, location } = req.body;
  // const location = req.body.location;

  let user = {};
  let ads;
  let filteredAds;
  let userCode = 'RW';

  try {
    user = await UserProfile.findOne({ 'info.userid': userid }).exec();
    if (!user) {
      console.log('User not found');
      // throw new Error('User not found')
    }
    userCode = await locationUtil.syncGetCountryCode(location, user);
    if (_.isEmpty(userCode)) {
      userCode = 'RW';
    }

    ads = await Ad.find({ isFeatured: true }).exec();

    if (ads) {
      ads.map((ad, index) => {
        // Filter Admob
        if (!_.eq(ad.typo, 'admob')) {
          const ccAd = ad.cc;

          // Filter all
          if (!_.eq(ccAd, 'ALL')) {
            switch (ccAd) {
              case 'HOME':
                // If not home remove ad
                if (!isHome(userCode)) {
                  ads.splice(index, 1);
                }

                break;
              case 'AWAY':
                // If it's homeboy hide the ad
                if (isHome(userCode)) {
                  ads.splice(index, 1);
                }
                break;
              default:
            }
          }
        }
      });
      // Send the ads
      console.log(
        `##################################################################ADS = ${ads.length}`
      );
      return res.json(ads);
    }
    throw new Error('Ads not found');
  } catch (error) {
    console.log(error.message);
    return res.json([]);
  }
});

// for puttin onclick
/**
 * @param userid, ad_id
 */
router.post('/put/onclick', (req, res) => {
  const userid = req.body.userid;
  const aid = req.body.aid;

  adUtil.addClick(aid, userid);

  res.status(200).json({ status: '200', msg: '' });
});

module.exports = router;
