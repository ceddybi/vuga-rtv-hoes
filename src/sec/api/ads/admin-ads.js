const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
var mongoose = require('mongoose'),
  PostModel = mongoose.model('Post'),
  FilmModel = mongoose.model('Film'),
  TvModel = mongoose.model('Tv'),
  RadioModel = mongoose.model('Radio'),
  Ad = mongoose.model('Ad'),
  AdSettings = mongoose.model('AdSettings'),
  RadioModel = mongoose.model('Radio');
const _ = require('lodash');

const config = require('../static/variables');

const multer = require('multer');

const upload = multer({
  dest: __dirname + config.api_public_folder,
});

// Check Ad settings
async function checkAdSettings() {
  let adx;
  const adSet = await AdSettings.findOne({ typo: 'ad2' }).exec();
  if (adSet) {
    console.log('Ad settings perfect');
  } else {
    adx = new AdSettings({
      //  __v: 0,
      // _id: '596d36c3b028163452cbee78',
      pushback: { breaks: 18, duration: 0, count: 3, active: false },
      breaks: 0,
      count: 0,
      isVideo: false,
      isBanner: true,
      isAudio: false,
      active: true,
      typo: 'ad2',
      sid: '',
    });
    adx.save((error, saved) => {
      console.log('Ad settings created');
    });
  }
}
checkAdSettings();

function getContentModel(typo) {
  if (typo === 'film') {
    return FilmModel;
  } else if (typo === 'tv') {
    return TvModel;
  } else if (typo === 'radio') {
    return RadioModel;
  }
}

function getContentObject(typo, obj) {
  if (typo === 'film') {
    return {
      title: obj.meta.name,
      id: obj._id,
    };
  } else if (typo === 'tv') {
    return {
      title: obj.title,
      id: obj._id,
    };
  } else if (typo === 'radio') {
    return {
      title: obj.title,
      id: obj._id,
    };
  }
}

async function createAd(req, res) {
  let contentObj;
  const typo = req.body.typo;

  const ad = new Ad({
    aid: config.rand(20),
    veri: 1,
    typo,
    title: req.body.name,
    cc: req.body.cc,
    isFeatured: req.body.isFeatured,
    featured: req.body.featured,
    des: req.body.des,
    slider: req.body.slider,
    ut: config.getUt(new Date()),
  });

  switch (typo) {
    case 'pushback':
      ad.pushback.duration = req.body.pb_duration;
      ad.pushback.isAdmob = req.body.pb_is_admob;
      ad.pushback.banner_bottom = req.body.pb_banner_bottom;
      ad.pushback.banner_top = req.body.pb_banner_top;
      ad.pushback.link = req.body.pb_link;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;

    case 'admob':
      ad.admob.isVideo = req.body.admob_isVideo;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });

      break;

    case 'content':
      var contentID = req.body.content_id;
      var contentTypo = req.body.content_typo;
      var contentModel;

      if (contentTypo === 'film') {
        contentModel = FilmModel;
      } else if (contentTypo === 'tv') {
        contentModel = TvModel;
      } else if (contentTypo === 'radio') {
        contentModel = RadioModel;
      }

      contentObj = await contentModel.findOne({ _id: contentID }).exec();

      ad.content.typo = contentTypo;
      ad.content.id = contentID;
      ad.content.obj = getContentObject(contentTypo, contentObj);
      ad.content.banner_url_rec = req.body.content_banner_url_rec;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });

      break;

    case 'local':
      ad.local.link = req.body.local_link;
      ad.local.obj = {};
      ad.local.banner_url_rec = req.body.local_banner_url_rec;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;

    case 'video':
      ad.video.isAdmob = req.body.isVideoAdmob;
      ad.video.link = req.body.video_link;
      ad.video.url = req.body.video_url;
      // ad.video.obj= {};
      // ad.video.banner_url_rec= req.body.local_banner_url_rec;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;

    case 'audio':
      ad.audio.link = req.body.audio_link;
      ad.audio.url = req.body.audio_url;
      ad.audio.banner_url_rec = req.body.audio_banner_url_rec;
      ad.audio.banner_url_sq = req.body.audio_banner_url_sq;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;

    case 'premium':
      ad.premium.video_url = req.body.pre_video_url;
      ad.premium.audio_url = req.body.pre_audio_url;
      ad.premium.banner_url_rec = req.body.premium_banner_url_rec;
      ad.premium.banner_url_sq = req.body.premium_banner_url_sq;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;
    default:
  }
}

async function editAd(req, res) {
  const typo = req.body.typo;

  const ad = await Ad.findOne({ _id: req.body._id }).exec();

  ad.slider = req.body.slider;
  ad.cc = req.body.cc;
  ad.title = req.body.name;
  ad.des = req.body.des;
  ad.isFeatured = req.body.isFeatured;
  ad.featured = req.body.featured;
  ad.ut = config.getUt(new Date());

  switch (typo) {
    case 'pushback':
      ad.pushback.duration = req.body.pb_duration;
      ad.pushback.isAdmob = req.body.pb_is_admob;
      ad.pushback.banner_bottom = req.body.pb_banner_bottom;
      ad.pushback.banner_top = req.body.pb_banner_top;
      ad.pushback.link = req.body.pb_link;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });

      break;
    case 'admob':
      ad.admob.isVideo = req.body.admob_isVideo;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });

      break;

    case 'content':
      ad.content.banner_url_rec = req.body.content_banner_url_rec;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });

      break;

    case 'local':
      ad.local.link = req.body.local_link;
      ad.local.banner_url_rec = req.body.local_banner_url_rec;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;
    case 'video':
      ad.video.isAdmob = req.body.isVideoAdmob;
      ad.video.link = req.body.video_link;
      ad.video.url = req.body.video_url;
      // ad.video.obj= {};
      // ad.video.banner_url_rec= req.body.local_banner_url_rec;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;

    case 'audio':
      ad.audio.link = req.body.audio_link;
      ad.audio.url = req.body.audio_url;
      ad.audio.banner_url_rec = req.body.audio_banner_url_rec;
      ad.audio.banner_url_sq = req.body.audio_banner_url_sq;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;

    case 'premium':
      ad.premium.video_url = req.body.pre_video_url;
      ad.premium.audio_url = req.body.pre_audio_url;
      ad.premium.banner_url_rec = req.body.premium_banner_url_rec;
      ad.premium.banner_url_sq = req.body.premium_banner_url_sq;
      ad.save((error, saved) => {
        if (!error) {
          res.status(200).json({ status: '200', msg: 'Saved' });
        } else {
          res.status(200).json({ status: '400', msg: 'Failed to save Advert' });
        }
      });
      break;
  }
}

// get Ad settings
router.post('/get/settings', async (req, res) => {
  let d;
  console.log('GET ADS Settings');
  let adsT;

  try {
    adsT = await AdSettings.findOne({ typo: 'ad2' }).exec();
    d = adsT;
    return res.status(200).json({ status: '200', msg: '', data: adsT });
  } catch (error) {
    return res.status(400).json({ status: '400', msg: error });
  }
});

// get Ad settings
router.post('/change/settings', (req, res) => {
  console.log('GET ADS Settings');

  console.log(req.body);
  AdSettings.findOne({ typo: 'ad' }, {}, (error, adsT) => {
    if (adsT) {
      console.log(adsT);
      const d = req.body;
      ad = {
        active: d.active,
        isAudio: d.isAudio,
        isBanner: d.isBanner,
        isVideo: d.isVideo,
        count: d.count,
        breaks: d.breaks,

        pushback: {
          active: d.pb_active,
          count: d.pb_count,
          breaks: d.pb_breaks,
        },
      };
      AdSettings.update({ _id: adsT._id }, { $set: ad }, (error, saved) => {
        console.log('saved Ad');
        console.log(saved);
      });
      res.status(200).json({ status: '200', msg: '', data: adsT });
    } else {
      res.status(200).json({ status: '400', msg: 'Not found', data: {} });
    }
  });
});

// get All ads
router.post('/get/all', async (req, res) => {
  let adsT;
  console.log('GET ALL ADS');
  try {
    adsT = await Ad.find({}).exec();
    return res.status(200).json(adsT);
  } catch (error) {
    return res.status(400).json({});
  }
});

// get All ads
router.post('/get/id', async (req, res) => {
  console.log('GET AD by id');
  console.log(req.body);
  const id = req.body.id;

  const adsT = await Ad.findOne({ _id: id }).exec();

  console.log(adsT);

  return res.status(200).json(adsT);
});

// get All ads
router.post('/get/featured', async (req, res) => {
  let premium;
  let video;
  let audio;
  let radio;
  let tv;
  let film;
  console.log('GET Featured');
  console.log(req.body);

  try {
    film = await Ad.find({
      slider: 'film',
      isFeatured: true,
      typo: { $nin: ['video', 'audio', 'premium'] },
    }).exec();

    tv = await Ad.find({
      slider: 'tv',
      isFeatured: true,
      typo: { $nin: ['video', 'audio', 'premium'] },
    }).exec();

    radio = await Ad.find({
      slider: 'radio',
      isFeatured: true,
      typo: { $nin: ['video', 'audio', 'premium'] },
    }).exec();

    video = await Ad.find({ typo: 'video', isFeatured: true }).exec();

    audio = await Ad.find({ typo: 'audio', isFeatured: true }).exec();

    premium = await Ad.find({ typo: 'premium', isFeatured: true }).exec();

    res.status(200).json({
      film,
      tv,
      radio,
      audio,
      video,
      premium,
    });
  } catch (error) {
    console.log(error);
    res.status(400).json(error);
  }
});

// get All content for ad creation
router.get('/get/content/:path', async (req, res) => {
  let film;
  let radio;
  let tv;
  const path = req.params.path;
  console.log(`${path} get content`);

  try {
    switch (path) {
      case 'tv':
        tv = await TvModel.find({}).exec();
        return res.status(200).json(tv);
        break;

      case 'radio':
        radio = await RadioModel.find({}).exec();
        return res.status(200).json(radio);
        break;

      case 'film':
        film = await FilmModel.find().exec();
        return res.status(200).json(film);
        break;
      default:
        return res.status(400).json([]);
        break;
    }
  } catch (error) {
    // Error right now
    console.log(error);
    res.status(400).json({});
  }
});

// for ads
router.post('/:path', (req, res) => {
  // two path edit and create
  const path = req.params.path;
  const typo = req.body.typo;

  console.log(req.body);

  if (path === 'create') {
    createAd(req, res);
  } else if (path === 'edit') {
    editAd(req, res);
  }
});

module.exports = router;
