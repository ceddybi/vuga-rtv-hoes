const CB = require('../static/cb');
const _ = require('lodash');
const config = require('../static/variables');
let mongoose = require('mongoose'),
  PostModel = mongoose.model('Post'),
  FilmModel = mongoose.model('Film'),
  TvModel = mongoose.model('Tv'),
  Ad = mongoose.model('Ad'),
  AdCount = mongoose.model('AdCount'),
  UserProfile = mongoose.model('UserProfile'),
  AdSettings = mongoose.model('AdSettings'),
  RadioModel = mongoose.model('Radio');

async function addClick(aid, userid) {
 let adObj;
 let userObj;

  
  try {

     adObj = await Ad.findOne({ _id: aid }).exec();
     userObj = await UserProfile.findOne({ 'info.userid': aid }).exec();
      if (!_.isEmpty(adObj)) {
          // Save summary on ad Object
          const c = adObj.clicks.cnum + 1;
          adObj.clicks.cnum = c;
          adObj.clicks.cstr = `${  c}`;

          adObj.save((err, saved) => {
            if (!err) {
              console.log('Ads count success added');
              // TODO Add click
              // adUtil.addClick(aid, userid);
            } else {
              console.log('Ads cout not added');
            }
          });

          const adC = new AdCount({
            aid,
            typo: adObj.typo,
            user: userObj,
            ut: config.getUt(new Date()),
          });

          adC.save((err, saved) => {
            if (!err) {
              console.log('Ad count added');
            } else {
              console.log('Ad count failed to add');
            }
          });
        } else {
          console.log('Ad not found');
        }


  }
  catch(error) {
    console.log(error.message);
  }
  
}

module.exports = {
  addClick,
};
