var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var postImage = require('./post-image');
var postVideo = require('./post-video');
var postAudio = require('./post-audio');
var lvcs = require('./a-like-view-comment');
var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post');
var _ = require('lodash');

var config = require('../static/variables');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + config.api_public_folder
});


/**
REQUESTS =  
-Update
-Fetch/GET
-Create
-Remove

-----------------------------------------------Sync-Posts----
-Sync
// Posts, news, dato,
[
  {
  parent: "", => post, dato,news  
  typo: "typo",
    data: [
      {id: "", version: ""}
    ]
  }      
]

--- Response ---- 
-status: "" 200=yes, 555=no-updates, 400=error
-deleted: ["id"]
-update: [Objects]

------------------------------------------------Sync Posts-----



-----Get Posts---------------

get-from-date-and
  -null
  -below
  -above

------------------------------


-----------------------------------------------------------FEED CREATOR-------------------
posts
- Robotic
- top posts
- Ads

- New posts from followers
- Comments on posts
- Likes
- Tags (FUTURE)

-RULES




 * 
 */



router.post('/create', upload.any(), function(req, res){

 
            switch(req.body.typo){

              case 'image':
              postImage.createBase64ImgDummy(req,res);
              return;

              case 'video':
              postVideo.createVideo(req,res);
              return;

              case 'audio':
              postAudio.createAudio(req,res);
              return;

              default: 
              postImage.createImg();
              return;

            };

});

router.post('/update',function(req, res) {

            switch(req.body.typo){
              
              case 'image':
              postImage.updateImg(req,res);
              return;

              default: 
              postImage.createImg();
              return;

            };

});


/**@body.userid = all posts by user **/
router.post('/get/userid',function(req, res) {
   var userid = req.body.userid;

   //Check if userid is not null
   if(!userid){
     res.status(400).send({status:"400", msg:"Not found"});
   }
   else{
        CB.getOneOrMany(PostModel,"many",{"user.userid": userid},  req, res);
   }    
   

});

/** @body.pid = post id **/
router.post('/get/one',function(req, res) {
       
   var pid = req.body.pid;    
   //Check if userid is not null
   if(!userid){
     res.status(400).send({status:"400", msg:"Not found"});
   }
   else{
        CB.getOneOrMany(PostModel,"one",{"meta.pid": pid},  req, res);
   } 

});


// Delete post
// @pid 
router.post('/delete', function(req, res){

  // Check if @pid is null
    if(!req.body.pid){
       res.status(400).send({status:"400", msg:"Not found"});
    }
    else{
          CB.deleteOne(PostModel, {"meta.pid": pid}, {"meta.pid": pid}, function(good){
                if(good.status === "200"){
                  res.status(200).send({status:"200", msg:"Success"});
                  //console.log("Okay")
                }
                else {
                  res.status(400).send({status:"400", msg:"No data found", data: {}});
                }
          });
    }

});


router.post('/dumb',function(req, res){
       console.log("dumbass here ");
       CB.b();
});


router.post('/lvcs',function(req, res){

       var x = lvcs.getLVCSDump({}, {});
       //console.log(x);

       CB.findMany(PostModel, {"user.userid": "ceddybi"}, function(data){
            if(data.status === "200"){

              var that = [];
              data.data.map(function(obj){
                 obj.like.like_data = x.like;
                 obj.comment.comment_data = x.comment;
                 obj.share.share_data = x.share;
                 obj.view.view_data = x.view;
                 that.push(obj);
              });
              

              // Send that shit
              
              
              console.log("Size = "+that.length);
              res.status(200).send(that);

            }else{
              res.status(200).send({status:"400", msg: "Error post not found"})
            }
       });
       
       
       
});

router.post('/upload/image',upload.any(), function(req, res){
       console.log(req.files);
        console.log(req.body);
       //CB.b();
});



module.exports = router;