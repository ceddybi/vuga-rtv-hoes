/**
 * 
 * 
 * HAS THE FOLLOWING METHODS
 * 1- UPDATE-AUDIO POST
 * 2- CREATE-AUDIO POST
 */



var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post');

var CB =  require('../static/cb');
var config = require('../static/variables');
var uploader = require('../static/uploader');
var d = new Date();
var currentTime = d.getTime();
var fs = require('fs');



var exports = module.exports = {



    /*** First verbs are static not for Bots, DONOT RETURN ANYTHINK */
     
      //@pid, and @text, @private
      // TODO, place, country, tag
      updateAudio: function (req, res){

          var text = req.body.text;
          var pid = req.body.pid;

          //Future
          var privacy = (req.body.private || false);

          var todo = {
              "meta.pid": pid, 
          };

          var dataToSave = {$set: {"content.audio.text": text} };
          

          // Find, then update
          CB.update(PostModel,todo, dataToSave,{}, function(good){
                    if(good.status === "200"){
                            // send error now failed to
                            res.status(200).send(good);
                        }
                        else {
                            // send error now failed to
                            res.status(400).send(good);
                        }
         });
     },



      // Audio can only be uploaded using as Binary Multipart/Form-data http
      // Parameters
      // @userid, file_name, file_data, text
      // TODO place, tag
     createAudio: function (req, res){
        //console.log("Good");
        //console.log(req.body);

        var userid = req.body.userid;
        var text = req.body.text;
        var file_name = req.body.file_name;
        var splittedName = file_name.split('.');
        var audio_name = userid+"_"+ config.rand(15) + '.' + splittedName[splittedName.length - 1];

        
        //Model data to save
        var dataToSave = {
           typo: "audio", 
           user: {
               userid: userid
           },
           meta: {
               pid: config.pid,
           },
           content: {
               audio: {
                   url: config.aws_host+config.aws_bucket+"/"+config.aws_audio_folder+ audio_name,
                   text: text,
               }
           },

        }

        // save video and upload
        req.files.map(function (value, index) {

                if (value.fieldname == 'file_data') {
                    var splittedName = value.originalname.split('.');
                    var audio_name = 'audio_'+currentTime + '.' + splittedName[splittedName.length - 1];
                    audio_name.replace(/ /g,"%20");
                    audio_path = config.api_public_video_folder + audio_name;
                    //console.log(value.path);

                    //add new url to data
                    dataToSave.content.audio.url = config.aws_host+config.aws_bucket+"/"+config.aws_audio_folder+ audio_name;
                    
                    //Rename stuff
                    fs.rename(value.path, audio_path);  

                    // Upload that shit!        
                    uploader.saveUniversalFile(audio_path, audio_name, splittedName[splittedName.length - 1], config.aws_audio_folder+ audio_name); 
                    //uploader.S3saveVideo(audio_path, video_name, splittedName[splittedName.length - 1], 'video');


                }

        });


        // Save Post Video
        CB.create(PostModel, dataToSave, function(good){
                if(good.status === "200"){
                    
                        // send error now failed to
                        res.status(200).send({data:good.data,  status:"200", msg:"Post created"});
                    }
                    else {
                        // send error now failed to
                        res.status(400).send({status:"400", msg:"Server Failed to save data", data:{}});
                    }
        });


     },



     /*** Static Bots */
}