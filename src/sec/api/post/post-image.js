/**
 * 
 * 
 * HAS THE FOLLOWING METHODS
 * 1- UPDATE-IMAGE POST
 * 2- CREATE-IMAGE POST
 */



var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   PostComment = mongoose.model('Postcomment'),
   PostView = mongoose.model('Postview'),
   PostShare = mongoose.model('Postshare'),
   PostLike = mongoose.model('Postlike');

var CB =  require('../static/cb');
var config = require('../static/variables');
var uploader = require('../static/uploader');




var exports = module.exports = {



    /*** First verbs are static not for Bots, DONOT RETURN ANYTHINK */
     
      //@pid, and @text, @private
      // TODO, place, country, tag
      updateImg: function (req, res){

          var text = req.body.text;
          var pid = req.body.pid;

          //Future
          var privacy = (req.body.private || false);

          var todo = {
              "meta.pid": pid, 
          };

          var dataToSave = {$set: {"content.image.text": text} };
          

          // Find, then update
          CB.update(PostModel,todo, dataToSave,{}, function(good){
                    if(good.status === "200"){
                            // send error now failed to
                            res.status(200).send(good);
                        }
                        else {
                            // send error now failed to
                            res.status(400).send(good);
                        }
         });
     },



      // Images can only be uploaded using base64 
      // Parameters
      // @userid, file_name, file_data, text
      // TODO place, tag
     createBase64Img: function (req, res){
        console.log("Good");

        var userid = req.body.userid;
        var text = req.body.text;
        var file_name = req.body.file_name;
        var file_data = req.body.file_data;
        
        var splittedName = file_name.split('.');
        var image_name = userid+"_"+ config.rand(15) + '.' + splittedName[splittedName.length - 1];
        //var local_image_folder = config.api_public_image_folder;

        
        // Model data to save
        var dataToSave = {
           user: {
               userid: userid
           },
           meta: {
               pid: config.pid,
           },
           typo: "image",
           content: {
               image: {
                   url: config.aws_host+config.aws_bucket+"/"+config.aws_image_folder+ image_name,
                   text: text,
               }
           },

        }

        // Write image data to server and proceed to next function
        uploader.saveBase64Image(file_data, image_name, function(err, fil){

                     if(err){
                      console.log("Failed to saved file");
                      console.log(err);

                       // send error now failed to
                       res.status(400).send({status:"400", msg:"Server Failed to save file"});
                     }
                     else{

                            // upload image to s3 and save data
                            uploader.S3saveImage(fil, image_name);


                                        CB.create(PostModel, dataToSave, function(good){
                                                if(good.status === "200"){
                                                    
                                                     // send error now failed to
                                                      res.status(200).send({data:good.data,  status:"200", msg:"Post created"});
                                                    }
                                                    else {
                                                     // send error now failed to
                                                      res.status(400).send({status:"400", msg:"Server Failed to save data", data:{}});
                                                    }
                                        });
                     }

        });


     },

      createBase64ImgDummy: function (req, res){
        console.log("Good");

        var userid = req.body.userid;
        var text = req.body.text;
        var file_name = req.body.file_name;
        var file_data = req.body.file_data;
        
        var splittedName = file_name.split('.');
        var image_name = userid+"_"+ config.rand(15) + '.' + splittedName[splittedName.length - 1];
        //var local_image_folder = config.api_public_image_folder;

        
        // Model data to save

        var p = config.rand(15);

        var dataToSave = {
           user: {
               userid: "ceddybi",
               firstname: "Ceddy",
               lastname: "Muhoza",
           },
           meta: {
               pid: p,
           },
           typo: "image",
           content: {
               image: {
                   url: config.aws_host+config.aws_bucket+"/"+config.aws_image_folder+ image_name,
                   text: text,
               }
           },

        }

        var sampledata = {
            
                user:{
                    userid: "ceddybi",
                    firstname: "Ceddy",
                    lastname: "Muhoza",
                },
                meta: {
                    pid: p
                }
        }

        CB.create(PostComment, sampledata, function(d){});
        CB.create(PostLike, sampledata, function(d){});
        CB.create(PostShare, sampledata, function(d){});
        CB.create(PostView, sampledata, function(d){});

        // Write image data to server and proceed to next function
        uploader.saveBase64Image(file_data, image_name, function(err, fil){

                     if(err){
                      console.log("Failed to saved file");
                      console.log(err);

                       // send error now failed to
                       res.status(400).send({status:"400", msg:"Server Failed to save file"});
                     }
                     else{

                            // upload image to s3 and save data
                            uploader.S3saveImage(fil, image_name);


                                        CB.create(PostModel, dataToSave, function(good){
                                                if(good.status === "200"){
                                                    
                                                     // send error now failed to
                                                      res.status(200).send({data:good.data,  status:"200", msg:"Post created"});
                                                    }
                                                    else {
                                                     // send error now failed to
                                                      res.status(400).send({status:"400", msg:"Server Failed to save data", data:{}});
                                                    }
                                        });
                     }

        });


     },


     createImg: function (req, res){
        //console.log("Good");
        //console.log(req.body);

        var userid = req.body.userid;
        var text = req.body.text;
        var file_name = req.body.file_name;
        var splittedName = file_name.split('.');
        var image_name = userid+"_"+ config.rand(15) + '.' + splittedName[splittedName.length - 1];

        
        //Model data to save
        var dataToSave = {
           user: {
               userid: userid
           },
           meta: {
               pid: config.rand(10),
           },
           typo: "image",
           content: {
               image: {
                   url: config.aws_host+config.aws_bucket+"/"+config.aws_image_folder+ image_name,
                   text: text,
               }
           },

        }

        // save video and upload
        req.files.map(function (value, index) {

                if (value.fieldname == 'file_data') {
                    var splittedName = value.originalname.split('.');
                    var image_name = userid+'_image_'+currentTime + '.' + splittedName[splittedName.length - 1];
                    image_name.replace(/ /g,"%20");
                    image_path = config.api_public_image_folder + image_name;
                    //console.log(value.path);

                    //add new url to data
                    dataToSave.content.image.url = config.aws_host+config.aws_bucket+"/"+config.aws_image_folder+ image_name;
                    
                    //Rename stuff
                    fs.rename(value.path, image_path);  

                    // Upload that shit!        
                    uploader.saveUniversalFile(image_path, image_name, splittedName[splittedName.length - 1], config.aws_image_folder+ image_name); 
                    //uploader.S3saveVideo(image_path, video_name, splittedName[splittedName.length - 1], 'video');


                }

        });


        // Save Post Video
        CB.create(PostModel, dataToSave, function(good){
                if(good.status === "200"){
                    
                        // send error now failed to
                        res.status(200).send({data:good.data,  status:"200", msg:"Post created"});
                    }
                    else {
                        // send error now failed to
                        res.status(400).send({status:"400", msg:"Server Failed to save data", data:{}});
                    }
        });


     },



     /*** Static Bots */
}