var CB =  require('../static/cb');

var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   PostComment = mongoose.model('Postcomment'),
   PostView = mongoose.model('Postview'),
   PostShare = mongoose.model('Postshare'),
   PostLike = mongoose.model('Postlike');



   const getArray = async (model, search) => {
         var data = await CB.findMany(PostLike, search, function(data){});
         if(data.status === "200"){
             return data.data;
         }
         else{
             return [];
         }
    };


    const getA = async (model, search) => {
         var data;

        return new Promise((resolve, reject) => {
                CB.findMany(PostLike, search, (body) => {
                if (body.status === "400") {
                    reject(body); return;
                }
                resolve(body.data);
                });
            });

        };


function syncGetArray(model, search){
        var ret;
        setTimeout(function(){
            // ret = "hello";
        CB.findMany(model, search, (body) => {
            if(body.status === "200"){
                ret = body.data
            }
            else{
                ret = []
            }
            
        })

        },0);
        while(ret === undefined) {
            require('deasync').sleep(0);
        }
        // returns hello with sleep; undefined without
        return ret;    
};


var exports = module.exports = {

   getLVCSDump: function() {
        var search = {
           "user.userid": "ceddybi",
           "meta.pid": "nast"
        }

       return {
            like: syncGetArray(PostComment, search),
            comment: syncGetArray(PostLike, search),
            view: syncGetArray(PostShare, search), 
            share: syncGetArray(PostView, search)
       };
    },
}