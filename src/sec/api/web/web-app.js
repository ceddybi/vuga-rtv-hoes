/**
 * Created by vuga on 9/14/17.
 */

/**
 * - GET Slider items
 * - GET category items
 * - Film items
 * */



var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var mongoose = require('mongoose'),
    PostModel = mongoose.model('Post'),
    FilmModel = mongoose.model('Film'),
TvModel = mongoose.model('Tv'),
Ad = mongoose.model('Ad'),
AdCount = mongoose.model('AdCount'),
UserProfile = mongoose.model('UserProfile'),
AdSettings = mongoose.model('AdSettings'),
NewsArt = mongoose.model('newsArt'),
RadioModel = mongoose.model('Radio');
var _ = require('lodash');

var config = require('../static/variables');
var homeAway = require('../location/location-home-away');
var locationUtil = require('../location/location-util');
var articleUtils = require('../news/article/article');

var omegaUtil = require('../omega/omega-util');
var adUtil = require('../ads/ads-utils');

var jsdom = require('jsdom');
const { URL } = require('url');

var read = require('../../../node-readability/src/readability');

const { saveOrUpdateArticle } = require('../news/article/article');

const {getContent, scrapFeaturedUrls, scrapByUrl,scrapUrlPub, scrapByUrlStl } = require('../news/viewbot/bot');




function isHome(cc) {
    return _.includes(homeAway.home, cc);
}


// Get ads
/**
 * @param userid, typo
 */
router.post('/sld/all',function(req, res){

    console.log("App get Featured ad");
    console.log(req.query);
    let userid = req.body.userid;
    let location = req.body.location;

    let typo = (req.body.kind || req.query.kind || "");

    // Default query
    let query = {isFeatured: true};

    //if it has typo query
    if(!_.isEmpty(typo)&&typo!=undefined){
        //query = {isFeatured: true, slider:typo};
        query = {isFeatured: true, typo:{"$ne":"video"},typo:{"$ne":"audio"},typo:{"$ne":"premium"}};
    }

    //let adsObj = CB.syncGetMany(Ad, {isFeatured: true});
    //let ads = adsObj;
    let user = {};

    // Check user
    if(!_.isEmpty(userid)) {
        user = CB.syncGetUser(userid);
    }


    //Initialize it
    let userCode = "";
    locationUtil.cbGetCountryCode(location, user, function(code){

        //check if code not null
        if(_.isEmpty(code)){
            //Default to Rwanda
            userCode = "RW";
        }

        // Loop thru the ads
        CB.findMany(Ad, query, function(allds){

            if(allds.status === "200") {
                let ads = allds.data;

                ads.map(function (ad, index) {
                    // Filter Admob
                    if (!_.eq(ad.typo, "admob")) {
                        let ccAd = ad.cc;

                        // Filter all
                        if (!_.eq(ccAd, "ALL")) {
                            // If user not same location remove the bish
                            /**if(!_.eq(ccAd, userCode)){
                              // Remove this ad now
                                 ads.splice(index,1);
                            }**/

                            switch (ccAd) {
                                case "HOME":
                                    // If not home remove ad
                                    if (!isHome(userCode)) {
                                        ads.splice(index, 1);
                                    }

                                    break;
                                case "AWAY":
                                    // If it's homeboy hide the ad
                                    if (isHome(userCode)) {
                                        ads.splice(index, 1);
                                    }
                                    break;
                            }


                        }
                        /**else{
                   // All users leave the bish
               }**/
                    }
                });
                // Send the ads
                console.log("##################################################################ADS = " + ads.length);
                res.status(200).json(ads);

            }
            else{
                res.status(400).json([]);
            }
        });


    });






});


/**
 * Get all other films
 * @param userid
 * @param location
 */
router.post('/flm/all', function(req, res){
    var location = req.body.location;


    //let loc = JSON.parse(location);
    //console.log("Long = "+ loc.lon);

    //let fetchedLoc = locationUtils.syncGetLoc(loc);




    console.log("GET Other films");
    console.log(req.body);

    //http://www.tolkienlibrary.com/press/images/movie-tie-in-The-hobbit.jpg

    var that = [];
    //Only get available movies
    CB.findMany(FilmModel,  {"meta.available": true}, function(data){
        console.log(data.data.length);
        if(data.status==="200"){
            data.data.map(function(film){

                // Convert Arrays to String
                var sub_cat = film.meta.sub_cat.join(", ");
                var tags = film.meta.tags.join(", ");
                //var lang = film.meta.lang.join(", ");
                var lang = film.meta.lang[0];
                var bad =  film.rating.bad.join(", ");
                var good = film.rating.good.join(", ");

                var newfilm ={
                    _id: film._id,
                    __v: film.__v,
                    featured: film.featured,
                    movie: film.movie,
                    serie: film.serie,
                    veri: film.veri,
                    id_num: film.id_num,

                    rating: {
                        "bad": bad,
                        "good": good
                    },

                    image:film.image,

                    meta:{
                        available: film.meta.available,
                        isTranslated: film.meta.isTranslated,
                        "lang": lang,
                        "tags": tags,
                        "sub_cat": sub_cat,
                        cat: film.meta.cat,
                        cast: film.meta.cast,
                        dir: film.meta.dir,
                        age_rating: film.meta.age_rating,
                        year: film.meta.year,
                        des: film.meta.des,
                        name: film.meta.name,
                        movie_url: film.meta.movie_url,
                        typo: film.meta.typo
                    },

                }

                that.push(newfilm);
            });
            console.log("Films sent = "+that.length)
            //console.log(that[0]);
            res.status(200).send(that);

        }

        else{
            console.log("Films sent = ")
            res.status(401).send("Error");
        }

    });
});


/**
 * Get all categories**/
// return catelog, tv,film,radio
router.post('/cat/all',function(req, res){

    console.log("App get catelog-------------------------------------------START");
    console.log(req.body);
    console.log("App get catelog-------------------------------------------END");
    let userid = req.body.userid;
    let location = req.body.location;

    var cats = [];

    if(!_.isEmpty(userid)) {

        let user = CB.syncGetUser(userid);
        //Check user


        if (!_.isEmpty(user)) {

            // check if location is available
            if (!_.isEmpty(req.body.location)) {
                location = JSON.parse(req.body.location);
                let userCode = locationUtils.syncGetCountryCode(location, user);

                if (!_.isEmpty(userCode)) {
                    console.log("User got code");
                    //console.log(userCode);
                    omegaUtil.mapWorld(userCode, res);
                }

                else {
                    // location but userCode empty
                    // Failback
                    console.log("Usercode location but userCode empty");
                    omegaUtil.mapWorld("", res);
                }

            }

            // No location but user found
            else {
                // Location not available, fallback films,
                // but send default tv,radio
                console.log("Usercode No location but user found");
                omegaUtil.mapWorld("", res);
            }

        }


        else {
            console.log("User not found get cat");
            //res.status(200).send([]);
            omegaUtil.mapWorld("", res);
        }
    }
    else{
        console.log("Userid empty get cat");
        //res.status(200).send([]);
        omegaUtil.mapWorld("", res);
    }







    /**
     //RADIO ===================
     var cRRD = {
        id: "rrd",
        name: "Rwandan Radios",
        typo: "radio",
        countryCode: "rw",
        index: 1,
        query: {field: "countryCode",value: "RW"}
    };
     cats.push(cRRD);

     //RADIO ===================


     var cRTV = {
    id: "rtvtv",
    name: "Rwandan Televison",
    typo: "tv",
    countryCode: "rw",
    index: 1,
    query: {field: "countryCode",value: "RW"}
  };
     cats.push(cRTV);

     var cKTV = {
    id: "ktvtv",
    name: "Nigerian Television",
    typo: "tv",
    countryCode: "ng",
    index: 1,
    query: {field: "countryCode",value: "NG"}
  };
     cats.push(cKTV);

     // TV ==========================================================


     // FILM ========================================================

     var cRWD = {
    id: "rwdmv",
    name: "Rwandan movies",
    typo: "film",
    countryCode: "rw",
    index: 2222,
    query: {field: "meta.cat",value: "RW"}
  };
     cats.push(cRWD);

     var cRWD = {
    id: "ngnmv",
    name: "Nigerian movies",
    typo: "film",
    countryCode: "ng",
    index: 2222,
    query: {field: "meta.cat",value: "NG"}
  };
     cats.push(cRWD);

     var cTZM = {
        id: "tvnmv",
        name: "Tanzanian movies",
        typo: "film",
        countryCode: "tv",
        index: 2222,
        query: {field: "meta.cat",value: "TZ"}
    };
     cats.push(cTZM);


     res.status(200).json(cats);


     **/

});

//router.post('/mda/all'), function(req, res){};



/**
 * Get all media**/
router.post('/mda/:typo',function(req, res){
    var cate = [];
    var typo = req.params.typo;

    if(typo === "radio"){

        CB.findMany(RadioModel, {"available": true}, function(data){
            if(data.status === "200"){
                res.status(200).send(data.data);
            }
            else{
                res.status(200).send([]);
            }

        });
    }

    else if (typo === "tv"){
        CB.findMany(TvModel, {"available": true}, function(data){
            if(data.status === "200"){
                res.status(200).send(data.data);
            }
            else{
                res.status(200).send([]);
            }

        });
    }

});




router.post('/feeds', async function (req, res) {
    var parsedDocs;
    let docs;
    try {
        docs = await articleUtils.getArticlesQuery(req.body);
        // console.log(req.body);

        parsedDocs = docs.map(doc => {
            // TODO for version 1
            let html = doc.raw[0].data;
            const dom = new jsdom.JSDOM(html);
            let text = dom.window.document.body.textContent;
            //text.replace(' ', '');
            const des = text.trim().substr(0, 150);

            console.log(des); // "Hello world";

            return {
                meta: doc.meta,
                des: des,
                // html: html,
                image: doc.image.banner,
                views: doc.views.cnum,
                utime: doc.ut.dnum,
            }


        });

        res.status(200).json(parsedDocs);
    }
    catch (error){
        console.log(error);
        res.status(200).json([]);
    }
});


/**
 * Post request
 * need an url only
 * @url*/
router.post('/feeds/get',async function (req,res) {

    let article;

    var toremove;
    var lastpart;
    var domain;
    var pathname;
    var c;
    var result;
    let url = req.body.url;

    // If url is blank
    if(req.body.url === undefined){
        return res.json({});
    }


    try {
        article = await NewsArt.findOne({$or: [
            {'meta.url': url},
            {'artid': url},
        ]}).exec();

        // article not empty
        if(!_.isEmpty(article)){

            // send article now
            // res.status(200).json({status: "200", data: result.content, title: result.title, img: result.img});
            res.json({
                title: article.meta.title_og,
                img: article.image.banner,
                views: article.views.cnum,
                utime: article.ut.dnum,
            });
        }
        else {
            throw new Error( "Article is empty" );
        }
    }
    catch (error){
        console.log(error);
        // Proceed with pulling the article then save it now

        try {
            u = new URL(url);

            //console.log(u.pathname);

            pathname = u.pathname;
            domain = u.hostname;

            lastpart = pathname.split(/[/ ]+/).pop();
            toremove = pathname.replace(lastpart, "");

            article = await new Promise(function (resolve, reject) {

                read(url, function (err, article, meta) {

                    if (article) {
                        if (article.content !== false) {


                            var filteredcontent = "";
                            //To remove now
                            if (pathname.startsWith('/')) {
                                //Remove the shit and add the slash
                                filteredcontent = article.content.replace(new RegExp(toremove, 'g'), "/");
                            }
                            else {
                                //Else no slash
                                filteredcontent = article.content.replace(new RegExp(toremove, 'g'), "");
                            }


                            //return res.status(200).json({status: "200", data: getContent(filteredcontent, domain)});
                            // return {};

                            //article.content = filteredcontent;
                            //console.log(article);

                            resolve({content: filteredcontent, title: article.title});


                            //Do get the elements using jsdom here


                        }
                        else {
                            //return  res.status(200).json({status:"200", data:{}})
                            reject({});
                        }
                        article.close;
                    }
                    else {
                        //return  res.status(200).json({status:"200", data:{}})
                        reject({});
                    }

                });
            });

            result = await getContent(article.content, article.content, url, article.title, null);

            if (result !== {}) {
                console.log(result.title);

                // save article for later
                await saveOrUpdateArticle({
                    status: "200",
                    publisher: result.publisher,
                    url: result.url,
                    data: result.data,
                    title: result.title,
                    img: result.img,
                    author: result.author,
                    des: result.des
                });

                return res.json({
                    url: result.url,
                    html: result.data,
                    title: result.title,
                    img: result.img,
                    views: 0,
                    utime: new Date().getTime(),
                });

                // res.status(200).json({status: "200", data: result.content, title: result.title, img: result.img});
            }
            else {
                res.json({});
            }

        }
        catch (err) {
            console.log(err);
            return res.json({});
        }

    }


});



module.exports = router;