var _ = require('lodash');
var twilio = require('twilio');
var conf = require('../static/variables');




function sendSMS(phone, message){

if(!_.isEmpty(phone) && !_.isEmpty(message)){

//Check the phone if plus exists 
if(!_.startsWith("+", phone)){
    //_.replace()
    phone  = "+"+phone;
}

      // Load the twilio module

 
// Create a new REST API client to make authenticated requests against the
// twilio back end
var client = new twilio(conf.TWILIO_ACCOUNT_SID, conf.TWILIO_AUTH_TOKEN);
 
// Pass in parameters to the REST API using an object literal notation. The
// REST client will handle authentication and response serialzation for you.
client.messages.create({
    to: phone,
    from: conf.TWILIO_SEND_NUMBER,
    body:message
}, function(error, message) {
    // The HTTP request to Twilio will run asynchronously. This callback
    // function will be called when a response is received from Twilio
    // The "error" variable will contain error information, if any.
    // If the request was successful, this value will be "falsy"
    if (!error) {
        // The second argument to the callback will contain the information
        // sent back by Twilio for the request. In this case, it is the
        // information about the text messsage you just sent:
        console.log('Success! The SID for this SMS message is:');
        console.log(message.sid);
 
        console.log('Message sent on:');
        console.log(message.dateCreated);
    } else {
        console.log(error);
    }
});

}
}


module.exports = {
    sendSMS: sendSMS
}
