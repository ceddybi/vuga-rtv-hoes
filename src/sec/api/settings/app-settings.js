var config = require('../static/variables');
var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');

var _ = require('lodash');

var mongoose = require('mongoose'),
   User = mongoose.model('UserProfile'),
   PwReset = mongoose.model('PwReset');

var smsUtil = require('../sms/sms-utils');


function updateUn(user, x, res){
     if(!_.isEmpty(user)){

           user.info.firstname = x.firstname;
           user.info.lastname = x.lastname;
           user.info.username = x.username;
           user.veri = (user.veri + 1);

            user.save(function(error, userdata){

                 if(!error){
                     res.status(200).send({status: "200", msg: "Success"});
                 }

                 else{
                     // error updating account
                      res.status(200).send({status: "400", msg: "Error updating account"});
                 }

            });
    }
    else{
         res.status(200).send({status: "400", msg: "Error updating account, User not found"});
    }
}

/**
 * @param userid
 * @param firstname
 * @param lastname
 * @param username
 * @param email
 * 
 */

// Change user info from the app
router.post('/change/info', function(req, res){

    var userid  = req.body.userid;
    var username  = req.body.username;
    var firstname  = req.body.firstname;
    var lastname  = req.body.lastname;

    console.log("Change info");
    console.log(req.body);
    var user = CB.syncGetUser(userid);
    var userOfun = CB.syncGetOne(User, {"info.username": username});

    // check if username exits
    if(!_.isEmpty(userOfun)){
        // my existing username so no error
        console.log("Got user");
        if(userOfun.info.userid === userid){
            updateUn(user, {firstname: firstname,lastname:lastname,username:username}, res);
        }

        // username not mine
        else{
          res.status(200).send({status: "400", msg: "Username already taken"});
        }
        
    }

    else{
          //Update user
          updateUn(user, {firstname: firstname,lastname:lastname,username:username}, res);
          //res.status(200).send({status: "400", msg: "User not found"});
        }

   

    
});

// change password from the app
/**
 * @param userid
 * @param username
 * 
 */
router.post('/change/pw', function(req, res){
    console.log(req.body);
    var userid  = req.body.userid;
    //var username  = req.body.username;
    var password  = req.body.password;

    var user = CB.syncGetUser(userid);

    if(!_.isEmpty(user)){

            var obj = {
                "info.a": config.pw(password)
            };

            User.findOneAndUpdate({"info.userid": userid}, {$set: obj, $inc: { "veri": 1 }}, 
             function(error, userdata){

                 if(!error){
                     res.status(200).send({status: "200", msg: "Success"});
                 }

                 else{
                     // error updating account
                      res.status(200).send({status: "400", msg: "Error updating account"});
                 }

            });
    }
    else{
         res.status(200).send({status: "400", msg: "Error updating account, User not found"});
    }

    

});

// check phone number if valid, if valid send sms
router.post('/forgot/pw', function(req, res){
    console.log(req.body);
    var phone = req.body.phone;
    if(phone != undefined){

       var user = CB.syncGetOne(User,{"info.phone": phone});

       if(!_.isEmpty(user)){

           var pwreset = new PwReset({
               userid: user.info.userid,
               phone: phone,
               code: req.body.code
           });

           pwreset.save(function(error, saved){
               if(!error){
                   // Cool saved
                   // TODO send sms code
                   smsUtil.sendSMS(phone,"Use "+req.body.code+" as your Rwanda TV password reset code. Thank you" );
                     res.status(200).send({
                       status: "200",
                       msg: "Success"
                    });

               }
               else{

                     res.status(200).send({
                       status: "400",
                       msg: "Failed to save"
                    });

               }
           })

       }
       else{
           // invalid phone number
           res.status(200).send({
             status: "400",
             msg: "User not found"
           });

       }

    }

    else{
        // invalid phone number
        res.status(200).send({
            status: "400",
            msg: "Invalid phone number"
        });
    }



});


// Check sms code and phone number
router.post('/reset/pw', async function(req, res){
  var obj;
    var phone = req.body.phone;
  var code = req.body.code;
  var password = req.body.password;

  var user;

  console.log(req.body);

       try {
            user = await User.findOne({"info.phone": phone}).exec();
            // Update user
            obj = {
                "info.a": config.pw(password)
            };

            User.findOneAndUpdate({"info.phone": phone}, {$set: obj, $inc: { "veri": 1 }}, 
             function(error, userdata){

                 if(!error){
                     res.status(200).send({
                         status: {status: "200", msg: "Success"},
                         user: CB.returnUser(userdata)
                     });
                 }

                 else{
                     // error updating account
                      res.status(200).send({
                         status: {status: "400", msg: "Error updating account"},
                         user: {}
                     });

                 }

            });
      }
      catch (error){
           console.log(error);
          // error updating account
          res.status(200).send({
              status: {status: "400", msg: "Error updating account"},
              user: {}
          });

      }

     
});



module.exports = router;