const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
const mongoose = require('mongoose');

const PostModel = mongoose.model('Post');
const FilmModel = mongoose.model('Film');
const TvModel = mongoose.model('Tv');
const RadioModel = mongoose.model('Radio');
const Ad = mongoose.model('Ad');
const _ = require('lodash');

const config = require('../static/variables');

const pushUtils = require('./push');

const multer = require('multer');

const upload = multer({
  dest: __dirname + config.api_public_folder,
});

function getContentModel(typo) {
  if (typo === 'film') {
    return FilmModel;
  } else if (typo === 'tv') {
    return TvModel;
  } else if (typo === 'radio') {
    return RadioModel;
  }

  /** 
        switch(typo){
            case "film":
            return FilmModel;
            break;

            case "radio":
            return RadioModel;
            break;

            case "tv":
            return TvModel;
            break
        }

        * */
}

function getContentObject(typo, obj) {
  if (typo === 'film') {
    return {
      title: obj.meta.name,
      id: obj._id,
    };
  } else if (typo === 'tv') {
    return {
      title: obj.title,
      id: obj._id,
    };
  } else if (typo === 'radio') {
    return {
      title: obj.title,
      id: obj._id,
    };
  }
}

// get All content for push creation
router.get('/get/content/:path', async (req, res) => {
  let film;
  let radio;
  let tv;
  const path = req.params.path;
  console.log(`${path} get content`);

  try {
    switch (path) {
      case 'tv':
        tv = await TvModel.find({}).exec();
        return res.status(200).json(tv);

      case 'radio':
        radio = await RadioModel.find({}).exec();
        return res.status(200).json(radio);

      case 'film':
        film = await FilmModel.find({}).exec();
        return res.status(200).json(film);
      default:
    }
  } catch (error) {
    console.log(error);
    return res.status(400).json([]);
  }
});

router.post('/send/msg', (req, res) => {
  pushUtils.sendPush(req, res);
});

module.exports = router;
