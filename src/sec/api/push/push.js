const mongoose = require('mongoose');

const UserDevice = mongoose.model('UserDevice');
const TvModel = mongoose.model('Tv');
const RadioModel = mongoose.model('Radio');

const FilmModel = mongoose.model('Film');

const http = require('https');

const key = 'AIzaSyAnrVE3xfJceR2CbEGzsKNrBBaOF1_h5cU';

const _ = require('lodash');

let count = 0;

function LoopArrayGroups(ids, push) {
  const pus = push;
  // var r = res;
  const arrSize = _.size(ids); // number of groups
  console.log(`Ids chunks = ${arrSize}`);
  // console.log("ids = " +ids);

  if (arrSize != 0) {
    // get First group array,remove it from the list
    const toBeUsed = ids[0];
    const d = {
      id: toBeUsed,
      pu: push,
    };
    console.log(`Group # ${count}`);

    // process first group
    pushIt(d, promise => {
      if (!promise.error) {
        ids.splice(0, 1);
        count += 1;

        if (_.size(ids) > 0) {
          // setTimeout(LoopArrayGroups.bind(ids, pus, r), 0);
          LoopArrayGroups(ids, pus); // run it again
          console.log(`Next is ${count}`);
        } else {
          console.log('Finished');
          // Push complete
          // savePush(push);
          // res.status(200).send({good:"Sent"});
        }
      } else {
        console.log(`Some error ${promise.error}`);
      }
    });
  } else {
    // no push to send
  }
}

/**
 * @param data
 * id = reg ID's array
 * pu = push object
 *    ,data,title,message,sound,click_action* */
function pushIt(du, callback) {
  const reg = du.id;
  const push = du.pu;
  let respo;

  const payload = JSON.stringify({
    registration_ids: reg, // required
    collapse_key: push.collapse_key || 'your_collapse_key',
    notification: {
      title: push.title,
      body: push.message,
      sound: push.sound,
      click_action: push.click_action,
      icon: 'ic_l',
      color: '#4cb5ab',
    },
    data: push.data,
  });

  const headers = {
    Host: 'fcm.googleapis.com',
    Authorization: `key=${key}`,
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(payload),
  };

  const post_options = {
    host: 'fcm.googleapis.com',
    port: 443,
    path: '/fcm/send',
    method: 'POST',
    headers,
  };

  const post_req = http.request(post_options, response => {
    response.setEncoding('utf8');

    response.on('data', namedata => {
      respo = String(namedata);
    });
    response.on('end', () => {
      console.log(respo);
      callback(respo);
    });

    response.on('error', err => {
      respo.error = err;
      callback(respo);
      console.log(`On error ${err}`);
    });
  });

  post_req.on('error', e => {
    respo.error = e;
    callback(respo);
    console.log(`On error ${e}`);
  });

  post_req.write(payload);
  post_req.end();
}

/**
 * For multiple clients
 * */
function SendPush(req, res) {
  UserDevice.find({ typo: 'android' })
    .lean()
    .exec((error, resu) => {
      if (error) throw error;
      console.log(`user devices = ${_.size(resu)}`);
      console.log(`Req typo = ${req.body.typo}`);
      const ids = _.map(resu, 'info.pushtoken');
      console.log(`IDs = ${_.size(ids)}`);
      const chk = _.chunk(ids, 1000);
      console.log(`Chunks = ${_.size(chk)}`);
      let push = {};
      const ac = req.body.typo.toUpperCase();
      push = {
        title: req.body.title,
        message: req.body.msg,
        sound: req.body.sound,
        click_action: 'APP',
      };

      const d = {
        typo: req.body.typo,
        typoid: req.body.content_id,
      };
      console.log(`Push = ${ac}`);
      getDataTypo(d, callback => {
        if (callback) {
          // Loop through the groups
          push.data = callback;

          if (push.data.typo === undefined) {
            push.data.typo = req.body.typo;
          }

          console.log(push.data);
          res.status(200).send({ status: '200', msg: 'Good' });
          LoopArrayGroups(chk, push);
        } else {
          console.log('Error sending push');
        }
      });
    });
}

/**
 * For custom push
 * */
async function sendCustomPush(query, pushObject) {
  let resu;

  let d;
  let ac;
  let push;
  let chk;
  let ids;

  const { title, msg, sound, click_action, data } = pushObject;

  try {
    resu = await UserDevice.find(query)
      .lean()
      .exec();
    console.log(`User devices = ${_.size(resu)}`);
    // console.log("Req typo = " + typo);
    ids = _.map(resu, 'info.pushtoken');
    console.log(`IDs = ${_.size(ids)}`);
    chk = _.chunk(ids, 1000);
    console.log(`Chunks = ${_.size(chk)}`);
    // push = {};
    // ac = req.body.typo.toUpperCase();
    push = {
      title,
      message: msg,
      sound: sound || 'vuga_zing',
      click_action: click_action || 'APP',
      collapse_key: title,
      data,
    };

    console.log('Push ================ ');
    console.log(push);
    LoopArrayGroups(chk, push);
  } catch (error) {
    console.log(error);
  }
}

function getDataTypo(d, callback) {
  const typo = d.typo;
  const id = d.typoid;
  let data;
  console.log(`getDataTypo = ${typo} ${id}`);

  switch (typo) {
    case 'film':
      FilmModel.findById(id, (error, resu) => {
        if (error) throw error;
        data = {
          typo: 'film',
          id: resu._id,
          obj: resu,
        };
        return callback(data);
      });
      break;

    case 'tv':
      TvModel.findById(id, (error, resu) => {
        if (error) throw error;
        data = {
          typo: 'tv',
          id: resu._id,
          link: resu.link,
          image_path: resu.image_path,
          title: resu.title,
          countryCode: resu.countryCode,
          obj: resu,
        };
        return callback(data);
      });
      break;

    case 'radio':
      RadioModel.findById(id, (error, resu) => {
        if (error) throw error;
        data = {
          typo: 'radio',
          id: resu._id,
          link: resu.link,
          image_path: resu.image_path,
          title: resu.title,
          countryCode: resu.countryCode,
          obj: resu,
        };
        return callback(data);
      });
      break;

    default:
      // const f = ;
      return callback({
        id,
        typo,
      });
  }
}

module.exports = {
  sendPush: SendPush,
  pushSingle: pushIt,
  sendCustomPush,
};
