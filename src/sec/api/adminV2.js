var express = require('express');
var router = express.Router();
var CB =  require('./static/cb');
var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   FilmModel = mongoose.model('Film');
var _ = require('lodash');

var config = require('./static/variables');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + config.api_public_folder
});


var ads = require('./ads/admin-ads');
var sync = require('./sync-admin/admin-sync');
var chat = require('./chat/admin-chat');
var chatO = require('./chat/admin-chat-old');
var push = require('./push/admin-push');
var user = require('./user/admin-user');
var device = require('./device/admin-device');
var traw = require('./traw/admin-traw');
var scrap = require('./news/admin-scrapper');
var news = require('./news/admin-news');


router.use('/ads',ads);
router.use('/sync',sync);
router.use('/chat',chat);
router.use('/chat-old',chatO);
router.use('/push',push);
router.use('/user',user);
router.use('/device',device);
router.use('/traw',traw);
router.use('/news',news);
router.use('/scrap',scrap);


module.exports = router;