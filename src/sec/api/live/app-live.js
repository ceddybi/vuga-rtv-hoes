const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
const mongoose = require('mongoose');

const RadioModel = mongoose.model('Radio');
const TvModel = mongoose.model('Tv');

const _ = require('lodash');

const config = require('../static/variables');

const multer = require('multer');

const upload = multer({
  dest: __dirname + config.api_public_folder,
});

/**
 * Get all categories with films
 * @param userid
 * @param location
 *
 */
router.post('/:typo/all', (req, res) => {
  const cate = [];
  const typo = req.params.typo;

  if (typo === 'radio') {
    CB.findMany(RadioModel, { available: true }, (data) => {
      if (data.status === '200') {
        res.status(200).send(data.data);
      } else {
        res.status(200).send([]);
      }
    });
  } else if (typo === 'tv') {
    CB.findMany(TvModel, { available: true }, (data) => {
      if (data.status === '200') {
        res.status(200).send(data.data);
      } else {
        res.status(200).send([]);
      }
    });
  }
});

module.exports = router;
