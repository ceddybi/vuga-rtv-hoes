var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var mongoose = require('mongoose'),
   TvModel = mongoose.model('Tv');
var _ = require('lodash');

var config = require('../static/variables');

// GET LIST
router.get('/',function(req, res){
       console.log("GET MANY");
       //console.log(req.query);
       //console.log(req.body);

       CB.findMany(TvModel, {}, function(d){
            if(d.status === "200"){

                var ts =[];
                  d.data.map(function(body){
                var data = {
                    id: body._id,
                    typo: "tv",
                    title: body.title,
                    description: body.description,
                    link: body.link,
                    image_path: body.image_path,
                    featured: body.featured,
                    //countries: body.countries,
                    available: body.available,
                }

                    ts.push(data);
                });

                console.log(ts.length)
                res.status(200).send(ts);
            }

            else{
                console.log("No data")
                res.status(200).send([]);
            }
       });
});

// GET ONE

router.get('/:id',function(req, res){
       console.log("GET ONE");
       console.log(req.params);
       console.log(req.body);

       var id = req.params.id;

    CB.findOne(TvModel, {_id: id}, function(gd){
            if(gd.status==="200"){

                var body = gd.data;
                var data = {
                    id: body._id,
                    title: body.title,
                    countryCode: body.countryCode,
                    isWebView: body.isWebView,
                    description: body.description,
                    link: body.link,
                    image_path: body.image_path,
                    featured: body.featured,
                    //countries: body.countries,
                    available: body.available,
             }
                
                res.status(200).send(data);
            }
            else{

                res.status(401).send("Error");
            }
    });
});

// CREATE
router.post('/', function(req, res){
       console.log("CREATE CALLED");
       //console.log(req.files);
       //console.log(req.body);

       var body = req.body;


        var data = {
                    typo: "tv",
                    title: body.title,
                    countryCode: body.countryCode,
                    isWebView: body.isWebView,
                    description: body.description,
                    link: body.link,
                    image_path: body.image_path,
                    featured: body.featured,
                    //countries: body.countries,
                    available: body.available,
        }

    CB.create(TvModel, data, function(gd){
        if(gd.status==="200"){
            console.log("Success creating TV");
            res.status(200).send({id:gd.data._id});
        }
        else{
            console.log("Error creating TV");
            console.log(gd.msg)
            res.status(404).send("Error");
        }
    });

});

// UPDATE
router.put('/:id',function(req, res){
       console.log("UPDATE CALLED");

       var id = req.params.id;
       var body = req.body;

       var data = {
                    
                    title: body.title,
                    description: body.description,
                    countryCode: body.countryCode,
                    isWebView: body.isWebView,
                    link: body.link,
                    image_path: body.image_path,
                    featured: body.featured,
                    //countries: body.countries,
                    available: body.available,
        }
        
        TvModel.findOneAndUpdate({_id: id}, {$set: data, $inc: { "veri": 1 }},
            function(err, good){

                    if(good){
                        console.log(good);
                        res.status(200).send({data:""});
                    }
                    else{
                        console.log(err);
                        res.status(400).send({error:"Could not update"});
                    }
                }
        );

       
});

// DELETE
router.delete('/:id',function(req, res){
       console.log("DELETE CALLED");
       var id = req.params.id;
       RadioModel.remove({_id:id}, function(c){
           if(c){
               res.status(401).send({error: "Delete not called"});
              
           }
           else{
               res.status(200).send({data:""});
           }
       })
});


module.exports = router;