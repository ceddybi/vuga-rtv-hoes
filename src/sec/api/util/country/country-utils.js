/**
 * Created by vuga on 9/11/17.
 * Contains supported countries with their indexes defined
 *
 * two letter-lower-case code, index value
 *
 * Else ot default to one
 *
 * Countries have groups
 */

var _ = require('lodash');



var supported = {
    //East Africa with Congo
     rw: "",ke: "",tz: "",ug: "",bi: "",cd: "",

    //West Africa
    gh: "",ng: "",sn: "",ci: "",

    //Southern Africa
    za:"", zm:"", mz:"",


    //Asia
    cn:"", in:"",kr:"",th:"",


    //USA, Euro
    us:"", gb:"",
};

var index = {

    //250
    rw: 25,

    //US
    us: 23,

    //indian
    in: 23,

    //Ghana & naija
    gh:24,ng: 24,

    //KE and TZ
    ke: 22, tz: 22,

    ug: 20,
    bi: 19,
    cd: 18,
};

function getIndex(code){

    let d = index[code];

    //Check if d is valid
    if(d!=undefined){
        return d;
    }
    else{
        return 1;
    }
}


//console.log(getIndex("gf"));
//console.log(getIndex("rw"));

module.exports = {
    getIndex: getIndex,
    supported, supported
}