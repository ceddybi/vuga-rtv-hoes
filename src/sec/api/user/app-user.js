const config = require('../static/variables');
const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
const _ = require('lodash');
const userCreate = require('./user-app-create');

let mongoose = require('mongoose'),
  UserOld = mongoose.model('User'),
  ChatOld = mongoose.model('Chat'),
  UserProfile = mongoose.model('UserProfile');

const smsUtil = require('../sms/sms-utils');
const adminUtil = require('../admin/admin-utils');

function loopOldMsg() {
  const newM = {
    title: 'Hello',
    typo: 'user',
    msg: req.body.msg,
    phone: `${calldo.phone}`,
    firstname: calldo.firstname,
    lastname: calldo.lastname,
    userid: calldo.userid,
    unique: calldo.unique,
    device: calldo.device,
    unix_time: `${new Date().getTime()}`,
  };
}

// UDPATES from OLD
// Now
router.post('/reg/update/old', (req, res) => {
  console.log('UPDATE OLD PATH');

  const info = {
    phone: req.body.phone,
    username: req.body.username,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    sex: req.body.sex,
  };

  const oldLad = CB.syncGetOne(UserOld, { phone: req.body.phone });

  if (_.isEmpty(oldLad)) {
    // Account not found
    console.log('Account not found');
    res.status(200).json({
      status: '400',
      msg: 'Please create a new Account',
    });
  } else {
    // Migrate this old asset
    const d = { info, oldlad: oldLad };

    userCreate.newUserFromOldWithoutPW(d, good => {
      if (good.status === '200') {
        console.log('Success new user migration');
        res.status(200).json(good);
      } else {
        console.log('Failed to migrate user from old');
        res.status(200).json({
          status: '400',
          msg: 'Please create a new Account',
        });
      }
    });
  }
});

// Create user

router.post('/reg/new', (req, res) => {
  console.log('REG NEW');
  console.log(req.body);

  userCreate.newUserWithoutPW(req.body, data => {
    if (data.status === '200') {
      // Add new user count to admin
      adminUtil.addNewUser();

      res.status(200).send({ status: '200', msg: 'Good' });
    } else {
      res.status(200).send({ status: '400', msg: 'Error creating user' });
    }
  });
});

// UPDATE PW FROM NEW
// TODO
router.post('/reg/update/pw', (req, res) => {
  console.log('UPDATE PW PATH');
  console.log(req.body);

  userCreate.updatePW(req.body, data => {
    if (data.status === '200') {
      const ud = data.data;
      var tosend = {
        status: { status: '200', msg: 'Good' },
        user: CB.returnUser(ud),
      };

      res.status(200).send(tosend);

      // res.status(200).send({status:"200", msg:"Good"});
    } else {
      var tosend = {
        status: { status: '400', msg: 'Error getting user' },
        user: {},
      };
      res.status(200).send(tosend);
    }
  });
});

// UPDATE BDAY ND RETURN USER OBJECT
router.post('/reg/update/bday', (req, res) => {
  console.log('UPDATE BDAY PATH');
  console.log(req.body);

  userCreate.updateBday(req.body, data => {
    if (data.status === '200') {
      let ud = data.data;
      let tosend = {
        status: { status: '200', msg: 'Good' },
        user: CB.returnUser(ud),
      };

      res.status(200).send(tosend);
    } else {
      res.status(200).send({ status: '400', msg: 'Error creating user' });
    }
  });
});

// GET SMS
router.post('/reg/get/sms', (req, res) => {
  console.log('SMS PATH');
  console.log(req.body);

  const smscode = req.body.code;
  const phone = req.body.phone;

  smsUtil.sendSMS(phone, `Use ${smscode} as your Rwanda TV code. Thank you`);

  res.status(200).send({ status: '200', msg: 'Good' });
});

// Checkers PHONE
router.post('/reg/check/phone', async (req, res) => {
  console.log('CHECK PHONE PATH');
  console.log(req.body);
  const phone = req.body.phone;
  let user;

  try {
    user = await UserProfile.findOne({ 'info.phone': phone }).exec();
    if (!user) {
      // No user found, Good
      return res.json({ status: '200', msg: 'Good' });
    }
    // We already have this user
    return res.json({ status: '400', msg: 'User already exists, use another phone number' });
  } catch (error) {
    console.log(error);
    return res.json({ status: '400', msg: 'An error occurred, please try again' });
  }
});

// Check USERNAME
router.post('/reg/check/username', async (req, res) => {
  console.log('CHECK USERNAME PATH');
  console.log(req.body);

  const username = req.body.username;
  let user;

  try {
    user = await UserProfile.findOne({ 'info.username': username }).exec();
    if (!user) {
      return res.json({ status: '200', msg: 'Good' });
    }
    return res.json({ status: '400', msg: 'Username already taken, please choose another one' });
  } catch (error) {
    console.log(error);
    return res.status(200).send({ status: '400', msg: 'Please try again' });
  }
});

router.post('/login', (req, res) => {
  // var timeTo = timeZone(req.body.gmt);
  // to add 7200000
  console.log('User Login request');
  console.log(req.body);
  const un = req.body.username;
  let argo = '';

  UserProfile.findOne(
    {
      $or: [{ 'info.username': un }, { 'info.email': un.toLowerCase() }, { 'info.phone': un }],
    },
    (err, doc) => {
      if (doc) {
        argo = doc.info.a;
        console.log('Found user');

        // Check Argon
        // console.log("User Found Argon = "+ doc.a);
        const oj = {
          hash: argo,
          pw: req.body.password,
        };

        config.argonVerify(oj, (pw) => {
          if (pw) {
            // console.log("User Found Argon Success");
            // Update User activity

            let tosend = CB.returnUser(doc);

            // Activator(req, doc.userid);

            // var tosend = changeTime(doc, timeTo);

            console.log(tosend);
            res.status(200).send({
              status: { status: '200', msg: 'Success' },
              user: tosend,
            });
          } else {
            res.status(200).send({
              status: { status: '400', msg: 'Invalid Password' },
              user: {},
            });
          }
        });
      } else {
        // user not found
        console.log('Found not user');
        res.status(200).send({
          status: { status: '400', msg: 'This User could not be found.' },
          user: {},
        });
      }
    }
  );
});

module.exports = router;
