var config = require('../static/variables');
var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var userCreate = require('./user-app-create');

var mongoose = require('mongoose'),
   User = mongoose.model('UserProfile');


router.post('/create', function(req, res){
      userCreate.newUserDumb(req, res);
});


router.post('/update', function(req, res){

});


router.post('/delete', function(req, res){

});

router.post('/namesync', function(req, res){
      var userid = req.body.userid;
      CB.findOne(User, {"info.userid": userid},function (data){
         if(data.status==="200"){
               // Send the user data
               res.status(200).send(data.data.info);
         }
         else{
               res.status(401).send(data);
         }
     })

});


module.exports = router;