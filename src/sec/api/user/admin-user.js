var config = require('../static/variables');
var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var _ = require('lodash');
var userCreate = require('./user-app-create');

var mongoose = require('mongoose'),
   UserOld = mongoose.model('User'),
   ChatOld = mongoose.model('Chat'),
   UserProfile = mongoose.model('UserProfile');




function checkUn(username, callback){
    if(!_.isEmpty(username)){
        let unUser = CB.syncGetOne(UserProfile, {"info.username": username});
        // Found user
        if(!_.isEmpty(unUser)){
            callback({status: "400", msg:"Already exists", data: unUser});
        }
        else{
            // User not found
            callback({status: "200", msg:"good", data: unUser});
        }
    }
    else{
        // Username empty
       callback({status: "600", msg:"empty"});
    }
}

function updateUser(data, res){

    let pw = data.password;
    if(!_.isEmpty(pw) && pw != undefined){

    }
    else{
        //set empty password
        data.password = "";
    }

    checkUn(data.username, function(result){
        switch (result.status){
            case "200":
                // Success
                userCreate.updateUserAdmin(data, function(data){
                    if(data.status === "200"){
                        res.status(200).send({status:"200", msg:"Good"});
                    }
                    else{
                        res.status(200).send({status:"400", msg:"Error creating user"});
                    }
                });

                break;
            case "400":
                // Error username already exists
                if(result.data.info.userid === data.userid){
                    // same user
                }
                else {
                    //set new username
                    data.username = data.username + "." + config.rand(5);
                }
                userCreate.updateUserAdmin(data, function(data){
                    if(data.status === "200"){
                        res.status(200).send({status:"200", msg:"Good"});
                    }
                    else{
                        res.status(200).send({status:"400", msg:"Error creating user"});
                    }
                });
                break;
            case "600":
                // Username Empty
                data.username = data.firstname +"." + data.lastname +config.rand(5);
                userCreate.updateUserAdmin(data, function(data){
                    if(data.status === "200"){
                        res.status(200).send({status:"200", msg:"Good"});
                    }
                    else{
                        res.status(200).send({status:"400", msg:"Error creating user"});
                    }
                });
                break;
        }
    });
}

function createUser(data, res){
    checkUn(data.username, function(result){
        switch (result.status){
            case "200":
                // Success
                userCreate.newUserAdmin(data, function(data){
                    if(data.status === "200"){
                        res.status(200).send({status:"200", msg:"Good"});
                    }
                    else{
                        res.status(200).send({status:"400", msg:"Error creating user"});
                    }
                });

                break;
            case "400":
                // Error username already exists
                data.username = data.username+"." +config.rand(5);
                userCreate.newUserAdmin(data, function(data){
                    if(data.status === "200"){
                        res.status(200).send({status:"200", msg:"Good"});
                    }
                    else{
                        res.status(200).send({status:"400", msg:"Error creating user"});
                    }
                });
                break;
            case "600":
                // Username Empty
                data.username = data.firstname +"." + data.lastname +config.rand(5);
                userCreate.newUserAdmin(data, function(data){
                    if(data.status === "200"){
                        res.status(200).send({status:"200", msg:"Good"});
                    }
                    else{
                        res.status(200).send({status:"400", msg:"Error creating user"});
                    }
                });
                break;
        }
    });
}



// Create user
router.post('/create', function(req, res){
   console.log("Create new user Admin");
   console.log(req.body);

   createUser(req.body, res);

});


// Update user
router.post('/update', function(req, res){
    console.log("Edit user admin");
    console.log(req.body);

    updateUser(req.body, res);

});


// Get all users
router.post('/get/all', function(req, res){
    console.log("Get all user admin");
    console.log(req.body);

    let dt =  {
        status: "200",
        msg: "Users not found",
        data: []
    };


   UserProfile.find({}).lean().exec(function(error, users){
       if(!error){
           // Map users now
           console.log("Users are "+users.length);
           console.log(users[0]);
           users.map(function(user){
               let x = {};
               x = user.info;
               x.id = user.info.userid;
               if(user.premium!=undefined){
                   x.isPremium = user.premium.active;
               }
               else{
                   x.isPremium =  false;
               }

               dt.data.push(x);
           });
           res.status(200).json(dt);
       }
       else{
           // Users not found
           res.status(200).json(dt);
       }
   })


});


// Get one
router.post('/get/id', function(req, res){
    console.log("get user by id admin");
    console.log(req.body);

    let user = CB.syncGetUser(req.body.userid);

    if(!_.isEmpty(user)){
        //
       var dt =  {
            status: "200",
            msg: "User found",
            data: {
                userid: user.info.userid,
                firstname: user.info.firstname,
                lastname: user.info.lastname,
                username: user.info.username,
                password: "",
                isPremium: user.premium.active,
            }
        };

       //dt.data.isPremium = user.premium.active;
       //console.log(dt.data.isPremium);
       res.status(200).json(dt);
    }


    else{

        var dt =  {
            status: "400",
            msg: "User not found",
            data:{}
        };
        res.status(200).json(dt);

    }


});





module.exports = router;