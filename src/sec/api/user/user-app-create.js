const config = require('../static/variables');
const CB = require('../static/cb');
const _ = require('lodash');
const mongoose = require('mongoose');

const UserOld = mongoose.model('User');
const UserDevice = mongoose.model('UserDevice');
const ChatOld = mongoose.model('Chat');
const User = mongoose.model('UserProfile');
const Convo = mongoose.model('Convo');

const appChat = require('../chat/chat-app');

function doOldMsg(chatObj, userid, isAdmin) {
  // from admin to user
  const convoObj = CB.syncGetOne(Convo, { user_id: { $all: [userid, 'vs'] } });

  if (!_.isEmpty(convoObj)) {
    if (isAdmin) {
      appChat.sendMsgCallback('vs', userid, chatObj.msg, chatObj._id, d => {
        if (d.status === '200') {
          console.log('Exported chat');
        }
      });
    } else {
      // else from user to admin
      appChat.sendMsgCallback(userid, 'vs', chatObj.msg, chatObj._id, d => {
        if (d.status === '200') {
          console.log('Exported chat');
        }
      });
    }
  } else {
    // Create new convo
    // appChat.newConvo("vs", userid, chatObj.msg, chatObj._id);

    const userCreator = CB.syncGetUser('vs');
    const userOperator = CB.syncGetUser(userid);

    const convoid = config.rand(30);

    if (!_.isEmpty(userCreator) && !_.isEmpty(userOperator)) {
      // console.log(userCreator);
      // console.log(userOperator);

      const convo = new Convo({
        user_id: [userCreator.info.userid, userOperator.info.userid],
        convoid,
        creator: {
          userid: userCreator.info.userid,
          firstname: userCreator.info.firstname,
          lastname: userCreator.info.lastname,
          username: userCreator.info.username,
          obj: userCreator,
        },
        operator: {
          userid: userOperator.info.userid,
          firstname: userOperator.info.firstname,
          lastname: userOperator.info.lastname,
          username: userOperator.info.username,
          obj: userOperator,
        },
      });

      convo.save((error, saved) => {
        if (!error) {
          console.log('Convo added');
          // Send new message
          if (isAdmin) {
            appChat.sendMsgCallback('vs', userid, chatObj.msg, chatObj._id, d => {
              if (d.status === '200') {
                console.log('Exported chat');
              }
            });
          } else {
            // else from user to admin
            appChat.sendMsgCallback(userid, 'vs', chatObj.msg, chatObj._id, d => {
              if (d.status === '200') {
                console.log('Exported chat');
              }
            });
          }
        } else {
          console.log('Failed to save Convo');
        }
      });
    } else {
      console.log('Error users empty');
    }
  }
}
function loopThruOldMsg(userid, msgs) {
 
  if (!_.isEmpty(msgs)) {
    // appChat.newConvo("vs", good.info.userid, Message, config.rand(20));
    msgs.map(chatObj => {
      doOldMsg(chatObj, userid, chatObj.isAdmin);
    });
  } else {
    console.log('Zero messages found from old user');
  }
}

function checkOldMsgFromNewUser(phone, newUid) {
  // Check for old messages
  const oldU = CB.syncGetOne(UserOld, { phone });

  // Check if null
  if (!_.isEmpty(oldU)) {
    const msgUser = CB.syncGetMany(ChatOld, { userid: oldU.userid });

    if (!_.isEmpty(msgUser)) {
      loopThruOldMsg(newUid, msgUser);
    }
  }
}
module.exports = {
  newUserAdmin(data, callback) {
    // var usid = rand();
    // var msgs = wtc;
    const userid = config.rand(20);

    const vg = new User({
      info: {
        a: config.pw(data.password),
        userid,
        firstname: data.firstname,
        lastname: data.lastname,
        username: data.username,
        phone: '',
        sex: data.sex.toLowerCase(),
      },
      premium: {
        active: data.isPremium,
      },
    });

    vg.save((error, result) => {
      if (error) {
        console.error(error);
        throw error;
      }

      callback({ status: '200', data: result });
    });
  },

  updateUserAdmin(data, callback) {
    // var usid = rand();
    // var msgs = wtc;
    const user = CB.syncGetUser(data.userid);

    if (!_.isEmpty(user)) {
      // Update not null
      user.premium.active = data.isPremium;
      user.info.firstname = data.firstname;
      user.info.lastname = data.lastname;
      user.info.username = data.username;

      // check password
      if (!_.isEmpty(data.password)) {
        user.info.a = config.pw(data.password);
      }

      user.save((error, result) => {
        if (error) {
          console.error(error);
          throw error;
        }

        callback({ status: '200', data: result });
      });
    } else {
      callback({ status: '400', msg: 'User not found' });
    }
  },

  newUserDumb(req, res) {
    // var usid = rand();
    // var msgs = wtc;

    const vg = new User({
      info: {
        userid: 'ceddybi',
        firstname: 'Ceddy',
        lastname: 'Muhoza',
        username: 'ceddybi',
      },
    });

    vg.save((error, result) => {
      if (error) {
        console.error(error);
        throw error;
      }
      // Add to user count
      // UserCount(1, true);
      res.status(200).send({ status: '200', data: result });
    });
  },

  newUserWithoutPW(data, callback) {
    // var usid = rand();
    // var msgs = wtc;
    const userid = config.rand(20);

    const vg = new User({
      info: {
        userid,

        firstname: data.firstname,
        lastname: data.lastname,
        username: data.username,
        phone: data.phone,
        sex: data.sex,
      },
    });

    vg.save((error, result) => {
      if (error) {
        console.error(error);
        throw error;
      }

      callback({ status: '200', data: result });
    });
  },

  newUserFromOldWithoutPW(x, callback) {
    // Look for messages in this old user
    // Account found
    const oldLad = x.oldlad;
    const newInfo = x.info;

    const newUid = config.rand(20);
    const oldUid = oldLad.userid;

    newInfo.userid = newUid;
    const newLad = {
      info: newInfo,
    };

    // Device
    const NewDevice = {
      user: {
        userid: newUid,
      },

      info: {
        wlan: oldLad.wlan,
        device: oldLad.device,
        osid: oldLad.androidId,
        pushtoken: oldLad.fcmToken,
        unique: oldLad.unique,
        appversion: oldLad.appversion,
      },
    };
    // NewDevice.location.countryCode = oldLad.loc;
    const oldD = new UserDevice(NewDevice);
    oldD.save((er, sa) => {
      if (!er) {
        console.log('Saved old device to new');
      } else {
        console.log('Failed to save new device');
      }
    });

    // TODO CREATE ACCOUNT
    const roUser = new User(newLad);
    roUser.save((error, saved) => {
      if (!error) {
        console.log('New User from old saved');
        // Loop through old and create new messages
        // TODO LOOP CHATS
        const msgUser = CB.syncGetMany(ChatOld, { userid: oldUid });
        loopThruOldMsg(newUid, msgUser);

        // Callback
        callback({ status: '200', msg: '', user: CB.returnUser(saved) });
      } else {
        console.log('New User from old failed to saved');
        callback({ status: '400', msg: 'Error saving account' });
      }
    });
  },

  // Final step
  updatePW(data, callback) {
    const username = data.username.toLowerCase();

    const obj = {
      'info.a': config.pw(data.password),
    };

    User.findOneAndUpdate(
      { 'info.username': username },
      { $set: obj, $inc: { veri: 1 } },
      (err, good) => {
        if (good) {
          console.log('Success updating password');

          // Check or Create new serie
          // SerieCheck(id, obj.meta.name, obj.meta.typo);

          console.log('Data update user');
          // console.log(good);
          // Add new user convo
          // creator, operator, msg, chatid
          const Message = 'Welcome to Rwanda TV, ijwi ryawe, Vuga!';
          appChat.newConvo('vs', good.info.userid, Message, config.rand(20));

          // Check old user for new messages
          checkOldMsgFromNewUser(good.info.phone, good.info.userid);

          // console.log(good);
          callback({ data: good, status: '200' });
        } else {
          console.log('Error updating pw');
          console.log(err);
          callback({ status: '400' });
        }
      }
    );
  },

  /** @DEPRECATED
   //Final step of all * */
  updateBday(data, callback) {
    const obj = {
      'info.bday': {
        year: data.year,
        month: data.month,
        day: data.day,
      },
    };

    User.findOneAndUpdate(
      { 'info.username': data.username },
      { $set: obj, $inc: { veri: 1 } },
      (err, good) => {
        if (good) {
          // Check or Create new serie
          // SerieCheck(id, obj.meta.name, obj.meta.typo);

          console.log('Data update user');
          // console.log(good);
          // Add new user convo
          // creator, operator, msg, chatid
          const Message = 'Welcome to Rwanda TV, ijwi ryawe, Vuga!';
          appChat.newConvo('vs', good.info.userid, Message, config.rand(20));

          callback({ data: good, status: '200' });
        } else {
          console.log('error');
          console.log(err);
          callback({ status: '400' });
        }
      }
    );
  },
};
