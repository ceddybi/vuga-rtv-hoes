/**
 * Created by vuga on 10/21/17.
 */

const _ = require('lodash');

const request = require('request');
const jsdom = require('jsdom');
const jsdomold = require('jsdom');
// const jsdomold= require("../../../../node_modules/jsdom/lib/old-api");
const serializeDocument = jsdom.serializeDocument;
const { URL } = require('url');

const config = require('../../static/variables');
const mongoose = require('mongoose');

const NewsArt = mongoose.model('newsArt');
const NewsPub = mongoose.model('newsPub');

const { getCorrectUrl } = require('../../static/textUtils');

const { saveOrUpdateArticle } = require('../article/article');

const xpath = require('xpath');
const xmldom = require('xmldom').DOMParser;

const read = require('../../../../node-readability/src/readability');

const scrapFeatureUrls = site => {
  let options;
  let path = site.xpath;
  let url = site.url;

  if (!url.startsWith('http')) {
    url = `http://${  url}`;
  }

  options = {
    url,
    headers: {
      'User-Agent':
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36',
      Accept: 'text/html,application/xhtml+xml,application/xml;charset=UTF-8',
      'Accept-Language': 'en-US,en;q=0.8',
      'Cache-Control': 'max-age=0',
      Connection: 'keep-alive',
    },
  };

  // /**
  return new Promise(((resolve, reject) => {
    request.get(options, (error, response, html) => {

            if(error){
                console.log("error getting site");
                reject(error);
            }
            else{

                //console.log(html);
                let doc = new xmldom().parseFromString(html);

                let nodes;
                try {
                    nodes = xpath.select(path, doc);
                }
                catch(e) {
                    //console.log()
                    reject(e)
                }

                let urls = [];

                if(nodes) {
                    //console.log(nodes);
                    for (let i = 0; i < nodes.length; ++i) {
                        if (nodes[i]) {
                            //console.log(nodes[i].value);
                            urls.push(getCorrectUrl(url, nodes[i].value))
                        }
                    }

                }
                console.log(url+" = "+urls.length);
                resolve(urls);

            }
        });
  }));

  //* */
};

const getContent = (html, originalHtml, url, title, publisher) => {
  return new Promise(((resolve, reject) => {
    jsdomold.env({
      html: '<!DOCTYPE html>' + html,
      done(errors, window) {

                if (errors) {
                    window.close();
                    console.log("Error manipulating jsdom");
                    resolve({content: html, img:"", title:title});
                }
                else {
                    if (window.document.body) {

                        var featured_image;
                        //Select all images, correcting the right image source
                        var imgs = window.document.getElementsByTagName('img');

                        for(var i = 0; i < imgs.length; i++){
                            var img = imgs[i];
                            var img_src = img.getAttribute('src');
                            //console.log(imgs[i].getAttribute('src'))

                            //Add domain
                            if (img_src.startsWith("data")){
                                var datasrc = (img.getAttribute("data-src") || "");
                                img.setAttribute('src', getCorrectUrl(url, datasrc));
                                img.setAttribute('data-src', "");
                            }

                            else{
                                //for http and A-Z
                                img.setAttribute('src', getCorrectUrl(url, img_src));
                            }


                            var nice_img = img.getAttribute('src');
                            // console.log();



                            //setting featured image as first img in this
                            if(nice_img!== undefined && !_.isEmpty(nice_img)){
                                //Set the first image as featured
                                if(featured_image === undefined) {
                                    featured_image = nice_img;
                                }
                            }

                            //Clean height and width plus style
                            if(img.hasAttribute('width')){
                                img.removeAttribute('width')
                            }
                            if(img.hasAttribute('style')){
                                img.removeAttribute('style')
                            }
                        }

                        // Select all scripts and remove them now
                        var scripts = window.document.getElementsByTagName('script');
                        for (var i = 0; i < scripts.length; i++){
                           // var  = imgs[i];
                            scripts[i].parentNode.removeChild(scripts[i]);
                        }

                        //console.log(originalHtml);
                        //if publisher is not null
                        if(publisher) {
                            let ogDoc = new xmldom().parseFromString(originalHtml);
                            //Images Goodness Goodies
                            if (!_.isEmpty(publisher.meta.xpath_fimg)) {
                                let nodes = xpath.select(publisher.meta.xpath_fimg, ogDoc);
                                var fimg = nodes.value;


                                //Check if fimg not null
                                if(fimg){
                                    if(!_.isEmpty(fimg) && !fimg.startsWith("data")){
                                        let f_url = getCorrectUrl(url, fimg);
                                        featured_image = f_url;
                                    }
                                }
                                //console.log("IMAGE");
                                //console.log(nodes);
                                //console.log(publisher.meta.xpath_fimg);
                                //console.log(fimg);
                            }
                            else{
                                console.log("Publisher featured image null");
                                console.log(publisher.meta.xpath_fimg);
                            }

                        }
                        else {
                            console.log("Publisher null")
                            //console.log(publisher)
                        }

                        var content = serializeDocument(window.document);


                        var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
                        while (SCRIPT_REGEX.test(content)) {
                            content = content.replace(SCRIPT_REGEX, "");
                        }

                        //console.log(content);
                        resolve({content: content, img:featured_image, title:title});
                        //console.log(content);

                    }
                    else{
                        console.log("Error jsdom body null");
                        resolve({content: html, img:"", title:title});
                    }
                }

            }
    });
  }));
};

const getContentXX = (html, originalHtml, url, title, publisher) => {
  let featured_image;
  let window;
  let dom;

  return new Promise(((resolve, reject) => {
    try {
      dom = new jsdom.JSDOM(`<!DOCTYPE html>${  html}`);
      window = dom.window;
      if (window.document.body) {
        // Select all images, correcting the right image source
        let imgs = window.document.getElementsByTagName('img');

        for (var i = 0; i < imgs.length; i++) {
          var img = imgs[i];
          let img_src = img.getAttribute('src');
          // console.log(imgs[i].getAttribute('src'))

          // Add domain
          if (img_src.startsWith('data')) {
            var datasrc = img.getAttribute('data-src') || '';
            img.setAttribute('src', getCorrectUrl(url, datasrc));
            img.setAttribute('data-src', '');
          } else {
            // for http and A-Z
            img.setAttribute('src', getCorrectUrl(url, img_src));
          }

          let nice_img = img.getAttribute('src');
          // console.log();

          // setting featured image as first img in this
          if (nice_img !== undefined && !_.isEmpty(nice_img)) {
            // Set the first image as featured
            if (featured_image === undefined) {
              featured_image = nice_img;
            }
          }

          // Clean height and width plus style
          if (img.hasAttribute('width')) {
            img.removeAttribute('width');
          }
          if (img.hasAttribute('style')) {
            img.removeAttribute('style');
          }
        }

        // Select all scripts and remove them now
        let scripts = window.document.getElementsByTagName('script');
        for (var i = 0; i < scripts.length; i++) {
          var img = imgs[i];
          scripts[i].parentNode.removeChild(scripts[i]);
        }

        // console.log(originalHtml);
        // if publisher is not null
        if (publisher) {
          const ogDoc = new xmldom().parseFromString(originalHtml);
          // Images Goodness Goodies
          if (!_.isEmpty(publisher.meta.xpath_fimg)) {
            const nodes = xpath.select(publisher.meta.xpath_fimg, ogDoc);
            let fimg = nodes.value;

            // Check if fimg not null
            if (fimg) {
              if (!_.isEmpty(fimg) && !fimg.startsWith('data')) {
                const f_url = getCorrectUrl(url, fimg);
                featured_image = f_url;
              }
            }
            // console.log("IMAGE");
            // console.log(nodes);
            // console.log(publisher.meta.xpath_fimg);
            // console.log(fimg);
          } else {
            console.log('Publisher featured image null');
            console.log(publisher.meta.xpath_fimg);
          }
        } else {
          console.log('Publisher null');
          // console.log(publisher)
        }

        let content = serializeDocument(window.document);
        // var content = dom.serialize();
        // var content = dom.window.document.documentElement.outerHTML;

        // console.log(content);
        resolve({ content: content, img: featured_image, title: title });
        // console.log(content);
      } else {
        console.log('Error jsdom body null');
        resolve({ content: html, img: '', title: title });
      }
    } catch (error) {
      console.log(error);
      window.close();
      console.log('Error manipulating jsdom');
      resolve({ content: html, img: '', title: title });
    }
  }));
};

const scrapByUrl = site => {
  const url = site.url;
  const publisher = site.publisher;

  return new Promise(((res, rej) => {
    // var url = (req.body.url || "http://igihe.com/politiki/amakuru/article/perezida-kagame-yasabye-ko-ikigo-cy-ibarurishamibare-cyajya-gisuzuma-imihigo");

    // IF url is not empty
    if (!_.isEmpty(url)) {
      let u = new URL(url);

      // console.log(u.pathname);

      let pathname = u.pathname;
      let domain = u.hostname;

      let lastpart = pathname.split(/[/ ]+/).pop();
      var toremove = pathname.replace(lastpart, '');

      console.log(toremove);
      console.log(domain);

      // get the content
      const c = () => {
        return new Promise(function(resolve, reject) {
          read(url, function(err, article, meta) {
            // Main Article
            // console.log(article.content);
            // check if there is html
            if (article !== undefined) {
              if (article.html !== undefined && _.isEmpty(article.html)) {
                const html = article.html;

                if (article.content !== false) {
                  var filteredcontent = '';
                  // To remove now
                  if (pathname.startsWith('/')) {
                    // Remove the shit and add the slash
                    filteredcontent = article.content.replace(new RegExp(toremove, 'g'), '/');
                  } else {
                    // Else no slash
                    filteredcontent = article.content.replace(new RegExp(toremove, 'g'), '');
                  }

                  // return res.status(200).json({status: "200", data: getContent(filteredcontent, domain)});
                  // return {};

                  // article.content = filteredcontent;
                  // console.log(article);

                  resolve({ originalHtml: html, content: filteredcontent, title: article.title });

                  /**
                                         if(c){
                  return  res.status(200).json({status: "200", data: c});
                }
                                         else{
                    return res.status(200).json({status: "200", data: {}});
                }

                                         * */

                  // Do get the elements using jsdom here
                } else {
                  // return  res.status(200).json({status:"200", data:{}})
                  reject({});
                }
                article.close();
              } else {
                // return  res.status(200).json({status:"200", data:{}})
                reject({});
              }
            } else {
              // return  res.status(200).json({status:"200", data:{}})
              reject({});
            }
            // Title
            // console.log(article.title);
            // console.log(meta);

            // HTML Source Code
            // console.log(article.html);

            // DOM
            // console.log(article.document);

            // Response Object from Request Lib
            // console.log(meta);

            // Close article to clean up jsdom and prevent leaks
            // article.close();
          });
        });
      };

      c
        .then(function(article) {
          // console.log(filtered);

          return getContent(article.content, article.originalHtml, url, article.title, publisher);
        })
        .catch(function(er) {
          console.log('Error getting article content');
          rej({ status: '400', msg: 'RESULT empty', data: {} });
        })
        .then(function(result) {
          if (result !== {}) {
            // console.log(result.title);
            res({ status: '200', data: result.content, title: result.title, img: result.img });
          } else {
            rej({ status: '400', msg: 'RESULT empty', data: {} });
          }
        })
        .catch(function(e) {
          console.log('Error getting blog content = ' + url);
          rej({ status: '400', msg: 'RESULT empty', data: {} });
        });
    } else {
      rej({ status: '400', msg: 'URL empty' });
    }
  }));
};

const scrapUrlPub = async site => {
  const url = site.url;
  // let publisher = site.publisher;

  const urlParse = new URL(url);
  const domain = urlParse.hostname;

  let publisher = await NewsPub.findOne({
    'meta.website': { $regex: `.*${  domain  }.*` },
  }).exec();
  let article;
  try {
    article = await getScraphtml(url);
    // console.log(article);
    console.log('Article is complete now');
  } catch (e) {
    console.log('Error getting article');
    console.log(e);
    return { status: '400', url, msg: 'Error getting article', data: {} };
  }

  let result;
  try {
    result = await getContent(article.content, article.originalHtml, url, article.title, publisher);
    console.log('Completed getting content from getContent');
    // console.log(result.content);

    // TODO SAVE OR UPDATE ARTICLE
    return {
      status: '200',
      publisher,
      url,
      data: result.content,
      title: result.title,
      img: result.img,
      author: result.author,
      des: result.des,
    };
  } catch (error) {
    console.log(error);
    console.log('Error getting complete content');
    return { status: '400', url, msg: 'Error getting complete content', data: {} };
  }
};

function getScraphtml(url) {
  // get the content
  return new Promise(((resolve, reject) => {
    let u = new URL(url);

    // console.log(u.pathname);

    let pathname = u.pathname;
    let domain = u.hostname;

    let lastpart = pathname.split(/[/ ]+/).pop();
    var toremove = pathname.replace(lastpart, '');

    read(url, function(err, article, meta) {
      if (article !== undefined) {
        if (article.html !== undefined && !_.isEmpty(article.html)) {
          const html = article.html;

          if (article.content !== undefined && article.content !== false) {
            var filteredcontent = '';
            // To remove now
            if (pathname.startsWith('/')) {
              // Remove the shit and add the slash
              filteredcontent = article.content.replace(new RegExp(toremove, 'g'), '/');
            } else {
              // Else no slash
              filteredcontent = article.content.replace(new RegExp(toremove, 'g'), '');
            }

            resolve({ originalHtml: html, content: filteredcontent, title: article.title });
          } else {
            // return  res.status(200).json({status:"200", data:{}})
            reject({ error: 'Article content undefined' });
          }
          article.close();
        } else {
          // return  res.status(200).json({status:"200", data:{}})
          reject({ error: 'Article HTML undefined' });
        }
      } else {
        // return  res.status(200).json({status:"200", data:{}})
        reject({ error: 'Article object undefined' });
      }
    });
  }));
}

function scrapByUrlStl(site) {
  const url = site.url;
  const publisher = site.publisher;

  const cx = async () => {


        //var url = (req.body.url || "http://igihe.com/politiki/amakuru/article/perezida-kagame-yasabye-ko-ikigo-cy-ibarurishamibare-cyajya-gisuzuma-imihigo");

        //IF url is not empty
        if (!_.isEmpty(url)) {
            var u = new URL(url);

            //console.log(u.pathname);

            var pathname = u.pathname;
            var domain = u.hostname;

            var lastpart = pathname.split(/[/ ]+/).pop();
            var toremove = pathname.replace(lastpart, "");

            console.log(toremove);
            console.log(domain);

            var article = await  read(url, function (err, article, meta) {
                    // Main Article
                    //console.log(article.content);
                    //check if there is html
                    if(article!== undefined) {
                        if (article.html !== undefined && _.isEmpty(article.html)) {
                            let html = article.html;

                            if (article.content !== false) {


                                var filteredcontent = "";
                                //To remove now
                                if (pathname.startsWith('/')) {
                                    //Remove the shit and add the slash
                                    filteredcontent = article.content.replace(new RegExp(toremove, 'g'), "/");
                                }
                                else {
                                    //Else no slash
                                    filteredcontent = article.content.replace(new RegExp(toremove, 'g'), "");
                                }

                                return ({originalHtml: html, content: filteredcontent, title: article.title});


                            }
                            else {
                                //return  res.status(200).json({status:"200", data:{}})
                                return ({});
                            }
                            article.close();
                        }
                        else {
                            //return  res.status(200).json({status:"200", data:{}})
                            return ({});
                        }
                    }
                    else {
                        //return  res.status(200).json({status:"200", data:{}})
                        return ({});
                    }

                });
            if(!_.isEmpty(article)){
                var result = await getContent(article.content, article.originalHtml, url, article.title, publisher)
                if(result) {
                  return ({status: "200", data: result.content, title: result.title, img: result.img});
                }
                
                    return({status: "400", msg: "RESULT empty", data: {}});
                

            }
            else{
                return({status: "400", msg: "URL empty"});
            }

        }
        
            return({status: "400", msg: "URL empty"});
        



    };

  if (!_.isEmpty(cx)) {
    // Check if article exists
    NewsArt.findOne({ 'meta.url': url }).exec((error, firstArt) => {
      if (firstArt) {
        // Update article now
        const content = [];
        content.push({
          typo: 'html',
          data: article.data,
        });

        firstArt.raw = content;
        firstArt.image.banner = article.img;
        firstArt.meta.title_og = article.title;
        firstArt.meta.des = article.des || '';
        firstArt.meta.author = article.author || '';

        firstArt.save(function(err, sav) {
          if (!error) {
            console.log('Success updating article');
          } else {
            console.log('Failed to update article');
          }
        });
      } else {
        // Create new article
        const content = [];
        content.push({
          typo: 'html',
          data: article.data,
        });
        // Save the article
        const newArt = new NewsArt({
          // Publisher ID
          pubid: publisher._id,

          // Article ID
          artid: config.rand(20),

          raw: content,

          // Basic info
          meta: {
            // twitter, facebook, ig, website,
            // title_sm:  "",
            title_og: article.title,
            des: article.des || '',

            url: url,
            author: article.author || '',
          },

          ut: config.getUt(new Date()),

          // Featured
          featured: true,

          // Article banner images
          image: {
            banner: article.img,
          },
        });

        newArt.save(function(error, saved) {
          if (!error) {
            console.log('Success saving article');
          } else {
            console.log('Failed to save article');
          }
        });
      }
    });
  } else {
    console.log('content is null');
  }
}
module.exports = {
  scrapFeaturedUrls: scrapFeatureUrls,
  getContent,
  scrapByUrl,
  scrapByUrlStl,
  scrapUrlPub,
};
