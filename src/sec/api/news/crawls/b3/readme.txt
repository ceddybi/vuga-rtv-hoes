#######################################
#######################################
#######################################
Run application.py with 1 argument.
It should be url of blog post.

Example:
python application.py http://igihe.com/abantu/kubaho/urukundo/article/imyitwarire-y-umusore-yagaragariza-umukobwa-hakiri-kare-ko-azamubera-ikigusha

Output you can find in folder and file in folder results like "/result/sitename.com/post_url.json".

#######################################
#######################################
#######################################
Run latest.py without arguments.

Example:
python latest.py

Output you can find in folder 'result' and file "result.json".