# xpathes for ukwezi.com
url = "http://www.ukwezi.com/"
url2 = "http://ukwezi.com"

title = "//div[@class='haberBaslik']/h1/text()"
description = "//div[@class='haberText']//p/strong/text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//meta[@name='author']/@content"
featured_image = "//image"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='haberText']/p | //div[@class='haberText']/p/strong | //div[@class='haberText']/dl | //div[@class='haberText']/ul/li | //div[@class='haberText']/iframe"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./alt"
data_item_videos = "./video"
data_item_iframes = "./@src | ./iframe/@src"

