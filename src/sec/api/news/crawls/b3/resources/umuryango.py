# main page url
url = "http://umuryango.rw/"
url2 = "http://www.umuryango.rw"

title = "//h1[@class='artticle title-xlarge']/text()"
description = "//div[@class='wrapart']/p[1]/strong/text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//span[@class='vcard author']/a/text()"
featured_image = "//img[contains(@class, 'artlogo')]/@src"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='wrapart']/p[position()>1] | //div[@class='wrapart']/div[@class='video-container'] | //div[@class='wrapart']/iframe"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./caption"
data_item_videos = ".//a[contains(@href, 'youtube.com')]/@href"
data_item_iframes = "./@src | ./iframe/@src"

