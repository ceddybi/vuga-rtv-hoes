# xpathes for kigalitoday.com
url = "http://www.kigalitoday.com"
url2 = "http://kigalitoday.com"

title = "//h1[@itemprop='headline']/text()"
description = "//h2[@itemprop='description']/p/text()"
meta_description = "//meta[@property='og:description']/@content"

author = "//div[@itemprop='author']/span/text()"
featured_image = "//image"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@itemprop='articleBody']/p | //div[@itemprop='articleBody']/p/strong | //div[@itemprop='articleBody']/div | //div[@itemprop='articleBody']/span | //div[@itemprop='articleBody']/dl | //div[@itemprop='articleBody']/iframe"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./@alt"
data_item_videos = "//video"
data_item_iframes = ".//iframe/@src | ./@src"

