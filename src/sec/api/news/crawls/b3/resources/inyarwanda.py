# main page url
url = "http://inyarwanda.com/"
url2 = "http://www.inyarwanda.com"

title = "//div[@class='artheader']/h1/text()"
description = "//div[@class='artheader']/div[@class='artlead']/text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//div[@class='artheader']/div[@class='artinfo']//a/text()"
featured_image = "//div[@class='artlogo']/a/@href"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='artbody']/div[@class='content-wrapper']/p"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@data-src"
data_item_img_caption = "./@alt"
data_item_videos = ".//a[contains(@href, 'youtube.com/')]/@href"
data_item_iframes = "./frame"

