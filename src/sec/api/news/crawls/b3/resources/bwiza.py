# xpathes for bwiza.com
url = "http://www.bwiza.com"
url2 = "http://bwiza.com"

title = "//h1[@class='post-tile entry-title']/text()"
description = "//div[@class='entry-content']/p[1]//text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//author"
featured_image = "//div[@class='feature-img']/img/@src"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='entry-content']/p[position()>1] | //div[@class='entry-content']/p[position()>1]/span | //div[@class='entry-content']/div[contains(@id, 'attachment')] | //div[@class='entry-content']/div[contains(@class, 'wp-audio-shortcode')]"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./parent::div/p[@class='wp-caption-text']/text()"
data_item_videos = ".//video"
data_item_iframes = "./iframe/@src"

