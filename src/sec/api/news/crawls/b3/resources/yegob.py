# xpathes for yegob.rw
url = "http://www.yegob.rw/"
url2 = "http://yegob.rw"

title = "//header[@class='td-post-title']/h1/text()"
description = "//div[@class='wrapart']/p[1]/strong/text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//div[@class='td-post-author-name']/a/text()"
featured_image = "//img[contains(@class, 'artlogo')]/@src"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='td-post-content']/p | //div[@class='td-post-content']/div[contains(@class, 'audioplayer')]"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./@alt"
data_item_videos = "./@data-source"
data_item_iframes = ".//iframe/@src | .//iframe//a/@href"

