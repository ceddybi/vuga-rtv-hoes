# xpathes for umuseke.rw
url = "http://www.umuseke.rw/"
url2 = "http://umuseke.rw"

title = "//header[@id='post-header']/h1/text()"
description = "//div[@class='content-main']/p[1]//text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//header[@id='post-header']//span[@itemprop='author']/a/text()"
featured_image = "//div[@class='content-main']/div[1]/@data-orig-file"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@id='content-main']/p[position()>1] | //div[@class='content-main']/p[position()>1] | //div[contains(@class, 'attachment')] | //div[@id='content-main']/p[position()>1]/strong/* | //div[@class='content-main']/p[position()>1]/strong/*"

data_item_text = "./text()"
data_item_quote = "./em/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./@alt"
data_item_videos = "./video"
data_item_iframes = ".//iframe/@src"

