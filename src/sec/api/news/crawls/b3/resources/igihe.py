# xpathes for igihe.com
url = "http://igihe.com/"
url2 = "http://www.igihe.com"

title = "//div[@class='wrap-article']/h3[@class='title-article']/text()"
description = "//div[@class='text-article margintop10']/div/div[2]//p/text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//span[@class='vcard author']/a/text()"
featured_image = "//div[@class='text-article margintop10']/div/div[1]/img/@src"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='fulltext margintop10']/p | //div[@class='fulltext margintop10']/p//* | //div[@class='fulltext margintop10']/center | //div[@class='fulltext margintop10']/iframe"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./@alt"
data_item_videos = "./video"
data_item_iframes = "./@src"

