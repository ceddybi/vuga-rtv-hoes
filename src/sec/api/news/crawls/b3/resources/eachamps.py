# xpathes for Eachamps.rw
url = "http://www.eachamps.rw"
url2 = "http://eachamps.rw"

title = "//div[@class='article-ttle']/h1/text()"
description = "//div[@class='article-desc']/p[1]//text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//div[@class='pull-left']/label[@class='art-auth']/text()"
featured_image = "//div[@class='art-bottom-left']/img/@src"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='article-desc']/p[position()>1] | //div[@class='article-desc']/p[position()>1]/span/span | //div[@class='article-desc']/p[position()>1]/span/span/*"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "//caption"
data_item_videos = "//videos"
data_item_iframes = "./iframe/@src"

