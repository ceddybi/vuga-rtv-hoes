# xpathes for newtimes.co.rw
url = "http://www.newtimes.co.rw/"
url2 = "http://newtimes.co.rw"

title = "//div[@class='the-article']/h1/text() | //div[@class='the-article']/h2/text() | //div[@class='the-article']/h3/text()"
description = "//div[@class='the-article']//div[@class='inner-caption']/text()"
meta_description = "//meta[@property='og:description']/@content"
author = "//div[@class='the-article']/div[@class='article-byline']/span/text()"
featured_image = "//div[@class='the-article']//img[1]/@src"
meta_featured_image = "//meta[@property='og:image']/@content"

data_entry = "//div[@class='the-article']//div/p | //div[@class='the-article']//div/div[@class='video-wrapper'] | //div[@class='the-article']//div/div[@class='pos-image']"

data_item_text = "./text()"
data_item_quote = "./i/text()"
data_item_imgs = ".//img"
data_item_img_src = "./@src"
data_item_img_caption = "./parent/div[@class='inner-caption']/text()"
data_item_videos = ".//iframe/@src"
data_item_iframes = "./frame"

