import os
import json
import requests
from lxml import html

# files with xpathes
from resources import inyarwanda, kigalitoday, bwiza, igihe, newtimes, eachamps, ukwezi, umuryango, yegob, umuseke
from resources.common import latest_pathes as latest
from resources.common import featured_path as featured


def scrap_all_featured():
    main_pages = [(bwiza.url2, featured.bwiza), (eachamps.url2, featured.eachamps), (igihe.url2, featured.igihe), (inyarwanda.url2, featured.inyarwanda), (kigalitoday.url2, featured.kigalitoday), (newtimes.url2, featured.newtimes), (ukwezi.url2, featured.ukwezi), (umuryango.url2, featured.umuryango), (umuseke.url2, featured.umuseke), (yegob.url2, featured.yegob)]
#    main_pages = [(umuseke.url2, featured.umuseke),(inyarwanda.url2, featured.inyarwanda)]
    json_data = list()

    general_count = 0
    error_counter = 0
    for url, xpath in main_pages:
        links = scrap_full_path_links(url, xpath)
        general_count += len(links)
        for link in links:
            status, data = scrap_by_url(link)
            if not status:
                error_counter += 1
            json_data.append(json.loads(data))

    result = json.dumps(json_data)
    save_latest(result)
    stats = dict()
    stats["success"] = general_count - error_counter
    stats["error"] = error_counter
    print json.dumps(stats)



def scrap_latest_by_url_path(blog_url, xpath):
    page = requests.get(blog_url)
    tree = html.fromstring(page.content)
    links = tree.xpath(xpath)
    result = list()
    if blog_url in (igihe.url, newtimes.url, inyarwanda.url, umuryango.url, ukwezi.url):
        for link in links:
            result.append(str(blog_url+link))
    elif blog_url in (kigalitoday.url):
        for link in links:
            result.append(str(blog_url + "/" + link))
    else:
        return links
    return result


def scrap_full_path_links(blog_url, xpath):
    page = requests.get(blog_url)
    tree = html.fromstring(page.content)
    links = tree.xpath(xpath)
    result = list()
    for link in links:
        if link.startswith('http'):
            result.append(str(link))
        elif link.startswith('/'):
            result.append(str(blog_url+link))
        else:
            result.append(str(blog_url+'/'+link))
    return result


def scrap_all_latest():
    main_pages = [(bwiza.url, latest.bwiza), (eachamps.url, latest.eachamps), (igihe.url, latest.igihe), (inyarwanda.url, latest.inyarwanda), (kigalitoday.url, latest.kigalitoday), (newtimes.url, latest.newtimes), (ukwezi.url, latest.ukwezi), (umuryango.url, latest.umuryango), (umuseke.url, latest.umuseke), (yegob.url, latest.yegob)]

    json_data = list()

    general_count = 0
    error_counter = 0
    for url, xpath in main_pages:
        links = scrap_latest_by_url_path(url, xpath)
        general_count += len(links)
        for link in links:
            status, data = scrap_by_url(link)
            if not status:
                error_counter += 1
            json_data.append(json.loads(data))

    result = json.dumps(json_data)
    save_latest(result)
    stats = dict()
    stats["success"] = general_count - error_counter
    stats["error"] = error_counter
    print json.dumps(stats)


def scrap_by_url(blog_url):
    status = True

    concat_img = ""
    meta_description = ""
    meta_featured_image = ""
    title = ""
    description = ""
    author = ""
    featured_image = ""
    data_entry = ""
    data_item_text = ""
    data_item_quote = ""
    data_item_imgs = ""
    data_item_img_src = ""
    data_item_img_caption = ""
    data_item_videos = ""
    data_item_iframes = ""

    json_data = {}
    json_data["url"] = blog_url

    try:
        if blog_url.startswith(igihe.url) or blog_url.startswith(igihe.url2):
            concat_img = igihe.url2
            title = igihe.title
            description = igihe.description
            meta_description = igihe.meta_description
            author = igihe.author
            featured_image = igihe.featured_image
            meta_featured_image = igihe.meta_featured_image
            data_entry = igihe.data_entry
            data_item_text = igihe.data_item_text
            data_item_quote = igihe.data_item_quote
            data_item_imgs = igihe.data_item_imgs
            data_item_img_src = igihe.data_item_img_src
            data_item_img_caption = igihe.data_item_img_caption
            data_item_videos = igihe.data_item_videos
            data_item_iframes = igihe.data_item_iframes
        elif blog_url.startswith(umuryango.url) or blog_url.startswith(umuryango.url2):
            concat_img = umuryango.url2
            title = umuryango.title
            description = umuryango.description
            meta_description = umuryango.meta_description
            author = umuryango.author
            featured_image = umuryango.featured_image
            meta_featured_image = umuryango.meta_featured_image
            data_entry = umuryango.data_entry
            data_item_text = umuryango.data_item_text
            data_item_quote = umuryango.data_item_quote
            data_item_imgs = umuryango.data_item_imgs
            data_item_img_src = umuryango.data_item_img_src
            data_item_img_caption = umuryango.data_item_img_caption
            data_item_videos = umuryango.data_item_videos
            data_item_iframes = umuryango.data_item_iframes
        elif blog_url.startswith(inyarwanda.url) or blog_url.startswith(inyarwanda.url2):
            concat_img = inyarwanda.url2
            title = inyarwanda.title
            description = inyarwanda.description
            meta_description = inyarwanda.meta_description
            author = inyarwanda.author
            featured_image = inyarwanda.featured_image
            meta_featured_image = inyarwanda.meta_featured_image
            data_entry = inyarwanda.data_entry
            data_item_text = inyarwanda.data_item_text
            data_item_quote = inyarwanda.data_item_quote
            data_item_imgs = inyarwanda.data_item_imgs
            data_item_img_src = inyarwanda.data_item_img_src
            data_item_img_caption = inyarwanda.data_item_img_caption
            data_item_videos = inyarwanda.data_item_videos
            data_item_iframes = inyarwanda.data_item_iframes
        elif blog_url.startswith(ukwezi.url) or blog_url.startswith(ukwezi.url2):
            concat_img = ukwezi.url2
            title = ukwezi.title
            description = ukwezi.description
            meta_description = ukwezi.meta_description
            author = ukwezi.author
            featured_image = ukwezi.featured_image
            meta_featured_image = ukwezi.meta_featured_image
            data_entry = ukwezi.data_entry
            data_item_text = ukwezi.data_item_text
            data_item_quote = ukwezi.data_item_quote
            data_item_imgs = ukwezi.data_item_imgs
            data_item_img_src = ukwezi.data_item_img_src
            data_item_img_caption = ukwezi.data_item_img_caption
            data_item_videos = ukwezi.data_item_videos
            data_item_iframes = ukwezi.data_item_iframes
        elif blog_url.startswith(eachamps.url) or blog_url.startswith(eachamps.url2):
            concat_img = eachamps.url2
            title = eachamps.title
            description = eachamps.description
            meta_description = eachamps.meta_description
            author = eachamps.author
            featured_image = eachamps.featured_image
            meta_featured_image = eachamps.meta_featured_image
            data_entry = eachamps.data_entry
            data_item_text = eachamps.data_item_text
            data_item_quote = eachamps.data_item_quote
            data_item_imgs = eachamps.data_item_imgs
            data_item_img_src = eachamps.data_item_img_src
            data_item_img_caption = eachamps.data_item_img_caption
            data_item_videos = eachamps.data_item_videos
            data_item_iframes = eachamps.data_item_iframes
        elif blog_url.startswith(umuseke.url) or blog_url.startswith(umuseke.url2):
            concat_img = umuseke.url2
            title = umuseke.title
            description = umuseke.description
            meta_description = umuseke.meta_description
            author = umuseke.author
            featured_image = umuseke.featured_image
            meta_featured_image = umuseke.meta_featured_image
            data_entry = umuseke.data_entry
            data_item_text = umuseke.data_item_text
            data_item_quote = umuseke.data_item_quote
            data_item_imgs = umuseke.data_item_imgs
            data_item_img_src = umuseke.data_item_img_src
            data_item_img_caption = umuseke.data_item_img_caption
            data_item_videos = umuseke.data_item_videos
            data_item_iframes = umuseke.data_item_iframes
        elif blog_url.startswith(yegob.url) or blog_url.startswith(yegob.url2):
            concat_img = yegob.url2
            title = yegob.title
            description = yegob.description
            meta_description = yegob.meta_description
            author = yegob.author
            featured_image = yegob.featured_image
            meta_featured_image = yegob.meta_featured_image
            data_entry = yegob.data_entry
            data_item_text = yegob.data_item_text
            data_item_quote = yegob.data_item_quote
            data_item_imgs = yegob.data_item_imgs
            data_item_img_src = yegob.data_item_img_src
            data_item_img_caption = yegob.data_item_img_caption
            data_item_videos = yegob.data_item_videos
            data_item_iframes = yegob.data_item_iframes
        elif blog_url.startswith(bwiza.url) or blog_url.startswith(bwiza.url2):
            concat_img = bwiza.url2
            title = bwiza.title
            description = bwiza.description
            meta_description = bwiza.meta_description
            author = bwiza.author
            featured_image = bwiza.featured_image
            meta_featured_image = bwiza.meta_featured_image
            data_entry = bwiza.data_entry
            data_item_text = bwiza.data_item_text
            data_item_quote = bwiza.data_item_quote
            data_item_imgs = bwiza.data_item_imgs
            data_item_img_src = bwiza.data_item_img_src
            data_item_img_caption = bwiza.data_item_img_caption
            data_item_videos = bwiza.data_item_videos
            data_item_iframes = bwiza.data_item_iframes
        elif blog_url.startswith(newtimes.url) or blog_url.startswith(newtimes.url2):
            concat_img = newtimes.url2
            title = newtimes.title
            description = newtimes.description
            meta_description = newtimes.meta_description
            author = newtimes.author
            featured_image = newtimes.featured_image
            meta_featured_image = newtimes.meta_featured_image
            data_entry = newtimes.data_entry
            data_item_text = newtimes.data_item_text
            data_item_quote = newtimes.data_item_quote
            data_item_imgs = newtimes.data_item_imgs
            data_item_img_src = newtimes.data_item_img_src
            data_item_img_caption = newtimes.data_item_img_caption
            data_item_videos = newtimes.data_item_videos
            data_item_iframes = newtimes.data_item_iframes
        elif blog_url.startswith(kigalitoday.url) or blog_url.startswith(kigalitoday.url2):
            concat_img = kigalitoday.url2
            title = kigalitoday.title
            description = kigalitoday.description
            meta_description = kigalitoday.meta_description
            author = kigalitoday.author
            featured_image = kigalitoday.featured_image
            meta_featured_image = kigalitoday.meta_featured_image
            data_entry = kigalitoday.data_entry
            data_item_text = kigalitoday.data_item_text
            data_item_quote = kigalitoday.data_item_quote
            data_item_imgs = kigalitoday.data_item_imgs
            data_item_img_src = kigalitoday.data_item_img_src
            data_item_img_caption = kigalitoday.data_item_img_caption
            data_item_videos = kigalitoday.data_item_videos
            data_item_iframes = kigalitoday.data_item_iframes

        page = requests.get(blog_url)
        tree = html.fromstring(page.content)

        json_data["title"] = "".join(tree.xpath(title))

        desc = tree.xpath(description)
        if desc:
            json_data["description"] = desc[0]
        else:
            desc = tree.xpath(meta_description)
            if desc:
                json_data["description"] = desc[0]

        auth = tree.xpath(author)
        if auth:
            json_data["author"] = auth[0]

        image = tree.xpath(featured_image)
        if image:
            if image[0]:
                if image[0].startswith('http'):
                    json_data["featured_image"] = image[0]
                elif image[0].startswith('/'):
                    json_data["featured_image"] = concat_img + image[0]
                else:
                    json_data["featured_image"] = concat_img + '/'+image[0]
        else:
            image = tree.xpath(meta_featured_image)
            if image:
                if image[0].startswith('http'):
                    json_data["featured_image"] = image[0]
                elif image[0].startswith('/'):
                    json_data["featured_image"] = concat_img + image[0]
                else:
                    json_data["featured_image"] = concat_img + '/'+image[0]

        json_data["data"] = []
        data = tree.xpath(data_entry)

        for paragraph in data:
            json_data_entry = {}
            text = paragraph.xpath(data_item_text)
            quote = paragraph.xpath(data_item_quote)
            imgs = paragraph.xpath(data_item_imgs)
            videos = paragraph.xpath(data_item_videos)
            iframes = paragraph.xpath(data_item_iframes)
            if text:
                json_data_entry["text"] = "".join(text)
            if quote:
                json_data_entry["quote"] = quote[0]
            json_data_entry["img"] = list()
            if imgs:
                for img in imgs:
                    img_entry = dict()
                    src = img.xpath(data_item_img_src)
                    if src:
                        if src[0].startswith('http'):
                            img_entry["src"] = src[0]
                        elif src[0].startswith('/'):
                            img_entry["src"] = concat_img + src[0]
                        else:
                            img_entry["src"] = concat_img + '/' + src[0]
                    caption = img.xpath(data_item_img_caption)
                    if caption:
                        img_entry["caption"] = caption[0]
                    json_data_entry["img"].append(img_entry)
            json_data_entry["video"] = list()
            if videos:
                for vd in videos:
                    entry = dict()
                    entry["src"] = vd
                    json_data_entry["video"].append(entry)
            json_data_entry["iframe"] = list()
            if iframes:
                for iframe in iframes:
                    entry = dict()
                    entry["src"] = iframe
                    json_data_entry["iframe"].append(entry)

            json_data["data"].append(json_data_entry)
    except Exception as ex:
        status = False

    result = json.dumps(json_data)
    return status, result


def _create_filename(blog_url):
    ext = ".json"
    start = blog_url.rindex('/', 0, -1)
    end = blog_url.rindex('/')
    if end != start:
        return blog_url[start+1: -1] + ext
    else:
        return blog_url[start+1:] + ext


def _create_folder(blog_url):
    start = blog_url.index('//')
    end = blog_url.index('/', 7)
    return blog_url[start+2:end]


def save(blog_url, result):
    folder = "result"
    subfolder = _create_folder(blog_url)
    filename = _create_filename(blog_url)

    path = os.path.join(folder, subfolder)
    if not os.path.exists(path):
        os.makedirs(path)
    with open(os.path.join(path, filename), 'w') as f:
        f.write(result)


def save_latest(result):
    path = "result"
    if not os.path.exists(path):
        os.makedirs(path)
    with open(os.path.join(path, "result.json"), 'w') as f:
        f.write(result)
