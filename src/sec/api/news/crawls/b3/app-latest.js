const exec = require('child_process').exec;
function getLatest(callback) {
    exec('python latest.py',{maxBuffer: 1024 * 500000},
        function (e, stdout, stderr) {
            if (e) {
                console.error(e);
                callback();
            }
            else {

                var msgData;
                try {
                    msgData = JSON.parse(stdout);
                    //console.log(msgData);
                    callback(msgData);
                } catch (e) {
                    console.error(e);
                    callback();
                }
            }
        });

}

module.exports = {
    getLatest:getLatest
}