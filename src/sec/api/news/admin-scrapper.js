/**
 * Created by vuga on 9/22/17.
 *
 * EXPORT, CRUD ARTICLE
 * GET ONE/ALL,
 * Create, update
 */
// var pm2 = require("pm2");
const express = require('express');

const router = express.Router();
const mongoose = require('mongoose');

const NewsArt = mongoose.model('newsArt');
const NewsPub = mongoose.model('newsPub');

const _ = require('lodash');

const read = require('../../../node-readability/src/readability');

const { URL } = require('url');

const { saveOrUpdateArticle } = require('./article/article');

const {
  getContent,
  scrapFeaturedUrls,
  scrapByUrl,
  scrapUrlPub,
  scrapByUrlStl,
} = require('./viewbot/bot');

function restart() {
  /**
    console.log("Restarting now");
    console.log("Delaying 3");
    setTimeout(function() {
        console.log("Resetting now");
        pm2.restart('dsc', function() {});
        pm2.reset('dsc', function() {});
    }, 3000);
    * */
}

async function bindZombieNow() {
  let zombie;
  try {
    zombie = await startZomie();
    console.log('Zombie completed now');
    restart();
    return 0;
  } catch (error) {
    console.log('Starting zombie from within failed', error);
    restart();
    return 0;
  }
}
/** 
setInterval(async function () {
    // now = moment().format("h:mm:ss");
    // if (now === midn) {
    // Mid night functions
    //Get featured after every hour
    //getFeatured();
    //}
    //console time
    //1000 = 1sec
    // 1 hour = 3600
    // return bindZombieNow();


}, (1000 * 60 * 60));
* */

router.post('/get', async (req, res) => {
  if (_.isEmpty(req.body.url)) {
    req.body.url = undefined;
  }

  var url =
    req.body.url ||
    'http://igihe.com/politiki/amakuru/article/perezida-kagame-yasabye-ko-ikigo-cy-ibarurishamibare-cyajya-gisuzuma-imihigo';

  let u = new URL(url);

  // console.log(u.pathname);

  let pathname = u.pathname;
  let domain = u.hostname;

  let lastpart = pathname.split(/[/ ]+/).pop();
  var toremove = pathname.replace(lastpart, '');

  console.log(toremove);
  console.log(domain);
  console.log(url);

  try {
    const c = new Promise(((resolve, reject) => {

            read(url, function (err, article, meta) {
                // Main Article
                //console.log(article.content);


                if (article) {
                    if (article.content !== false) {


                        var filteredcontent = "";
                        //To remove now
                        if (pathname.startsWith('/')) {
                            //Remove the shit and add the slash
                            filteredcontent = article.content.replace(new RegExp(toremove, 'g'), "/");
                        }
                        else {
                            //Else no slash
                            filteredcontent = article.content.replace(new RegExp(toremove, 'g'), "");
                        }


                        //return res.status(200).json({status: "200", data: getContent(filteredcontent, domain)});
                        // return {};

                        //article.content = filteredcontent;
                        //console.log(article);

                        resolve({content:filteredcontent, title:article.title});

                        /**
                         if(c){
                  return  res.status(200).json({status: "200", data: c});
                }
                         else{
                    return res.status(200).json({status: "200", data: {}});
                }

                         **/


                        //Do get the elements using jsdom here


                    }
                    else {
                        //return  res.status(200).json({status:"200", data:{}})
                        reject({});
                    }
                    article.close;
                }
                else {
                    //return  res.status(200).json({status:"200", data:{}})
                    reject({});
                }
                // Title
                //console.log(article.title);
                // console.log(meta);

                // HTML Source Code
                //console.log(article.html);

                // DOM
                //console.log(article.document);

                // Response Object from Request Lib
                //console.log(meta);

                // Close article to clean up jsdom and prevent leaks
                //article.close();
            });
        }));


    c
      .then(function(article) {
        // console.log(filtered);

        // html,originalHtml, url, title, publisher
        return getContent(article.content, article.content, url, article.title, null);
      })
      .then(function(result) {
        if (result !== {}) {
          console.log(result.title);
          res
            .status(200)
            .json({ status: '200', data: result.content, title: result.title, img: result.img });
        } else {
          res.status(200).json({ status: '200', data: {} });
        }
      });
  } catch (error) {
    console.log(error);
  }

  // get the content
});

router.post('/scrap/:typo', async (req, res) => {
  const site = req.params.typo;
  console.log('website = ' + site);

  var f = await NewsPub.findOne({ 'meta.website': site }).exec();

  if (f) {
    // Get the links
    const x = scrapFeaturedUrls({
      xpath: f.meta.xpath_featured,
      url: f.meta.website,
    });

    x
      .then(function(result) {
        console.log('Complete now');
        return result;
      })
      .catch(function(error) {
        console.log('Error failed to get the links: no complete');
      })
      .then(function(links) {
        // console.log(links);
        const nowurl = links[0];
        console.log(`Now scrapping${nowurl}`)
        return scrapByUrl({
          url: nowurl,
          publisher: f,
        });
      })
      .catch(function(errorLinks) {
        console.log('Failed to scrap first link');
      })
      .then(function(scrap) {
        console.log('Scrapped content here from first link');
      });
  } else {
    console.log('Error fetching igihe');
  }
});

router.post('/test', async (req, res) => {
  let zombie;
  // let site = req.params.typo;
  // console.log("website = "+ site);
  res.status(200).json({ msg: 'IN QUEUE' });

  try {
    zombie = await startZomie();
    console.log('Zombie completed now');
    return 0;
  } catch (error) {
    console.log('Starting zombie from within failed', error);
    return 0;
  }

  [];
});

router.post('/links', async (req, res) => {
  // let site = req.params.typo;
  // console.log("website = "+ site);
  const site = req.body.url;
  const path = req.body.xpath;

  console.log(site + ' ' + path);

  try {
    let con = await scrapFeaturedUrls({
      xpath: path,
      // xpath: f.meta.xpath_featured,
      url: site,
    });

    return res.status(200).json(con);
  } catch (e) {
    return res.status(200).json([]);
  }
});

// Will remove all falsy values: undefined, null, 0, false, NaN and "" (empty string)
function cleanArray(actual) {
  const newArray = new Array();
  for (let i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}

async function nanoBot(pubs) {
  // Get them links from all publishers

  let mergedurls;
  let publistArray;
  try {
    // /**
    publistArray = await Promise.all(
      pubs.map(async f => {
        // return async () => {
        try {
          return await scrapFeaturedUrls({
            // xpath: f.meta.xpath_featured + " | " + f.meta.xpath_latest,
            xpath: f.meta.xpath_featured,
            url: f.meta.website,
          });
        } catch (error) {
          console.log(error);
          console.log('Error getting links from', {
            site: f.meta.website,
            xpath: f.meta.xpath_featured,
          });
          return [];
        }

        // }
      })
    );
    // **/
    // publistArray = [[],['http://www.yegob.rw/kera-kabaye-king-james-yahishuye-icyamujyanye-muri-amerika-anakomoza-ku-mukunzi-we/']]

    // Merged urls
    mergedurls = cleanArray([].concat.apply([], publistArray));
    console.log(mergedurls.length);

    // Loop thru all the urls now and return a promise
    return await Promise.all(
      mergedurls.map(async url => {

                // return async () => {
                    // scrap the bitch
                    var result;
                    try {
                        result = await scrapUrlPub({url});

                        if(result.status === "200") {
                            if (!result.title.match(/.*(503|404|403|Site under construction|403 Forbidden|Service Unavailable|Internal Server|Server Error).*/)) {
                                //No error in title

                                if(!_.isEmpty(result.title)) {
                                    /**
                                     console.log("Saving article now");
                                     console.log(result)
                                     console.log(result.title)
                                     console.log(result.img)
                                     console.log(result.data)
                                     **/
                                    return await saveOrUpdateArticle({
                                        status: "200",
                                        publisher: result.publisher,
                                        url: result.url,
                                        data: result.data,
                                        title: result.title,
                                        img: result.img,
                                        author: result.author,
                                        des: result.des
                                    });

                                }
                                
                                    console.log("Error in article title, null title");
                                    return {};
                                
                            }
                            
                                console.log("Error in article title, skipping it now");
                                return {};
                            
                        } else {

                            console.log("Error fetching article");
                            return result;
                        }
                    }
                    catch (error) {
                        console.log(error);
                        console.log("Error fetching article from", {site: url});
                        return {};
                    }

               // }


            })
    );
  } catch (error) {
    console.log(error);
    return {};
  }
}

async function startZomie() {
  let ff;
  try {
    ff = await NewsPub.find({ available: true }).exec();

    // return await testing(ff);
    return await nanoBot(ff);
  } catch (error) {
    console.log(error);
    console.log('Starting zombie failed');
    return {};
  }
}

module.exports = router;
