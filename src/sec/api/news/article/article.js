/**
 * Created by vuga on 9/22/17.
 *
 * EXPORT, CRUD ARTICLE
 * GET ONE/ALL,
 * Create, update
 */

const CB = require('../../static/cb');
const config = require('../../static/variables');
const mongoose = require('mongoose');

const _ = require('lodash');

const NewsArt = mongoose.model('newsArt');
const NewsPub = mongoose.model('newsPub');

const migaArt = require('../miga-articles');
const latestBot = require('../crawls/b3/app-latest');
const featuredBot = require('../crawls/b3/app-featured');
const fs = require('fs');

function migrateFromFile() {
  NewsArt.find({}, (error, arts) => {
    if (arts.length > 3) {
      console.log('Articles already available');
    } else {
      const filetoJson = require('../crawls/b3/result/result.json');

      filetoJson.map(json => {
        if (json) {
          const newArt = {
            // Publisher ID
            // pubid : art.id,

            // Article banner images
            image: {
              banner: json.featured_image,
            },

            // Basic info
            meta: {
              title_og: json.title,
              des: json.description,

              url: json.url,
              author: json.author || '',
            },

            // contents of the post
            data: json.data,

            ut: {
              dstr: `${new Date().getTime()}`,
              dnum: new Date().getTime(),
            },
          };

          // Save the article
          const a = new NewsArt(newArt);
          a.save((e, s) => {
            if (e) {
              console.log('Failed to save article');
              console.log(e);
            }
          });
        }
      });
      console.log('finished adding data');
    }
  });
}
function migrate() {
  const artsX = migaArt.getPostsUrls();

  NewsArt.find({}, (error, arts) => {
    if (arts.length > 3) {
      // Good data aleady exists
      console.log('Articles already in place');
    } else {
      // Migrate the dat now

      artsX.map(art => {
        const publisher_id = art.pubid;

        const json_content = fs.readFileSync(art.path);

        const json = JSON.parse(json_content);
        // check the post content
        if (json) {
          const newArt = {
            // Publisher ID
            pubid: art.id,

            // Article banner images
            image: {
              banner: json.featured_image,
            },

            // Basic info
            meta: {
              title_og: json.title,
              des: json.description,

              url: art.path,
              author: json.author || '',
            },

            // contents of the post
            data: json.data,

            ut: {
              dstr: `${new Date().getTime()}`,
              dnum: new Date().getTime(),
            },
          };

          // Save the article
          const a = new NewsArt(newArt);
          a.save((e, s) => {
            if (e) {
              console.log('Failed to save article');
              console.log(e);
            }
          });
        }
      });
    }
  });
}

// migrateFromFile();
// getFeatured();

// getFeatured();

// Loop set timeout

function getLatest() {
  latestBot.getLatest(cb => {
    if (cb) {
      cb.map(json => {
        // if(json){

        // Check if it already exists
        NewsArt.findOne({ 'meta.url': json.url }, {}, (error, arti) => {
          if (arti) {
            // found the bitch, UPDATE

            arti.meta.title_og = json.title;
            arti.meta.des = json.description;
            arti.meta.author = json.author || '';
            arti.image.banner = json.featured_image || '';
            arti.data = json.data || [];
            arti.save((e, saved) => {
              if (!e) {
                console.log('Updated article');
              } else {
                console.log('Failed to update article');
              }
            });
          } else {
            // Create a new one
            const newArt = {
              image: {
                banner: json.featured_image,
              },

              // Basic info
              meta: {
                title_og: json.title,
                des: json.description,

                url: json.url,
                author: json.author || '',
              },

              // contents of the post
              data: json.data,

              ut: {
                dstr: `${  new Date().getTime()}`,
                dnum: new Date().getTime(),
              },
            };

            // Save the article
            const a = new NewsArt(newArt);
            a.save((e, s) => {
              if (e) {
                console.log('Failed to save article');
                console.log(e);
              } else {
                console.log('Created new article');
              }
            });
          }
        });

        // }
      });
    } else {
      console.log('Latest Article data is null');
    }
  });
}

function getFeatured() {
  featuredBot.getFeatured(cb => {
    if (cb) {
      cb.map(json => {
        if (json.data !== undefined) {
          // Check if it already exists
          NewsArt.findOne({ 'meta.url': json.url }, {}, (error, arti) => {
            if (arti) {
              // found the bitch, UPDATE

              arti.featured = true;
              arti.meta.title_og = json.title;
              arti.meta.des = json.description;
              arti.meta.author = json.author || '';
              arti.image.banner = json.featured_image || '';
              arti.data = json.data || [];
              arti.save((e, saved) => {
                if (!e) {
                  console.log('Updated article');
                } else {
                  console.log('Failed to update article');
                }
              });
            } else {
              // Create a new one
              let newArt = {
                featured: true,

                image: {
                  banner: json.featured_image,
                },

                // Basic info
                meta: {
                  title_og: json.title,
                  des: json.description,

                  url: json.url,
                  author: json.author || '',
                },

                // contents of the post
                data: json.data,

                ut: {
                  dstr: `${  new Date().getTime()}`,
                  dnum: new Date().getTime(),
                },
              };

              // Save the article
              const a = new NewsArt(newArt);
              a.save((e, s) => {
                if (e) {
                  console.log('Failed to save article');
                  console.log(e);
                } else {
                  console.log('Created new article');
                }
              });
            }
          });
        }
      });
    } else {
      console.log('Featured Article data is null');
      // callback()
    }
  });
}

async function getArtsArray(query) {
  const doc = await NewsArt.find(query).exec();
  if (doc) {
    // console.log(doc.length);
    return doc;
  }

  console.log('Doc null');
  return [];
}

function getArticlesQuery(doc) {
  const pubid = doc.pubid;

  const start = doc.start || new Date().getTime();

  const sort = doc.sort || 'lt';

  const lmt = doc.limit || 10;

  const ut = {};

  const sorting = doc.sorting || { 'ut.dnum': -1 };

  console.log('Pulling arts now');

  ut[`$${sort}`] = start;

  console.log(ut);

  // try {
  if (pubid) {
    return NewsArt.find({ pubid, 'ut.dnum': ut })
      .limit(lmt)
      .sort(sorting)
      .exec();
  }

  // No pubid
  return NewsArt.find({ 'ut.dnum': ut })
    .limit(lmt)
    .sort(sorting)
    .exec();
}

/** GET ONE/MANY
 * size = one/many
 * count = number of articles, default 10
 * start = Newer or order, default to newer
 *        if older, provider startpoint
 * artid = publisher id */
function getArticles(x, callback) {
  const size = x.typo || 'many';
  const count = x.count || 10;
  const start = x.start || new Date().getTime();

  const artid = x.artid || '';
  const pubid = x.pubid || '';

  const query = {};

  // If artid not empty
  if (!_.isEmpty(artid)) {
    query.artid = artid;
  }

  // If artid not empty
  if (!_.isEmpty(pubid)) {
    query.pubid = pubid;
  }

  // If start number is greater than a couple hundred
  // if(start > 100){
  query['ut.dnum'] = { $lt: start };
  // }

  if (size === 'many') {
    // Find many
    // Add sort

    // If sort is not negative
    // if(sort > 0)

    const finalart = [];
    // First get publishers
    NewsPub.find({}, (err, publishers) => {
      if (!err) {
        publishers.map(publisher => {
          if (publisher) {
            // FIND MANY From this publisher
            query['meta.url'] = { $regex: `.*${publisher.meta.website}.*` };

            const arts = getArtsArray(query);

            console.log(arts.length);
            if (arts !== undefined) {
              arts.map(sa => {
                finalart.push(sa);
              });
            }
          }
        });

        console.log(`#######################################${finalart.length}`);
        callback({ status: '200', msg: `Articles found ${finalart.length}`, data: finalart });
      } else {
        callback({ status: '400', msg: 'Publisher not found' });
      }
    });
  } else {
    // findone

    // FIND ONE
    NewsArt.findOne(query).exec((error, saved) => {
      if (saved) {
        callback({ status: '200', msg: '', data: saved });
      } else {
        callback({ status: '400', msg: 'Not found' });
      }
    });
  }
}

function returnArticles(query) {
  let f = [];
  NewsArt.find(query)
    .lean()
    .limit(10)
    .sort({ pos: -1 })
    .exec((error, saved) => {
      if (saved) {
        f = saved;
        /** console.log(saved.length);
            saved.map(function (sa) {
                finalart.push(sa);
            });
            //finalart.concat(saved)
            //callback({status: "200", msg:"", data:saved, publisher:publisher});
             * */
      } else {
        // callback({status: "400", msg:"Articles not found"});
      }
    });

  return f;
}

/** CREATE Article
 *
 * url, String
 * pid, String
 * data, Array
 * des, String
 * title, String
 * image, String
 *
 * * */
function RESTcreateArt(x) {
  // IMPORTANT
  const url = x.url;
  const pubid = x.pubid;
  const title = x.title;
  const des = x.des;
  const data = x.data;
  const date = x.unix_time;
  const banner = x.banner;
  const author = x.author;

  NewsArt.findOne({ 'meta.url': url, pubid }, (error, artExist) => {
    if (artExist) {
      // Update article
      artExist.meta.title_og = title;
      artExist.meta.des = des;
      artExist.meta.author = author;
      artExist.meta.url = url;
      artExist.image.banner = banner;
      artExist.data = data;

      // Date
      // artExist.ut.dnum = +date;
      // artExist.ut.dstr = date+"";

      // Update
      artExist.save((error, saved) => {
        if (saved) {
          console.log('ARTICLE UPDATE saved');
        } else {
          console.log('ARTICLE UPDATE failed to saved');
        }
      });
    } else {
      // Create new article

      const artID = config.rand(20);

      const articleData = {
        pubid,
        artid: artID,
        meta: {
          url,
          title_og: title,
          des,
          author,
        },
        image: {
          banner,
        },

        // TODO LOOP thru the data
        data,

        ut: {
          dstr: `${date}`,
          dnum: +date,
        },
      };

      const art = new NewsArt(articleData);

      // Save article
      art.save((error, saved) => {
        if (saved) {
          console.log('ARTICLE saved');
        } else {
          console.log('ARTICLE failed to save');
        }
      });
    }
  });
}

/** CREATE ARTICLE
 * Create
 *
 * publish ID, username
 *
 * meta=>-title_og, des, url
 * image=>-banner
 * -data
 *
 * pubid: ",
 * meta: {url, title, des },
 * image: { banner },
 * data: []
 *
 * * */
function createArt(x, callback) {
  const publisherID = x.pubid || '';
  const url = x.url || '';

  // Check if publisher not null
  if (!_.isEmpty(publisherID)) {
    // Check if publisher not null
    if (!_.isEmpty(url)) {
      // Create or update the aricle
      const article = {
        pubid: publisherID,
        url,
        data: x.data,
        title: x.title,
        des: x.des,
        banner: x.banner,
        unix_time: x.unix_time,
        author: x.author,
      };

      // Create article
      RESTcreateArt(article);
      callback({ status: '200', msg: 'Cool, Article created' });
    } else {
      callback({ status: '400', msg: 'URL is null' });
    }
  } else {
    callback({ status: '400', msg: 'Publisher ID is null' });
  }
}

function updateSmTitle(x, callback) {
  const artid = x.artid;
  const title = x.title;

  if (!_.isEmpty(artid)) {
    NewsArt.findOne({ artid }, {}, (error, saved) => {
      if (saved) {
        // Update object and save
        saved.meta.title_sm = title;
        saved.save((e, sa) => {
          if (sa) {
            callback({ status: '200', msg: 'Article Saved!' });
          } else {
            callback({ status: '400', msg: 'Article could not be saved' });
          }
        });
      } else {
        callback({ status: '400', msg: 'Article not available' });
      }
    });
  } else {
    callback({ status: '400', msg: 'Article not found' });
  }
}

async function asyncGetArts() {
  const doc = await NewsArt.find({})
    .lean()
    .limit(10)
    .exec();
  if (doc) {
    // console.log(doc.length);
    return doc;
  }

  console.log('Doc null');
  return [];
}

const makeRequest = async () => {
  const data = await NewsPub.find()
    .lean()
    .exec();

  // return data;
  if (data) {
    return await Promise.all(
      data.map(async publisher => {
        const query = { $regex: `.*${publisher.website}.*` };
        const query1 = { $regex: `${'^' + 'http://'}${publisher.website}` };
        const query2 = { $regex: `${'^' + 'http://www.'}${publisher.website}` };

        return await NewsArt.find({
          $or: [{ 'meta.url': query }, { 'meta.url': query1 }, { 'meta.url': query2 }],
        })
          .sort({ 'ut.dnum': -1 })
          .lean()
          .limit(5)
          .exec();
        // return x;
      })
    );
  }
  console.log('Error occured');
  return [];
};

/* DATA is article id,
* TODO LOGO USER WHO DID VIEW* */

function addView(data) {
  NewsArt.findOne({ _id: data }, {}, (error, doc) => {
    if (doc) {
      const curView = parseInt(doc.views.cnum) + config.getRandomInt(1000, 2500);
      doc.views.cnum = curView;
      doc.views.cstr = `${curView}`;

      doc.save((e, saved) => {
        if (!e) {
          console.log(
            `$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ $ Saved article view ${curView}`
          );
        } else {
          console.log(
            `$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ $ Saved article view ${curView}`
          );
        }
      });
    } else {
      console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ $ Article not found ${data}`);
    }
  });
}

async function saveOrUpdateArticle(article) {
  const link = article.url;
  const publisher = article.publisher;
  let firstArt;

  try {
    firstArt = await NewsArt.findOne({ 'meta.url': link }).exec();
    if (firstArt) {
      console.log('Updating article');
      // Update article now
      const content = [];
      content.push({
        typo: '',
        data: article.data,
      });

      firstArt.raw = content;
      firstArt.image.banner = article.img;
      firstArt.meta.title_og = article.title;
      firstArt.meta.des = article.des || '';
      firstArt.meta.author = article.author || '';

      firstArt.save((err, sav) => {
        if (!err) {
          console.log('Success updating article');
          return { success: {} };
        }

        console.log('Failed to update article');
        return { success: {} };
      });
    } else {
      console.log('Creating new article');
      // Create new article
      const content = [];
      content.push({
        typo: '',
        data: article.data,
      });

      let pubid = '';
      if (publisher) {
        pubid = publisher._id || '';
      }
      const curView = config.getRandomInt(100, 290);
      const nArt = {
        // Publisher ID
        pubid,

        // default
        views: {
          cnum: curView,
          cstr: `${curView}`,
        },

        // Article ID
        artid: config.rand(20),

        raw: content,

        // Basic info
        meta: {
          // twitter, facebook, ig, website,
          // title_sm:  "",
          title_og: article.title,
          des: article.des || '',

          url: link,
          author: article.author || '',
        },

        ut: config.getUt(new Date()),

        // Featured
        featured: true,

        // Article banner images
        image: {
          banner: article.img,
        },
      };

      // Save the article
      const newArt = new NewsArt(nArt);

      newArt.save((error, saved) => {
        if (!error) {
          console.log('Success saving article');
          return { success: {} };
        }
        console.log('Failed to save article');
        return { success: {} };
      });
    }
  } catch (error) {
    console.log(error);
    return { success: {} };
  }
}

module.exports = {
  // For getting articles
  getArticles,

  asyncGetArts,

  asyncMake: makeRequest,

  // For updating smaller titles
  updateSmTitle,

  // for creacting new articles
  CBcreateArt: createArt,
  saveOrUpdateArticle,

  // Get all articles
  CBgetArticles: getArticles,

  articleAddView: addView,

  getArticlesQuery,
};
