/**
 * Created by vuga on 9/22/17.
 *
 * EXPORT, CRUD ARTICLE
 * GET ONE/ALL,
 * Create, update
 */

var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var config =  require('../static/variables');
var mongoose = require('mongoose');

var _ = require('lodash');

var articleUtil = require('./article/article');
var publisherUtil = require('./publisher/publisher');


router.post('/:typo/:action',async function (req,res) {

    //console.log("")
    var typo = req.params.typo;
    var action = req.params.action;
    console.log(req.body);
    switch (typo){

        case "publisher":

            switch (action){
                case "get":

                    ///**
                    publisherUtil.CBgetPublisher({
                        available:true,
                        typo: "many"
                    }, function (xc) {
                        res.status(200).json(xc);
                    });

                    //**/

                    break;
            }
            break;


        case "article":
            switch (action){
                case "pull":
                    let docs;
                    try {
                        docs = await articleUtil.getArticlesQuery(req.body);
                        console.log(req.body);
                        res.status(200).json(docs);
                    }
                    catch (error){
                        console.log(error);
                        res.status(200).json([]);
                    }
                    break;

                case "edit":
                    articleUtil.updateSmTitle(req.body, function (xc) {
                        console.log(xc);
                        res.status(200).json(xc);
                    });
                    break;
                case "get":
                        //articleUtil.getArticles()
                    var doc = await articleUtil.asyncMake();
                    if(doc) {
                        var dido = [];
                        doc.map(function (dox) {
                            if(dox.length > 0){
                                dox.map(function (dos) {
                                    dido.push(dos);
                                });
                            }
                        });

                        res.status(200).json({status: "200",msg:"Found "+dido.length, data:dido});
                    }
                    else{
                        res.status(200).json({status: "200",msg:"", data:[]});
                    }
            }
            break;
    }
});






module.exports= router;