/**
 * Created by vuga on 9/22/17.
 *
 * EXPORT, CRUD ARTICLE
 * GET ONE/ALL,
 * Create, update
 */

const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
const config = require('../static/variables');
const mongoose = require('mongoose');

const _ = require('lodash');

const articleUtil = require('./article/article');
const publisherUtil = require('./publisher/publisher');

const igihe = require('./spider/igihe/igihe');

function sync() {
  /**
    publisherUtil.CBgetPublisher({typo:"many"}, function (cb) {
        if(cb.status === "200"){
            var pubs = cb.data;

            var igihe_id = cb.data[0];
            igihe.
        }
    });  * */
  igihe.index();
}

router.post('/:typo/:action', async (req, res) => {
  console.log("Get contents", req.params);
  const { typo, action } = req.params;
  // const action = req.params.action;
  let doc;
  console.log(req.body);
  switch (typo) {
    case 'sync':
      sync();
      break;

    case 'publisher':
      switch (action) {
        case 'edit':
          publisherUtil.CBupdatePublisher(req.body, xc => {
            console.log(xc);
            return res.status(200).json(xc);
          });
          break;
        case 'create':
          publisherUtil.CBcreatePublisher(req.body, xc => {
            return res.status(200).json(xc);
          });
          break;
        case 'get':
          // /**
          publisherUtil.CBgetPublisher(req.body, xc => {
            return res.status(200).json(xc);
          });

          //* */

          break;
        default:
          return res.status(200).json({});
          break;
      }
      break;

    case 'article':
      switch (action) {
        case 'edit':
          articleUtil.updateSmTitle(req.body, xc => {
            console.log(xc);
            return res.status(200).json(xc);
          });
          break;
        case 'get':
          doc = await articleUtil.asyncMake();
          if (doc) {
            const dido = [];
            doc.map(dox => {
              if (dox.length > 0) {
                dox.map(dos => {
                  dido.push(dos);
                });
              }
            });

            return res.json({ status: '200', msg: `Found ${dido.length}`, data: dido });
          }
          return res.json({ status: '200', msg: '', data: [] });

        default:
          break;
      }
      break;
    default:
      break;
  }
});

module.exports = router;
