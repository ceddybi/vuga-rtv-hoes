/**
 * Created by ceddy on 10/10/17.
 */



var host = "http://192.168.0.11:4040/dsc/img/logo/";
var pubs= [
    {index:299,id: "igihe.com", logo_white:host+"igihe.png", logo_color:"",
        xpath_latest:"//div/a[@id='pagination_gh_news']/../div[@class='article-wrap']//span[@class='homenews-title']/a/@href",
        xpath_featured:"//*[@id='bn7']/ul/li/a/@href",
        xpath_fimg:"//meta[@property='og:image']/@content",
        xpath_des:"//div[@class='text-article margintop10']/div/div[2]//p/text()",
        xpath_author:"//span[@class='vcard author']/a/text()"
    },
    {index:199,id: "inyarwanda.com", logo_white:host+"inyarwanda.png", logo_color:"",
        xpath_latest:"//div[@class='leftContent']/div[@class='grabNews']/div[@class='artLogo']/a/@href",
        xpath_featured:"//*[contains(@id, 'fragment-')]/div[@class='logoslide']/a/@href",
        xpath_fimg:"//div[@class='artlogo']/a/@href",
        xpath_des:"//div[@class='artheader']/div[@class='artlead']/text()",
        xpath_author:"//div[@class='artheader']/div[@class='artinfo']//a/text()"
    },
    {index:99,id: "umuryango.rw", logo_white:host+"umuryango.png", logo_color:"",
        xpath_latest:"//div[contains(@class,'card')]//div/h2[contains(@class,'artticle')]/a/@href",
        xpath_featured:"//div[contains(@class,'card')]//div/h2[contains(@class,'artticle')]/a/@href",
        xpath_fimg:"//img[contains(@class, 'artlogo')]/@src",
        xpath_des:"//div[@class='wrapart']/p[1]/strong/text()",
        xpath_author:"//span[@class='vcard author']/a/text()"
    },
    {index:199,id: "umuseke.rw", logo_white:host+"umuseke.png", logo_color:"",
        xpath_latest:"//ul[@class='blog-post-list']/li/a/@href",
        xpath_featured:"//div[contains(@id, 'post-')]/a/@href",
        xpath_fimg:"//div[@class='content-main']/div[1]/@data-orig-file",
        xpath_des:"//div[@class='content-main']/p[1]//text()",
        xpath_author:"//header[@id='post-header']//span[@itemprop='author']/a/text()"
    },
    {index:19,id: "bwiza.com", logo_white:host+"bwiza.png", logo_color:"",
        xpath_latest: "//div[contains(@class,'news-box')]/div[@class='nb-content']//div[contains(@class,'news')]/article/div/h3/a/@href",
        xpath_featured:"/html/body/div[1]/div[2]/div[6]/div[1]/div[1]/div[1]/div[2]/div/div/a/@href",
        xpath_fimg:"//div[@class='feature-img']/img/@src",
        xpath_des:"//div[@class='entry-content']/p[1]//text()",
        xpath_author:"//author"
    },
    {index:199,id: "eachamps.rw", logo_white:host+"eachamps.png", logo_color:"",
        xpath_latest:"//div[@class='top-left-bottom-cont']//ul/li//div[@class='more-cont-ttle']/a/@href",
        xpath_featured:"//div[@class='ps-current']/ul/li/a/@href",
        xpath_fimg:"//div[@class='art-bottom-left']/img/@src",
        xpath_des:"//div[@class='article-desc']/p[1]//text()",
        xpath_author:"//div[@class='pull-left']/label[@class='art-auth']/text()"
    },
    {index:39,id: "kigalitoday.com", logo_white:host+"kigalitoday.png", logo_color:"",
        xpath_latest:"//div[contains(@class,'hed-set')]//h2/a/@href | //div[contains(@class,'hed-set')]//h3/a/@href | //li[contains(@class,'hed-summ')]//h2/a/@href | //li[contains(@class,'hed-summ')]//h3/a/@href",
        xpath_featured:"//div[contains(@class,'hed-set')]//h2/a/@href | //div[contains(@class,'hed-set')]//h3/a/@href | //li[contains(@class,'hed-summ')]//h2/a/@href | //li[contains(@class,'hed-summ')]//h3/a/@href",
        xpath_fimg:"//meta[@property='og:image']/@content",
        xpath_des:"//h2[@itemprop='description']/p/text()",
        xpath_author:"//div[@itemprop='author']/span/text()"
    },
    {index:59,id: "newtimes.co.rw", logo_white:host+"newtimes.png", logo_color:"",
        xpath_latest:"//div[contains(@class,'hed-set')]//h2/a/@href | //div[contains(@class,'hed-set')]//h3/a/@href | //li[contains(@class,'hed-summ')]//h2/a/@href | //li[contains(@class,'hed-summ')]//h3/a/@href",
        xpath_featured:"//div[contains(@class,'hed-set')]//h2/a/@href | //div[contains(@class,'hed-set')]//h3/a/@href | //li[contains(@class,'hed-summ')]//h2/a/@href | //li[contains(@class,'hed-summ')]//h3/a/@href",
        xpath_fimg:"//meta[@property='og:image']/@content",
        xpath_des:"//h2[@itemprop='description']/p/text()",
        xpath_author:"//div[@itemprop='author']/span/text()"
    },
    {index:19,id: "ukwezi.com", logo_white:host+"ukwezi.png", logo_color:"",
        xpath_latest:"//div[@class='yeniDivana1']/a/@href | //div[@class='yeniDivana2']/a/@href | //div[@class='yanGorunum1']/a/@href | //div[@class='yanGorunum31']/a/@href | //div[@class='videoManset2']/a/@href",
        xpath_featured:"//div[@class='swiper-wrapper']/div/a/@href",
        xpath_fimg:"//meta[@property='og:image']/@content",
        xpath_des:"//div[@class='haberText']//p/strong/text()",
        xpath_author:"//meta[@name='author']/@content"
    },
    {index:199,id: "yegob.rw", logo_white:host+"yegob.png", logo_color:"",
        xpath_latest:"//div[contains(@class,'td_module_4')]/h3/a/@href | //div[@class='item-details']/h3/a/@href",
        xpath_featured:"//div[contains(@class,'td_module_4')]/h3/a/@href | //div[@class='item-details']/h3/a/@href",
        xpath_fimg:"//img[contains(@class, 'artlogo')]/@src",
        xpath_des:"//div[@class='wrapart']/p[1]/strong/text()",
        xpath_author:"//div[@class='td-post-author-name']/a/text()"
    },
    ];


function migratePublishers(){
    
    var f = [];
    pubs.map(function (pub) {
        var publish = {  // Publisher ID
                pubid :  pub.id,
                pos:  pub.index,
                countryCode: "RW",
                image: {
                    logo_white:  pub.logo_white,
                    //logo_black:  {type: String, default: ""},
                    //logo_color:  {type: String, default: ""},
                },


                // Basic info
                meta: {
                    name:pub.id,
                    website:  pub.id,
                    xpath_latest:  pub.xpath_latest,
                    xpath_featured:  pub.xpath_featured,
                    xpath_fimg:  pub.xpath_fimg,
                    xpath_author:  pub.xpath_author,
                    xpath_des:  pub.xpath_des,
                },




            };
        
        f.push(publish);
    });
    
    
    return f;
}


function migrateArticles() {
    
}



module.exports = {
    migrate: migratePublishers
};