/**
 * Created by ceddy on 9/26/17.

CREATE PUBLISHER,
 EDIT PUBLISHER,
 GET PUBLISHERS
 */

const CB = require('../../static/cb');
const config = require('../../static/variables');
const mongoose = require('mongoose');

const _ = require('lodash');

const NewsArt = mongoose.model('newsArt');
const NewsPub = mongoose.model('newsPub');

const migrator = require('../miga-publishers');

// Check initial data
function mig() {
  NewsPub.find({}, (error, pubs) => {
    if (pubs.length > 3) {
      console.log('Publishers are avalable');
    } else {
      console.log('Publishers migrating');

      const pubsX = migrator.migrate();

      pubsX.map(pub => {
        // Update or create new
        const p = new NewsPub(pub);
        p.save((error, saved) => {
          if (error) {
            console.log('Error saving pulisher');
            console.log(error);
          }
        });
      });
    }
  });
}

// mig();

/**
 * CREATE PUBLISHER
 *
 * name, des, slogan
 * image = logo_white, logo_black
 *
 *
 * ........more
 * */
function CBcreatePublisher(x, callback) {
  // Basic info
  const pid = config.rand(20);

  const publisher = {
    // Publisher ID
    pubid: pid,

    // Availability
    available: x.available,

    countryCode: x.countryCode,

    // Index
    pos: x.pos,

    // Unix time of created publisher
    ut: config.getUt(new Date()),

    meta: {
      name: x.name,
      slogan: x.slogan,
      des: x.des,
      website: x.website,
      username: x.username,
      xpath_fimg: x.xpath_fimg,
      xpath_latest: x.xpath_latest,
    },

    image: {
      logo_white: x.logo_white,
      logo_black: x.logo_black,
      logo_color: x.logo_color,
    },

    // Social Media
    social: {
      tw: x.tw,
      fb: x.fb,
      ig: x.ig,
      sn: x.sn,
    },
  };

  const pub = new NewsPub(publisher);

  pub.save((err, saved) => {
    if (saved) {
      callback({ status: '200', msg: 'CREATE PUBLISHER saved' });
    } else {
      callback({ status: '400', msg: 'CREATE PUBLISHER Failed to save' });
    }
  });
}

/** UPDATE by pubid* */
function CBupdatePublisher(x, callback) {
  const pubid = x.pubid;

  // Check if not null
  if (!_.isEmpty(pubid)) {
    NewsPub.findOne({ _id: pubid }, (error, publisher) => {
      if (publisher) {
        publisher.pos = x.pos;
        publisher.available = x.available;
        publisher.countryCode = x.countryCode;
        // META
        publisher.meta.name = x.name;
        publisher.meta.slogan = x.slogan;
        publisher.meta.des = x.des;
        publisher.meta.website = x.website;
        publisher.meta.username = x.username;
        publisher.meta.xpath_fimg = x.xpath_fimg;
        publisher.meta.xpath_latest = x.xpath_latest;
        publisher.meta.xpath_featured = x.xpath_featured;
        // IMAGE
        publisher.image.logo_white = x.logo_white;
        publisher.image.logo_black = x.logo_black;
        publisher.image.logo_color = x.logo_color;
        // Social
        publisher.social = {
          tw: x.tw,
          fb: x.fb,
          ig: x.ig,
          sn: x.sn,
        };

        // Save the bitch
        publisher.save((error, saved) => {
          if (saved) {
            callback({ status: '200', msg: 'UPDATE PUBLISHER publisher save' });
          } else {
            callback({ status: '400', msg: 'UPDATE PUBLISHER publisher failed to save' });
          }
        });
      } else {
        callback({ status: '400', msg: 'UPDATE PUBLISHER publisher failed to find' });
      }
    });
  } else {
    callback({ status: '400', msg: 'UPDATE PUBLISHER publisher not found' });
  }
}

/**
 * Get all publishers whether available or not* */
function CBgetPublisher(x, callback) {
  const size = x.typo || 'many';

  let query = {};

  const available = x.available || false;

  if (available) {
    query = { available: true };
  }

  if (size === 'many') {
    // find many
    NewsPub.find(query, (error, pubs) => {
      if (pubs) {
        const pus_to = [];

        // Filtify
        pubs.map(publisher => {
          const p = {
            // Availability
            _id: publisher._id,
            available: publisher.available,

            // Country code
            countryCode: publisher.countryCode,

            // Index
            pos: publisher.pos,

            // Meta
            name: publisher.name,
            slogan: publisher.slogan || '',
            des: publisher.des,
            website: publisher.website,
            username: publisher.username,
            xpath_latest: publisher.xpath_latest,
            xpath_featured: publisher.xpath_featured,
            xpath_fimg: publisher.xpath_fimg,

            // images
            logo_white: '',
            logo_black: '',
            logo_color: '',

            /** 
            tw: publisher.social.tw,
            fb: publisher.social.fb,
            ig: publisher.social.ig,
            sn: publisher.social.sn, * */
          };
          pus_to.push(p);
        });

        callback({ status: '200', msg: 'Publishers found', data: pus_to });
      } else {
        callback({ status: '400', msg: 'Publishers not found' });
      }
    });
  } else {
    // find one by id
    const pubid = x.pubid;
    NewsPub.findOne({ _id: pubid }, (error, publisher) => {
      if (publisher) {
        const publi = {
          // Availability
          _id: publisher._id,
          available: publisher.available,

          // Country code
          countryCode: publisher.countryCode,

          // Index
          pos: publisher.pos,

          // Meta
          name: publisher.meta.name,
          slogan: publisher.meta.slogan,
          des: publisher.meta.des,
          website: publisher.meta.website,
          username: publisher.meta.username,
          xpath_latest: publisher.meta.xpath_latest,
          xpath_featured: publisher.meta.xpath_featured,
          xpath_fimg: publisher.meta.xpath_fimg,

          // images
          logo_white: publisher.image.logo_white,
          logo_black: publisher.image.logo_black,
          logo_color: publisher.image.logo_color,

          // socials
          tw: publisher.social.tw,
          fb: publisher.social.fb,
          ig: publisher.social.ig,
          sn: publisher.social.sn,
        };

        callback({ status: '200', msg: 'Publishers not found', data: publi });
      } else {
        callback({ status: '400', msg: 'Publishers not found' });
      }
    });
  }
}

module.exports = {
  CBcreatePublisher,
  CBgetPublisher,
  CBupdatePublisher,
};
