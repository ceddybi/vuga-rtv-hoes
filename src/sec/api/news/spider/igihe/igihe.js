/**
 * Created by ceddy on 9/28/17.
 */


var request = require('request');
var cheerio = require('cheerio');
var article = require('../../article/article');
var url = 'http://igihe.com/';
var mongoose = require('mongoose');
var NewsPub = mongoose.model('newsPub');

var _ = require('lodash');


function getLatest (callback) {

    var urls = [];

    const cheerioReq = require("cheerio-req");

    request(url, function(error, response, responseHtml) {

        if(!error) {
            var $ = cheerio.load(responseHtml);
            var main = $("#bn7").html();  // Fetch the breaking news slider
            var c = cheerio.load(main);    // Load the bitch
            //  Arrays

            console.log(c('ul').children().length);    // Get the lists

            //console.log(main);

            c('ul').children().each(function (i, elem) {
                urls.push(cheerio.load(c(this).html()).root().find('a').attr('href')); //Map each list and get content
            });

            callback(urls);
        }
        else{
            callback(urls);
        }

    });

}

function getArticleData(path, callback){

    request(url+path, function(error, response, responseHtml) {
        if(!error) {
            var $ = cheerio.load(responseHtml);
            //METADATA
            var metadata = {};

            var metaTags = $('meta').filter(function () {
                if (!this.attribs.name) {
                    return false;
                }
                return this.attribs.name.match('twitter:');
            });
            metaTags.each(function (i, element) {
                var attrs;
                attrs = element.attribs;
                metadata[attrs.name.replace('twitter:', '').toLowerCase()] = attrs.content;
            });
            //


            //FIRST PARAGRAPH
            var firstpara = $(".surtitre").text();

            //TITLE
            var title = $(".title-article").text();

            // PARAGRAPHS
            var paras = [];

            //IMAGES
            var images = [];
            $(".fulltext").children().each(function (i, elem) {
                var para = {
                    index: 0,
                    text: ""
                };

                /**GET PARAGRAPH*/
                var par = cheerio.load($(this).html()).text();

                if (par !== undefined && par[0] !== "") {
                    para.index = i;
                    para.text = par;

                    paras.push(para)
                }
                //console.log($(this).find('img').html())

                var img = $(this).find('img').length;
                if (img > 0) {

                    var image = {
                        src: "",
                        cap: ""
                    };

                    img = $(this).find('img');
                    //console.log(img["0"].attribs.src);

                    // Set source
                    image.src = img["0"].attribs.src;

                    /**Look for caption of image*/
                    var next = img["0"].next.next;
                    if (next != undefined) {
                        //console.log(next.children[0].data);
                        image.cap = next.children[0].data;
                    }
                    //console.log(img["0"].next.data)

                    images.push(image);
                }

                // if()
                //p.push(par);
            });

            // Final object
            var x = {
                path: path,
                meta: metadata,
                paragraphs: paras,
                images: images
            }

            //
            //console.log(x);
            callback(x);
        }
        else{
            callback();
        }

    });

}


function index(){

    getLatest(function (urls) {

        if(!_.isEmpty(urls)){
            // Got the urls
            urls.map(function (url, index) {

                if(!_.isEmpty(url)){
                    // Fetch Article data
                    getArticleData(url, function (articleData) {

                        if(!_.isEmpty(articleData)){
                            //Save article
                            saveArticle(articleData);

                        }
                        else{
                            console.log("Error ARTICLE NULL")
                        }
                    });
                }
                else{
                    console.log("Error URL EMPTY")
                }

            });
        }
        else{
            console.log("Failed to get URLs")
        }
    });
}


function saveArticle(x) {
    NewsPub.findOne({"meta.name":"igihe"}, function (error, igh) {
        if(!error){
            // got pubid
            var art = {
                pubid: igh.pubid,
                url: x.path,
                data: [],
                title: x.meta.title,
                des: x.meta.description,
                banner: x.meta["image:src"],
                unix_time: new Date().getTime(),
                author: "Ceddy Bi"
            };
            //console.log(art);

            //art.data.push()
            var indexnum = 0;
            //Push paraphs as data into the data
            x.paragraphs.map(function (para, index) {
                if(!_.isEmpty(para)){
                    //increment indexnum
                    indexnum = indexnum+1;
                    art.data.push(
                        {
                         //Media ID, for all types
                         medid:  "",


                         // Index
                         pos:  indexnum,

                         //Typo, txt,img,vid
                         typo:  "txt",


                         txt: {
                             //TODO, bold,h2,h3,p,it
                             style:  "",

                             text:  para.text,
                             quote:  "",
                         },
                    }
                    );
                }
                else{
                    console.log("Paragraph null")
                }
            });

            x.images.map(function (image,index) {
                if(!_.isEmpty(image)){
                    //increment indexnum
                    indexnum = indexnum+1;
                    art.data.push(
                        {
                            //Media ID, for all types
                            medid:  "",
                            // Index
                            pos:  indexnum,
                            //Typo, txt,img,vid
                            typo:  "img",
                            img: {
                                // TODO 300x400, XxY
                                size: "",
                                src:  image.src,
                                cap:  image.cap,
                            },
                        }
                    );
                }
                else{
                    console.log("Image null")
                }
            });


            article.CBcreateArt(art, function (don) {
                console.log(don);
            })



        }
        else{
            console.log("Could not get the publisher")
        }
    });

}

module.exports = {
    index:index
};