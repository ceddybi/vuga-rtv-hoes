//var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');

var url = 'http://igihe.com/amakuru/u-rwanda/article/perezida-kagame-yitabiriye-irahira-rya-joao-lourenco-watorewe-kuyobora-angola';

request(url, function(error, response, responseHtml) {
    var $ = cheerio.load(responseHtml);
    //METADATA
    var metadata = {};

    var metaTags = $('meta').filter(function () {
        if (!this.attribs.name) {
            return false;
        }
        return this.attribs.name.match('twitter:');
    });
    metaTags.each(function (i, element) {
        var attrs;
        attrs = element.attribs;
        metadata[attrs.name.replace('twitter:', '').toLowerCase()] = attrs.content;
    });
    //


    //FIRST PARAGRAPH
    var firstpara = $(".surtitre").text();

    //TITLE
    var title = $(".title-article").text();

    // PARAGRAPHS
    var paras = [];

    //IMAGES
    var images = [];
    $(".fulltext").children().each(function (i, elem) {
        var para = {
            index: 0,
            text: ""
        };

        /**GET PARAGRAPH*/
        var par = cheerio.load($(this).html()).text();

        if (par !== undefined && par[0] !== "") {
            para.index = i;
            para.text = par;

            paras.push(para)
        }
        //console.log($(this).find('img').html())

        var img = $(this).find('img').length;
        if (img > 0) {

            var image = {
                src: "",
                cap: ""
            };

            img = $(this).find('img');
            //console.log(img["0"].attribs.src);

            // Set source
            image.src = img["0"].attribs.src;

            /**Look for caption of image*/
            var next = img["0"].next.next;
            if (next != undefined) {
                //console.log(next.children[0].data);
                image.cap = next.children[0].data;
            }
            //console.log(img["0"].next.data)

            images.push(image);
        }

        // if()
        //p.push(par);
    });

    // Final object
    var x = {
        meta: metadata,
        paragraphs: paras,
        images: images
    }

    //
    console.log(x);


});
