var that = require('./cb');
var _ = require('lodash');

var mongoose = require('mongoose'),
 UserProfile = mongoose.model('UserProfile');

 function sycnReturnMany(model, query){
   model.find(query, (error, body) => {
            if(body){
                return body
            }
            else{
                return []
            }
            
        });
 }

var exports = module.exports = {

     /**
      *
      * HELLO CALLBACK HELL*/

     CBfindMany: function(model, query, sort, isLean,limit, callback){
         // Lean the data
         if(isLean){
             model.find(query).sort(sort).lean().limit(limit).exec(query, (error, body) => {
                 if (body) {
                     callback({status: "200", msg:"", data: body});
                 }
                 else {
                     callback({status: "400", msg:"Error findin data", data: []});
                 }

             });
         }
         else {
             // no lean

             model.find(query).sort(sort).limit(limit).exec(query, (error, body) => {
                 if (body) {
                     callback({status: "200", msg:"", data: body});
                 }
                 else {
                     callback({status: "400", msg:"Error findin data", data: []});
                 }

             });
         }

     },

  create: function(model,todo, callback) {
        model.create(todo, function(err, data){
              if(data){
              callback({status: "200", msg: "Saved", data: data});
            }
            else{
              callback({status: "400", msg: "Error saving data"});
            }
      });
  },
  

  findOne: function(model,todo, callback) {
        model.findOne(todo, function(err, data){
              if(data){
              callback({status: "200", msg: "Success", data: data});
            }
            else{
              callback({status: "400", msg: "Error finding data"});
            }
      });
  },

  findMany: function(model,todo, callback) {
        model.find(todo, function(err, data){
              if(data){
              callback({status: "200", msg: "Success", data: data});
            }
            else{
              callback({status: "400", msg: "Error finding data"});
            }
      });
  },



   // Returns data callback with 
   // STATUS AND MSG
   update: function(model,todo,tosave,options, callback) {
     //instance.updateAttributes(req.body);
        model.update(todo,tosave,options, function(error, data){
            if(!error){
                  callback({status: "200", msg: "Saved"});
            }
            else{
              callback({status: "400", msg: "Not found"});
            }
      });
  },


  // Returns data callback with 
  // STATUS AND MSG
  deleteOne: function(model,todo,tosa, callback) {
  model.findOne(todo, function(error, data){
              if(data){
                   model.deleteOne(tosa, function(err, success){
                          if(!err){
                            callback({status: "200", msg: "Saved"});
                          }
                          else{
                            callback({status: "400", msg: "Failed to save document"});
                          }
                    });
            }
            else{
              callback({status: "400", msg: "Not found"});
            }
      });
  },






/** NO CALLBACK FUNCTIONS */
// Get one or many
// @model
// @count = one or many
// @todo the find query
getOneOrMany: function(model, count, todo, req, res){
      var howMany = (count || "Dumb");      
       // get by @pid only
      if(howMany!= undefined && howMany ==="one")
      {

                      this.findOne(model, todo, function(good){
                              if(good.status === "200"){
                                res.status(200).send({status:"200", msg:"Success", data:good.data});
                                //console.log("Okay")
                              }
                              else {
                                res.status(400).send({status:"400", msg:"No data found", data: {}})
                              }
                        });
                   
      }


      else if ( howMany!= undefined && howMany ==="many" )
      {  
                      this.findMany(model, todo, function(good){
                            if(good.status === "200"){
                               res.status(200).send({status:"200", msg:"Success", data:good.data});
                              //console.log("Okay")
                            }
                            else {
                              res.status(400).send({status:"400", msg:"No data found", data: {}})
                            }
                      });               
      }

      else{
           res.status(400).send({status:"400", msg:"Unknown"});
      }

      
     
},




stringStartsWith: function(str, prefix) {
    return str.indexOf(prefix) === 0;
},

getUserDocByUid: function(uid, callback){
   UserProfile.findOne({"info.userid": uid}).exec(function (error, result) {
        if (result)
        {
          // got doc
          callback(result);
        }
        else{
        callback();
        }

  });
},


syncGetUser: function(userid){
        if(_.isEmpty(userid)){
            return {};
        }
        else {
            var ret;
            setTimeout(function () {
                // ret = "hello";
                UserProfile.findOne({"info.userid": userid}, (error, body) => {
                    if (body) {
                        ret = body
                    }
                    else {
                        ret = {}
                    }

                });

            }, 0);
            while (ret === undefined) {
                require('deasync').sleep(0);
            }
            // returns hello with sleep; undefined without
            return ret;
        }
},

asyncGetUser: async function (userid) {
    return await UserProfile.findOne({"info.userid": userid}).exec();
},

asyncGetOne: async function(model, query) {
    return await model.findOne(query).exec();
},

syncGetOne: function(model, query){
        var ret;
        setTimeout(function(){
            // ret = "hello";
        model.findOne(query, (error, body) => {
            if(body){
                ret = body
            }
            else{
                ret = {}
            }
            
        });

        },0);
        while(ret === undefined) {
            require('deasync').sleep(0);
        }
        // returns hello with sleep; undefined without
        return ret;    
},

syncGetMany: function(model, query){
        var ret;
        setTimeout(function(){
            // ret = "hello";
        //ret = sycnReturnMany(model,query);

         model.find(query, (error, body) => {
            if(body){
                ret=  body
            }
            else{
                ret = [];
            }
            
        });

        },0);
        while(ret === undefined) {
            require('deasync').sleep(0);
        }
        // returns hello with sleep; undefined without
        return ret;    
},
syncGetManyLean: function(model, query,sort, limit){
        var ret;
        setTimeout(function(){
            // ret = "hello";
            //ret = sycnReturnMany(model,query);

            model.find(query).sort(sort).lean().limit(limit).exec(query, (error, body) => {
                if(body){
                    ret=  body
                }
                else{
                    ret = [];
                }

            });

        },0);
        while(ret === undefined) {
            require('deasync').sleep(0);
        }
        // returns hello with sleep; undefined without
        return ret;
},






returnUser(ud){

if(!_.isEmpty(ud)){
var userToreturn = {
                _id: ud._id,
                 userid:ud.info.userid,
                 firstname: ud.info.firstname,
                 lastname: ud.info.lastname,
                 username: ud.info.username,
                 phone: ud.info.phone,
                 email: ud.info.email,
                 sex: ud.info.sex,
                 chatcount: ud.chatcount,

                 premium: ud.premium

};

return userToreturn;
}
else{
    return {};
}
},



   
};
