var exports = module.exports = {

    mime(typo){
       var ty = (typo || "nul");
       ty.toUpperCase();

       return typos[ty];

    },
}

var typos = {
        // Null first
        NUL: "",

        // Audio Data    
        "MPGA": "audio/mpeg",
        "MP3": "audio/mpeg",
        "M4A": "audio/mp4",
        "WAV": "audio/x-wav",
        "AMR": "audio/amr",
        "AWB": "audio/amr-wb",
        "WMA": "audio/x-ms-wma",
        "OGG": "audio/ogg",
        "OGG": "application/ogg",
        "OGA": "application/ogg",
        "AAC": "audio/aac",
        "AAC": "audio/aac-adts",
        "MKA": "audio/x-matroska",
        "MID": "audio/midi",
        "MIDI": "audio/midi",
        "XMF": "audio/midi",
        "RTTTL": "audio/midi",
        "SMF": "audio/sp-midi",
        "IMY": "audio/imelody",
        "RTX": "audio/midi",
        "OTA": "audio/midi",
        "MXMF": "audio/midi",

        // Videodata
        "MPEG": "video/mpeg",
        "MPG": "video/mpeg", 
        "MP4": "video/mp4", 
        "M4V": "video/mp4",
        "3GP": "video/3gpp", 
        "3GPP": "video/3gpp",
        "3G2": "video/3gpp2", 
        "3GPP2": "video/3gpp2",
        "MKV": "video/x-matroska",
        "WEBM": "video/webm",
        "TS": "video/mp2ts",
        "AVI": "video/avi",
        "WMV": "video/x-ms-wmv",
        "ASF": "video/x-ms-asf",


        // Image
        "JPG": "image/jpeg",
        "JPEG": "image/jpeg", 
        "GIF": "image/gif", 
        "PNG": "image/png", 
        "BMP": "image/x-ms-bmp",
        "WBMP": "image/vnd.wap.wbmp",
        "WEBP": "image/webp",
        "M3U": "audio/x-mpegurl",
        "M3U": "application/x-mpegurl", 
        "PLS": "audio/x-scpls", 
        "WPL": "application/vnd.ms-wpl", 
        "M3U8": "application/vnd.apple.mpegurl",
        "M3U8": "audio/mpegurl",
        "M3U8": "audio/x-mpegurl",
        "MPG": "video/mp2p",
        "MPEG": "video/mp2p",


}