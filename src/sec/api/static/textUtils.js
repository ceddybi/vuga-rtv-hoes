/**
 * Created by vuga on 10/21/17.
 */

const { URL } = require('url');


function getCorrectUrl(site, current) {

    let ogsite = new URL(site);
    let domain = ogsite.hostname;

    var url = current;
    if(current.startsWith('/')){
        url = "http://"+domain+current;
    }
    else if (current.startsWith('http')){
        url = current;
    }
    else{
        url = "http://"+domain+"/"+current;
    }


    return url;
}


module.exports = {
    getCorrectUrl: getCorrectUrl
};