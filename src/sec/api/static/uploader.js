//var mkdirp = require('mkdirp');
var base64Img = require('base64-img');
var getDirName = require('path').dirname;
var fs = require('fs');
var path = require('path');

var AWS = require('aws-sdk');
var config = require('./variables');
var mimeType = require('./mimeType');
AWS.config = config;
var s3 = new AWS.S3();

var d = new Date();
var currentTime = d.getTime();


var exports = module.exports = {


// Takes file @content and put it in @path with @ContentEncoding e.g base64
// @Callback 

/** 
writeF: function(path, contents,ContentEncoding, cb) {
  mkdirp(getDirName(path), function (err) {
    if (err) return cb(err);
    
    fs.writeFile(path, contents,ContentEncoding, cb);
  });
}, **/

saveBase64Image: function(data, name, callback){
   base64Img.img(data, config.api_public_image_folder, name, callback);
},


// Saves image to s3 Bucket @file_path of file and new @file_name
// Nothing to return
S3saveImage: function(file_path, file_name){
    fs.stat(file_path, function(err, file_inf) {
      
           if(file_inf!=undefined){

                var bodyStrea = fs.createReadStream( file_path );

                var param = {
                  Bucket: config.aws_bucket,
                  Key: config.aws_image_folder+file_name,
                  Body: bodyStrea,
                  ACL: 'public-read',
                  ContentEncoding: 'base64',
                  ContentType: 'image/jpg',
                  ContentLength: file_inf.size
                };

        
                s3.putObject(param, function (err, data) {
                  if (err) {
                    console.log("Error uploading data: ", err);
                    //callback({status: "400"});
                  } else {

                    console.log("Successfully uploaded "+file_name, data);
                    //callback({status: "200"});
                     
                  }
                });

           }
           
        });

},




S3saveVideo :function (file_path, file_name, file_exe, file_type){

	  fs.stat(file_path, function(err, file_inf) {

                var bodyStrea = fs.createReadStream( file_path );

                var param = {
                  Bucket: config.aws_bucket,
                  Key: config.aws_video_folder+file_name,
                  Body: bodyStrea,
                  ACL: 'public-read',
                  ContentEncoding: 'base64',
                  ContentType: file_type+'/'+ file_exe,
                  ContentLength: file_inf.size
                };

        
                s3.putObject(param, function (err, data) {
                  if (err) {
                    console.log("Error uploading data: ", err);
                  } else {

                    console.log("Successfully uploaded "+file_type, data);
                  }
                });

             });

},



saveVideo: function(req, callback) {

             req.files.map(function (value, index) {

                if (value.fieldname == 'file_data') {
                    var splittedName = value.originalname.split('.');
                    var video_name = 'video_'+currentTime + '.' + splittedName[splittedName.length - 1];
                    video_path = __dirname + config.api_public_video_folder + video_name;
                    //data.video_path = '/data/videos/' + video_name;
                    // video.video_path = confi + video_name.replace(/ /g,"%20");
                    fs.rename(value.path, video_path);           
                    this.saveFile(video_path, video_name, splittedName[splittedName.length - 1], 'video');
                    //callback(video_path);

                }

              });




},



/**Save any file to the cloud s3
@param  file_path , file_name, file_exe, file_file_type, aws_path/folder_&_name
**/

saveUniversalFile :function (file_path, file_name, file_exe, aws_path){

	  fs.stat(file_path, function(err, file_inf) {

                var bodyStrea = fs.createReadStream( file_path );

                var param = {
                  Bucket: 'rtv01',
                  Key: aws_path,
                  Body: bodyStrea,
                  ACL: 'public-read',
                  ContentEncoding: 'base64',
                  ContentType: mimeType.mime(file_type),
                  ContentLength: file_inf.size
                };

        
                s3.putObject(param, function (err, data) {
                  if (err) {
                    console.log("Error uploading data: ", err);
                  } else {

                    console.log("Successfully uploaded "+mimeType.mime(file_type), data);
                  }
                });

             });

},




}