const randtoken = require('rand-token');
const argon2i = require('argon2-ffi').argon2i;

const crypto = require('crypto');
const Promise = require('bluebird');

const randomBytes = Promise.promisify(crypto.randomBytes);

const time = new Date();

const a_cur = 50;
const a_min = 50;
const a_changelog =
  'UPDATE: Starting from March 30th 2018 we are now called Vuga, please update the app to get new features like commenting on live tv & radio, news posts, find friends & chat plus many more...';

module.exports = {
  // For app update
  a_cur,
  a_min,
  a_changelog,

  // Create a random string @num = length of string
  // Return random string

  getUt(date) {
    const t = {
      dstr: `${date.getTime()}`,
      dnum: date.getTime(),
      d: date.now,
    };

    return t;
  },

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  },

  getAnonymousUser() {
    return {
      _id: 'anonymous',
      // Version updates of profile
      veri: 1,

      // Usermeta, basic info
      info: {
        // top level
        a: '',

        d: false,
        dd: '',

        // Init data
        isVerify: '',
        reg: new Date(),
        reg_unix_int: new Date().getTime(),

        // basic info
        userid: '',
        username: '',
        firstname: '',
        lastname: '',
        sex: '',
        email: '',
        phone: '',
        image_path: '',

        gmt: '',
        ethic: '',
        interest: [''],
      },

      // user location
      location: {
        // GEO
        longitude: 0,
        latitude: 0,

        // ADDRESS
        formattedAddress: '',
        streetName: '',
        city: '',
        country: '',
        countryCode: '',

        extra: {},
        administrativeLevels: {},
      },
    };
  },

  argonVerify(ob, callback) {
    console.log(`Argon Verify called hash = ${ob.hash}`);
    console.log(ob);
    const encodedHash = ob.hash;
    const password = new Buffer(ob.pw);
    try {
      argon2i.verify(encodedHash, password).then(correct => callback(correct));
    } catch (error) {
      console.log(error);
      callback(null);
    }
  },

  pw(arg) {
    let ret;
    setTimeout(() => {
      // ret = "hello";
      const password = new Buffer(arg);
      const options = { timeCost: 4, memoryCost: 1 << 14, parallelism: 2, hashLength: 64 };
      randomBytes(32)
        .then(salt => argon2i.hash(password, salt, options))
        .then(hash => (ret = hash));
    }, 0);
    while (ret === undefined) {
      require('deasync').sleep(0);
    }
    // returns hello with sleep; undefined without
    return ret;
  },

  /** Return random number
   * @param num number of characters* */
  rand(num) {
    return randtoken.generate(num);
  },

  pid() {
    return this.rand(15);
  },

  // AWS paths & credintals
  aws_host: 'https://s3.amazonaws.com/',
  aws_bucket: 'rtv01',
  aws_image_folder: 'vs/img/',
  aws_video_folder: 'vs/vid/',
  aws_audio_folder: 'vs/aud/',
  // Credintials
  accessKeyId: 'AKIAJ3UXKXPE6KFVFGSA',
  secretAccessKey: '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',
  region: 'us-east-1',

  // Only to be accessed by files in the sub directories of e.g these file => api/post/*.js
  // Directiory where s3 picks data
  api_public_image_folder: './public/uploads/image/',
  api_public_video_folder: './public/uploads/video/',
  api_public_audio_folder: './public/uploads/audio/',

  // Where Multipart saves all uploads
  api_public_folder: '/../../../public/uploads/dump/',

  // Amazon

  // Twilio
  TWILIO_ACCOUNT_SID: 'AC6654e67d6c7b6d91b2c9d4f6aaba8830',
  TWILIO_AUTH_TOKEN: '4d02576ce5d5bb75d541de19bc50f53a',
  TWILIO_SEND_NUMBER: '+12017205967',
};
