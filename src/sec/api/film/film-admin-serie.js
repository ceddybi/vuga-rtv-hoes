var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   FilmModel = mongoose.model('Film'),
   FilmSerie = mongoose.model('FilmSerie');
var _ = require('lodash');

var config = require('../static/variables');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + config.api_public_folder
});



// GET LIST
router.get('/',function(req, res){
       console.log("GET MANY SERIE");
       //console.log(req.query);
       //console.log(req.body);

       CB.findMany(FilmSerie, {}, function(d){
            if(d.status === "200"){

                var ts =[];
                console.log(d.data)
                  d.data.map(function(body, index){
                    var data = {
                        id: body._id,
                        film_id: body.film_id,    
                        film_name: body.film_name,
                        //year: body.meta.year,
                    };

                    ts.push(data);
                });

                console.log(ts.length)
                console.log(ts)
                res.status(200).send(ts);
            }

            else{
                console.log("No data")
                res.status(200).send([]);
            }
       });
});


function getSeasons(source){

/** 
 season0:
   [ { index_num0: '1', url0: '1' },
     { index_num0: '2', url0: '2' },
     { index_num0: '3', url0: '4' } ],
  season1:
   [ { index_num1: '1', url1: '1' },
     { index_num1: '2', url1: '2' } ] }


**/
    //from fetch
    var datatosend = [];

    seasons = ["1","2","3","4","5","6","7","8","9"];

    seasons.map(function(member, indexSeason){
        var x_season = "season"+indexSeason; // season0
       
        
        var x_index_num = "index_num"+indexSeason;
        var x_url = "url"+indexSeason;
        var x_title = "title"+indexSeason;
        var x_des = "des"+indexSeason;
        var x_image = "image"+indexSeason;

        var arrEpi = [];

        // get Episodes from season
        var episodesArr = source[x_season];

        // If season not null
        if(episodesArr!=undefined){
             console.log(x_season);
                  episodesArr.map(function(epi, epiIndex){
                        var episode ={
                            url:  epi[x_url], // = url0
                            episode_position: epiIndex,
                            title: epi[x_title],
                            des: epi[x_des],
                            image: {
                                cover: epi[x_image]
                            }
                        }
                        //Push the episode to season
                        arrEpi.push(episode)
                    });

                    // var to save
                    var seasonObj = {
                        
                        season_position: indexSeason,

                        episodes : arrEpi
                    }

                    datatosend.push(seasonObj);
        }
        
        

    });

    var film = {
        film_name: source.name,
        film_id:  source.id,
        seasons: datatosend
    }

    return film;


    // Now got the serie as var serie
    

    // TODO, reverse this function



}

// UPDATE
router.put('/:id',function(req, res){
       console.log("UPDATE CALLED");
       //console.log(req.body);
    var f = getSeasons(req.body);
    var id = req.params.id;
    //console.log(f.seasons[0].episodes[0]);

    var serie = {
        seasons: f.seasons
    }

    FilmSerie.findOneAndUpdate({_id: id}, {$set: serie, $inc: { "veri": 1 }},
            function(err, good){

                    if(good){

                        // Check or Create new serie
                        //SerieCheck(id, obj.meta.name, obj.meta.typo);

                        console.log("UPDATED SERIE");
                        res.status(200).send({data:""});
                    }
                    else{
                        console.log(err);
                        res.status(400).send({error:"Could not update"});
                    }
                }
        );
});



function reverseSeasons(source){

/** 
 * FROM
 [ { season_position: 0,
    episodes: [ [Object], [Object], [Object] ] },
  { season_position: 1,
    episodes: [ [Object], [Object], [Object] ] } ]

 TO
 season0:
   [ { index_num0: '1', url0: '1' },
     { index_num0: '2', url0: '2' },
     { index_num0: '3', url0: '4' } ],
  season1:
   [ { index_num1: '1', url1: '1' },
     { index_num1: '2', url1: '2' } ] }


**/
    //from fetch\
    console.log("Source");
    console.log(source);
    var datatosend = {};
    datatosend.film_name= source.film_name;
    datatosend.film_id=  source.film_id;
    datatosend.id=  source.id;

    //seasons = ["1","2","3","4","5","6","7","8","9"];

    source.seasons.map(function(seasonObj, indexSeason){
        var x_season = "season"+indexSeason; // season0
       
        
        var x_episode_position = "episode_position"+indexSeason;
        var x_url = "url"+indexSeason;
        var x_title = "title"+indexSeason;
        var x_des = "des"+indexSeason;
        var x_image = "image"+indexSeason;

        var arrEpi = [];

        // get Episodes from season
        var episodesArr = seasonObj.episodes;

        // If season not null
        if(episodesArr!=undefined){

            // TODO Sort episodes now

             console.log(x_season);
                  episodesArr.map(function(epi, epiIndex){

                        var episode ={};
                        //console.log(epi);
                          episode[x_url]= epi.url; // = url0
                          episode[x_title]= epi.title; // = url0
                          episode[x_des]= epi.des; // = url0
                          episode[x_image]= epi.image.cover; // = url0
                          episode[x_episode_position]=epi.episode_position;
                        //Push the episode to season
                        arrEpi.push(episode)
                    });

                    // var to save

                    datatosend[x_season] = arrEpi;
                    
        }
        
        

    });
     

    return datatosend;

}


// GET ONE
/**
 

 */

router.get('/:id',function(req, res){
       console.log("GET ONE");
       console.log(req.params);
       //console.log(req.body);

       var id = req.params.id;

    CB.findOne(FilmSerie, {_id: id}, function(gd){
            //console.log(gd);
            if(gd.status==="200"){
                
                console.log(reverseSeasons(gd.data));
                res.status(200).send(reverseSeasons(gd.data));
            }
            else{

                res.status(200).send({});
            }
    });
});

// CREATE
// TODO ADD MOVIE TIME DETECTOR
router.post('/', function(req, res){
       console.log("CREATE CALLED");
       //console.log(req.files);
       console.log(req.body);



});


// DELETE
router.delete('/:id',function(req, res){
       console.log("DELETE CALLED");
       var id = req.params.id;
       FilmSerie.remove({film_id:id}, function(c){
           if(c){
               res.status(200).send({error: "Delete not called"});
              
           }
           else{
               res.status(200).send({data:""});
           }
       })
});


module.exports = router;