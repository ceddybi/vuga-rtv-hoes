var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   FilmModel = mongoose.model('Film'),
   FilmSerie = mongoose.model('FilmSerie');
var _ = require('lodash');

var config = require('../static/variables');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + config.api_public_folder
});


function SerieCheck(id, name, typo){
    if(typo === "serie"){
            CB.findOne(FilmSerie, {"film_id": id}, function(data){
                        if(data.status === "200"){
                        // Already created
                            console.log("Serie Already Exists = "+ name)
                        }
                        else{
                            // Create a new Serie
                            var serie = {
                                 film_id: id,
                                  film_name: name,
                                   seasons: []
                            }
                            CB.create(FilmSerie, serie, function(d){
                                if(d.status==="200"){
                                    console.log("Created new serie "+ d.data.film_name+ " "+d.data.film_id);
                                }
                            });
                        }

            });
    }
}

function SerieDelete(id){

    FilmSerie.remove({film_id:id}, function(c){
                        if(c){
                            console.log("Serie Not deleted")
                        }
                        else{
                            console.log("Serie deleted")
                        }
    });
    
}



// GET LIST
router.get('/',function(req, res){
       console.log("GET MANY");
       //console.log(req.query);
       //console.log(req.body);

       CB.findMany(FilmModel, {}, function(d){
            if(d.status === "200"){

                var ts =[];
                  d.data.map(function(body){
                        var data = {
                        lang: body.meta.lang,
                        isTranslated: body.meta.isTranslated,
                        id: body._id,    
                        typo: body.meta.typo,
                        movie_url: body.movie.movie_url,
                        name: body.meta.name,
                        des: body.meta.des,
                        year: body.meta.year,
                        age_rating: body.meta.age_rating,
                        dir: body.meta.dir,
                        cast: body.meta.cast,
                        cat: body.meta.cat,
                        sub_cat: body.meta.sub_cat,
                        tags: body.meta.sub_cat,
                        // For Movie Viewer
                        cover: body.image.cover,
                        //item logo
                        logo: body.image.logo,
                        // For list views
                        banner: body.image.banner,
                        // For slider
                        ad: body.image.ad,
                    };

                    ts.push(data);
                });

                console.log(ts.length)
                res.status(200).send(ts);
            }

            else{
                console.log("No data")
                res.status(200).send([]);
            }
       });
});

// GET ONE

router.get('/:id',function(req, res){
       console.log("GET ONE");
       console.log(req.params);
       console.log(req.body);

       var id = req.params.id;

    CB.findOne(FilmModel, {_id: id}, function(gd){
            if(gd.status==="200"){

                var body = gd.data;
                var data = {
                        featured: body.featured,
                        lang: body.meta.lang,
                        isTranslated: body.meta.isTranslated,
                        id: body._id,    
                        typo: body.meta.typo,
                        movie_url: body.movie.movie_url,
                        name: body.meta.name,
                        des: body.meta.des,
                        year: body.meta.year,
                        age_rating: body.meta.age_rating,
                        dir: body.meta.dir,
                        cast: body.meta.cast,
                        cat: body.meta.cat,
                        sub_cat: body.meta.sub_cat,
                        tags: body.meta.sub_cat,

                        // TODO COUNTRIES
                        //countries: body.meta.countries,
                        available: body.meta.available,

                        // For Movie Viewer
                        cover: body.image.cover,
                        //item logo
                        logo: body.image.logo,
                        // For list views
                        banner: body.image.banner,
                        // For slider
                        ad: body.image.ad,
                };


                
                res.status(200).send(data);
            }
            else{

                res.status(401).send("Error");
            }
    });
});

// CREATE
// TODO ADD MOVIE TIME DETECTOR
router.post('/', function(req, res){
       console.log("CREATE CALLED");
       //console.log(req.files);
       //console.log(req.body);

       var body = req.body;


        var data = {
            featured: body.featured,
            meta: {
                    typo: body.typo,
                    name: body.name,

                    lang: body.lang,
                    isTranslated: body.isTranslated,

                    des: body.des,
                    year: body.year,
                    age_rating: body.age_rating,
                    dir: body.dir,
                    cast: body.cast,
                    cat: body.cat,
                    sub_cat: body.sub_cat,
                    tags: body.sub_cat,
                    //countries: body.countries,
                    available: body.available,
            },

            // FOR MOVIES
            movie: {
                movie_url: body.movie_url
            },

            image: {

                // For Movie Viewer
                cover: body.cover,

                //item logo
                logo: body.logo,

                // For list views
                banner: body.banner,
                
                // For slider
                ad: body.ad,

            },

            
        }

    CB.create(FilmModel, data, function(gd){
        if(gd.status==="200"){
            console.log("Success creating film");

            // Create new serie
            SerieCheck(gd.data._id, gd.data.meta.name, gd.data.meta.typo);

            res.status(200).send({id:gd.data._id});
        }
        else{
            console.log("Error creating film");
            console.log(gd.msg)
            res.status(401).send("Error");
        }
    });

});

// UPDATE
router.put('/:id',function(req, res){
       console.log("UPDATE CALLED");

       var id = req.params.id;
       var body = req.body;

        var obj = {

            featured: body.featured,
            meta: {
                    typo: body.typo,
                    name: body.name,
                    lang: body.lang,
                    isTranslated: body.isTranslated,

                    des: body.des,
                    year: body.year,
                    age_rating: body.age_rating,
                    dir: body.dir,
                    cast: body.cast,
                    cat: body.cat,
                    sub_cat: body.sub_cat,
                    tags: body.sub_cat,
                    //countries: body.countries,
                    available: body.available,
            },

            // FOR MOVIES
            movie: {
                movie_url: body.movie_url
            },

            image: {

                // For Movie Viewer
                cover: body.cover,

                //item logo
                logo: body.logo,

                // For list views
                banner: body.banner,
                
                // For slider
                ad: body.ad,

            },
 
        }
        
        FilmModel.findOneAndUpdate({_id: id}, {$set: obj, $inc: { "veri": 1 }},
            function(err, good){

                    if(good){

                        // Check or Create new serie
                        SerieCheck(id, obj.meta.name, obj.meta.typo);

                        console.log(good);
                        res.status(200).send({data:""});
                    }
                    else{
                        console.log(err);
                        res.status(400).send({error:"Could not update"});
                    }
                }
        );

       
});

// DELETE
router.delete('/:id',function(req, res){
       console.log("DELETE CALLED");
       var id = req.params.id;
       FilmModel.remove({_id:id}, function(c){
           if(c){
               res.status(401).send({error: "Delete not called"});
              
           }
           else{
               SerieDelete(id);
               res.status(200).send({data:""});
           }
       })
});


module.exports = router;