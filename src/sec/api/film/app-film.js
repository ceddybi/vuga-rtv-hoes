var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');

let locationUtils = require('../location/location-util');
let locationParse = require('../location/location-parse');
var mongoose = require('mongoose'),
   PostModel = mongoose.model('Post'),
   FilmModel = mongoose.model('Film'),
   FilmSerie = mongoose.model('FilmSerie'),
   MyFilmModel = mongoose.model('MyFilm');
var _ = require('lodash');

var config = require('../static/variables');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + config.api_public_folder
});



/**
 * Get all categories with films
 * @param userid
 * @param location
 * 
 */
router.post('/get/categories',function(req, res){
    var cate = [];
       CB.findMany(FilmModel, {}, function(data){
           console.log(data.data[0]);
                data.data.map(function(film){
                    if(_.contains(cate, film.meta.cat)){
                        
                    }
                    else{
                        cate.push(film.meta.cat);
                    }
                });
           res.status(200).send(cate);
       })
});


/**
 * Get all other films
 * @param userid
 * @param location
 */
router.post('/uni/all', upload.any(), function(req, res){
    var location = req.body.location;


    //let loc = JSON.parse(location);
    //console.log("Long = "+ loc.lon);

    //let fetchedLoc = locationUtils.syncGetLoc(loc);




    console.log("GET Other films");
    console.log(req.body);

    //http://www.tolkienlibrary.com/press/images/movie-tie-in-The-hobbit.jpg

       var that = [];
       //Only get available movies
       CB.findMany(FilmModel,  {"meta.available": true}, function(data){
           console.log(data.data.length);
           if(data.status==="200"){
               data.data.map(function(film){

                    // Convert Arrays to String
                    var sub_cat = film.meta.sub_cat.join(", ");
                    var tags = film.meta.tags.join(", ");
                    //var lang = film.meta.lang.join(", ");
                    var lang = film.meta.lang[0];
                    var bad =  film.rating.bad.join(", ");
                    var good = film.rating.good.join(", ");

                     var newfilm ={
                        _id: film._id,
                        __v: film.__v,
                        featured: film.featured,
                        movie: film.movie,
                        serie: film.serie,
                        veri: film.veri,
                        id_num: film.id_num,

                        rating: { 
                            "bad": bad, 
                            "good": good 
                        },

                        image:film.image,

                        meta:{ 
                                available: film.meta.available,
                                isTranslated: film.meta.isTranslated,
                                "lang": lang,
                                "tags": tags,
                                "sub_cat": sub_cat,
                                cat: film.meta.cat,
                                cast: film.meta.cast,
                                dir: film.meta.dir,
                                age_rating: film.meta.age_rating,
                                year: film.meta.year,
                                des: film.meta.des,
                                name: film.meta.name,
                                movie_url: film.meta.movie_url,
                                typo: film.meta.typo 
                            },
                            
                     }

                     that.push(newfilm);
               });
               console.log("Films sent = "+that.length)
               //console.log(that[0]);
               res.status(200).send(that);

           }

           else{
               console.log("Films sent = ")
               res.status(401).send("Error");
           }
           
       });
});



/**
 * Get film by id
 *  Returns one object
 * @param userid
 *  @param location
 * 
 */
router.post('/uni/id',function(req, res){
       
});


/**
 * Get all other films
 * @param userid
 * @param location
 */
router.post('/serie/all', upload.any(), function(req, res){
    var location = req.body.location;

    console.log("GET All serie films");
    console.log(req.body);

    //http://www.tolkienlibrary.com/press/images/movie-tie-in-The-hobbit.jpg

       var that = [];
       CB.findMany(FilmSerie, {}, function(data){
           console.log(data.data.length);
           if(data.status==="200"){

               res.status(200).send(JSON.stringify(data.data));

           }

           else{
               console.log("Films sent = ")
               res.status(401).send("Error");
           }
           
       });
});



/**
 * Get all user films
 * Return array
 * @param userid
 *  @param location
 * 
 */
router.post('/my/all',function(req, res){
       
});



/**
 * Find user myfilm and add track, else create new and add track aswell
 * @param userid
 * @param filmID, @param time, @param typo
 * if season = season_num, episode_num
 * 
 */
router.post('/my/put/track',function(req, res){
       
});


/**
 * Find movie and remove or add like to it
 * @param userid
 * @param filmID, @param like, @param unlike
 * 
 */
router.post('/my/put/vote',function(req, res){
       
});


module.exports = router;