/**
 * Created by vuga on 9/11/17.
 */

var mailgun = require('mailgun-js')({apiKey: "key-221954a88b1022ec333cc775908f360a", domain: "mail.vuga.io"});
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var wellcome_templateDir = path.join(__dirname, 'email_templates', 'wellcome');
var wellcome_email = new EmailTemplate(wellcome_templateDir);


function sendEmail(user){
    wellcome_email.render({
        name: user.name,
        phone: user.phone,
        msg: user.msg,
        loc: user.loc
    }, function(err, result){
        if(err){console.log(err)}
        else{
            //console.log("sucess "+result);
            sendMsg(result.html, user);

        };
    })
};




function sendMsg(html, user){
    mailgun.messages().send({
        from: 'Vuga Chat Bot<chat@mail.vuga.io>',
        to: 'Vuga Inc <incvuga@gmail.com>',
        subject: "Re: "+user.phone,
        html: html
    },   function(error, body){

        if(error) console.log(error);

        else console.log(body);

    });
};



module.exports = {
    sendEmail:sendEmail
}