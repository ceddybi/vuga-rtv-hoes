const CB = require('../static/cb');
const _ = require('lodash');
const config = require('../static/variables');

const TRAW = require('./traw');

let mongoose = require('mongoose');
let  Day = mongoose.model('Day');
let  DayTo = mongoose.model('Dayto');
let  UserProfile = mongoose.model('UserProfile');

function midnight() {
  console.log("It's " + 'midnight Baby v2');
  const d = new Date();
  d.setHours(0, 0, 0, 0);

  DayTo.findOne({ typo: 't' }, {}, (erro, resu) => {
    if (resu) {
      // Day to found, copy and rest the shit

      const v = new Day({
        created_at: new Date(d),
        unix_time: `${  d.getTime()}`,

        all_count_int: resu.all_count_int,
        all_count_str: resu.all_count_int,

        user_new_count_int: resu.user_new_count_int,
        user_new_count_str: resu.user_new_count_str,

        user_active_count_int: resu.user_active_count_int,
        user_active_count_str: resu.user_active_count_str,

        video_count_int: resu.video_count_int,
        video_count_str: resu.video_count_str,

        videogrp_count_int: resu.videogrp_count_int,
        videogrp_count_str: resu.videogrp_count_str,

        tv_count_int: resu.tv_count_int,
        tv_count_str: resu.tv_count_str,

        radio_count_int: resu.radio_count_int,
        radio_count_str: resu.radio_count_str,

        music_count_int: resu.music_count_int,
        music_count_str: resu.music_count_str,

        artist_count_int: resu.artist_count_int,
        artist_count_str: resu.artist_count_str,

        web_count_int: resu.web_count_int,
        web_count_str: resu.web_count_str,

        other_count_int: resu.other_count_int,
        other_count_str: resu.other_count_str,

        // New
        app_count_int: resu.app_count_int,
        app_count_str: resu.app_count_str,
        film_count_int: resu.film_count_int,
        film_count_str: resu.film_count_str,
      });

      // Save the day
      v.save((error, result) => {
        if (error) {
          console.error(error);
          //  throw error;
        } else {
          console.log('Created new **Day**');
        }
        // res.status(200).send(result);
      });

      resu.all_count_int = 1;
      resu.all_count_str = '1';

      resu.user_new_count_int = 1;
      resu.user_new_count_str = '1';

      resu.user_active_count_int = 1;
      resu.user_active_count_str = '1';

      resu.video_count_int = 1;
      resu.video_count_str = '1';

      resu.videogrp_count_int = 1;
      resu.videogrp_count_str = '1';

      resu.tv_count_int = 1;
      resu.tv_count_str = '1';

      resu.radio_count_int = 1;
      resu.radio_count_str = '1';

      resu.music_count_int = 1;
      resu.music_count_str = '1';

      resu.artist_count_int = 1;
      resu.artist_count_str = '1';

      resu.web_count_int = 1;
      resu.web_count_str = '1';

      resu.other_count_int = 1;
      resu.other_count_str = '1';

      // New
      resu.app_count_int = 1;
      resu.app_count_str = '1';
      resu.film_count_int = 1;
      resu.film_count_str = '1';

      // reset the day to
      resu.save((error, result) => {
        if (error) {
          console.error(error);
          //  throw error;
        } else {
          console.log('Reset Dayto');
        }
        // res.status(200).send(result);
      });
    }
  });
}

async function updateLastActive(userid) {
  let userObj;

  try {
    userObj = await UserProfile.findOne({ 'info.userid': userid }).exec();

    if (!_.isEmpty(userObj)) {
      userObj.lastactive = config.getUt(new Date());
      userObj.save((err, data) => {
        if (!err) {
          // Updated lastseen of user
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
}

async function note(typo, id, userid, duration) {
  let time;
  let d;
  try {
    if (!_.isEmpty(typo)) {
      d = await DayTo.findOne({ typo: 'v2' }).exec();
      time = parseInt(duration);

      switch (typo) {
        case 'app':
          const ax = d.app_count_int + time;
          (d.app_count_int = ax), (d.app_count_str = `${ax}`);
          break;
        case 'tv':
          const atv = d.tv_count_int + time;
          (d.tv_count_int = atv), (d.tv_count_str = `${atv}`);
          break;
        case 'radio':
          const ar = d.radio_count_int + time;
          (d.radio_count_int = ar), (d.radio_count_str = `${ar}`);
          break;
        case 'film':
          const af = d.film_count_int + time;
          (d.film_count_int = af), (d.film_count_str = `${af}`);
          break;
      }

      d.save((err, saved) => {
        if (!err) {
          // TODO ADD USER
          console.log('Saved todo');
          updateLastActive(userid);
        } else {
          //
          console.log('Failed to save todo');
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  midnight,
  noteRaw: note,
};
