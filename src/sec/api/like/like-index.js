var express = require('express');
var router = express.Router();
var _ = require('lodash');

var mongoose = require('mongoose'),
   LikeModel = mongoose.model('Like');


/** @pid, @userid */
// TODO need to validate req.body attributes
router.post('/create',function(req, res){

var userid = req.body.userid;
var pid = req.body.pid;


  var dataTosave = {
    meta: {
      pid: pid
    },
    user: {
      userid: userid
    },
  }


      CB.create(LikeModel, dataToSave, function(good){
            if(good.status === "200"){
                
                  // send error now failed to
                  res.status(200).send({data:good.data,  status:"200", msg:"Post created"});
                }
                else {
                  // send error now failed to
                  res.status(400).send({status:"400", msg:"Server Failed to save data", data:{}});
                }
      });
   
});


// TODO verify req.body
/** @pid, @userid */
router.post('/delete',function(req, res){

    var pid = req.body.pid;
    var userid = req.body.userid;

    var todo = { "user.userid": userid, "meta.pid": pid};

    var dataToSave = {$set: {"meta.unlike": true} };

    // Find, then update
    CB.update(LikeModel,todo, dataToSave,{}, function(good){
              if(good.status === "200"){
                      // send error now failed to
                      res.status(200).send(good);
                  }
                  else {
                      // send error now failed to
                      res.status(400).send(good);
                  }
    });

});



/** Get all likes from pid */
router.post('/get',function(req, res){

   var pid = req.body.pid;

   //Check if userid is not null
   if(!pid){
     res.status(400).send({status:"400", msg:"Not found"});
   }
   else{
        CB.getOneOrMany(LikeModel,"many",{"meta.pid": pid},  req, res);
   }

});




module.exports = router;