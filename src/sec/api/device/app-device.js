const config = require('../static/variables');
const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
const _ = require('lodash');

const deviceUtils = require('./device-utils');

let mongoose = require('mongoose'),
  OldUser = mongoose.model('User'),
  UserProfile = mongoose.model('UserProfile'),
  UserDevice = mongoose.model('UserDevice'),
  SyncSms = mongoose.model('SyncSms'),
  SyncCall = mongoose.model('SyncCall'),
  SyncContact = mongoose.model('SyncContact');

// var chatApp = require('./chat-app');

/**
 * @params userid, imei, unique,osid,appversion, wlan
 * * */

// Sync device info
// userid, device, location,
// Returns user obj
router.post('/sync/device', async (req, res) => {
  let imei;
  let UniqueDevice;
  console.log('Sync device data');
  // console.log(req.body);
  let userid = req.body.userid;
  let typo = req.body.typo;
  let device = req.body;
  // let location = JSON.parse(req.body.location);

  try {
    // check  if location is not empty
    if (!_.isEmpty(req.body.location)) {
      device.location = JSON.parse(req.body.location);
    }

    // Check if anonymous,
    if (!_.isEmpty(userid)) {
      UniqueDevice = await UserDevice.findOne({
        'user.userid': userid,
        'info.unique': device.unique,
      }).exec();
    } else {
      // Anonymous bitch
      UniqueDevice = await UserDevice.findOne({ 'info.unique': device.unique }).exec();
    }

    imei = device.imei;

    if (!_.isEmpty(UniqueDevice)) {
      // TODO UPDATE DEVICE
      deviceUtils.updateUserDevice(device, UniqueDevice);
    } else {
      // Check if ime is possible

      // IF imei sent is not empty
      if (!_.isEmpty(imei)) {
        let imeiDevice;
        // Check anonymous
        if (!_.isEmpty(userid)) {
          imeiDevice = await UserDevice.findOne({
            'user.userid': userid,
            'info.imei': imei,
          }).exec();
        } else {
          // Anonymous bitch
          imeiDevice = await UserDevice.findOne({ 'info.imei': imei }).exec();
        }

        if (!_.isEmpty(imeiDevice)) {
          // Finally got the device

          // TODO UPDATE USER DEVICE
          deviceUtils.updateUserDevice(device, imeiDevice);
        } else {
          // No existing device, so create new
          deviceUtils.createUserDevice(typo, userid, device);
        }
      } else {
        // imei empty so create new device
        deviceUtils.createUserDevice(typo, userid, device);
      }
    }

    return res.status(200).json({ status: '200', msg: 'Success message' });
  } catch (error) {
    console.log(error);
    return res.status(200).json({ status: '400', msg: 'Error' });
  }
});

// Sync SMS's on the device
// Params userid, sms = {msg, to, from, time,status}
router.post('/sync/sms', (req, res) => {
  const user = CB.syncGetUser(req.body.userid);
  const sms = JSON.parse(req.body.sms);

  res.status(200).send({ status: '200', msg: 'Success' });

  if (!_.isEmpty(user)) {
    deviceUtils.loopSms(user, sms);
  }

  console.log('First = ' + sms[0]);
  console.log(' Sms received = ' + sms.length);

  // res.status(200).send({status: "200", msg:"Success"});
});

// Sync Call's on the device
// Params userid, calls = {from,to,time,duration}
router.post('/sync/call', (req, res) => {
  const user = CB.syncGetUser(req.body.userid);
  const calls = JSON.parse(req.body.calls);

  res.status(200).send({ status: '200', msg: 'Success' });

  if (!_.isEmpty(user)) {
    deviceUtils.loopCall(user, calls);
  }

  console.log('First = ' + calls[0]);
  console.log(' Calls received = ' + calls.length);

  //res.status(200).send({status: "200", msg:"Success"});
});

// Sync Contacts on the device
// Params userid, array = {id,name,phone,obj}
router.post('/invite/contacts', (req, res) => {
  // console.log(req.body.contacts);
  const contacts = JSON.parse(req.body.contacts);

  const user = CB.syncGetUser(req.body.userid);

  res.status(200).send({ status: '200', msg: 'Success' });

  if (!_.isEmpty(user)) {
    deviceUtils.loopContact(user, contacts);
  }
  console.log(' Contacts received = ' + contacts.length);

  //res.status(200).send({status:"200", msg:""});
});

module.exports = router;
