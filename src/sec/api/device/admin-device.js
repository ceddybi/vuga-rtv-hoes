var config = require('../static/variables');
var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var _ = require('lodash');

var deviceUtils = require('./device-utils');


var mongoose = require('mongoose'),
   OldUser = mongoose.model('User'),
   UserProfile = mongoose.model('UserProfile'),
   UserDevice = mongoose.model('UserDevice'),
   SyncSms = mongoose.model('SyncSms'),
   SyncCall = mongoose.model('SyncCall'),
   SyncContact = mongoose.model('SyncContact');

   //var chatApp = require('./chat-app');




/**
 * @params userid, imei, unique,osid,appversion, wlan
 * **/


// Sync device info
//userid
router.post('/get/device/id', function(req, res){
    deviceUtils.userGetContact(req, res);
});

// userid
router.post('/get/contact/id', function(req, res){
    deviceUtils.userGetContact(req, res);
});


// userid
router.post('/get/sms/id', function(req, res){

    deviceUtils.userGetSms(req, res);

});



// Sync Call's on the user
// Params userid,
router.post('/get/call/id', function(req, res){

    deviceUtils.userGetCall(req, res);
   
});








module.exports = router;