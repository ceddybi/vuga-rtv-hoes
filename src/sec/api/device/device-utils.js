var config = require('../static/variables');
var CB =  require('../static/cb');
var locationUtil =  require('../location/location-util');
var _ = require('lodash');

var mongoose = require('mongoose'),
    OldUser = mongoose.model('User'),
    UserProfile = mongoose.model('UserProfile'),
    UserDevice = mongoose.model('UserDevice'),
    SyncSms = mongoose.model('SyncSms'),
    SyncCall = mongoose.model('SyncCall'),
    SyncContact = mongoose.model('SyncContact');


// Save device & location
function updateUserDevice(newDevice, deviceObj){
    deviceObj.info = {
        wlan: newDevice.wlan,
        imei: newDevice.imei,
        osid: newDevice.osid,
        pushtoken: newDevice.pushtoken,
        unique: newDevice.unique,
        appversion: newDevice.appversion,
        active: {
            last: new Date().getTime()+""
        }
    };
    //deviceObj.info = newDevice;
    deviceObj.ut = config.getUt(new Date());

    deviceObj.save(function(error, save){
        if(error){
            console.log("Error updating user device");
        }
        else{
            console.log("Success updating user device");

            // IF not empty update device and user location
            if(!_.isEmpty(newDevice.location)){
                locationUtil.saveUserDevice(newDevice.location, deviceObj._id);
            }
        }
    });

}

async function createUserDevice(typo, userid, device){

    var toDevice;
    var user;

    var dev = {};

    try {
        user = await CB.asyncGetUser(userid);
        //Anonymous
        if(!_.isEmpty(user)) {
            dev.user = user.info;
        }

        dev.info = {
            wlan: device.wlan,
            imei: device.imei,
            osid: device.osid,
            pushtoken: device.pushtoken,
            unique: device.unique,
            appversion: device.appversion,
            active: {
                last: new Date().getTime()+""
            }
        };

        dev.ut = config.getUt(new Date());
        dev.typo = typo;

        toDevice = new UserDevice(dev);
        toDevice.save(function (error, saved){

            if(!error){
                console.log("Saved Device");

                // check if location is not empty
                if(!_.isEmpty(device.location)){
                    locationUtil.saveUserDevice(device.location, toDevice._id);
                }

            }
            else{
                console.log("Failed to save device");
            }

        });
    }
    catch (error) {
        console.log(error);
    }

}

//takes user,sms[] and creates sms sync objects
function loopSms(user,sms){
    //sms = JSON.parse(sms);
    sms.map(function (sm) {
        sm = JSON.parse(sm);
        let smx = {
            user: user.info,
            sms: {
                address: sm._address,
                typo: sm._folderName,
                id: sm._id,
                readState: sm._readState,
                time: sm._time,
                msg: sm._msg,
            },
            ut: config.getUt(new Date())
        };
        /**
        SyncSms.findOne({"user.userid": smx.user.userid, "sms.id": smx.sms.id },{},{}, function(err, d){

              if(d){
                  // update d
                  SyncSms.updateById()
              }
              else{
                  //Else save the new obj
                  let c = new SyncSms(smx);
                  c.save(function(er,saved){
                  
                  });
              }
        }); **/
         
        SyncSms.update({"user.userid": user.info.userid, "sms.id": sm._id}, smx, {upsert: true, setDefaultsOnInsert: true},
            function(error, saved){
            }); 
    });
}

// user, calls[]
function loopCall(user,calls){
    //calls = JSON.parse(calls);
    calls.map(function (cal) {
        cal = JSON.parse(cal);
        let smx = {
            user: user.info,
            call: cal,
            ut: config.getUt(new Date())
        };
        SyncCall.update({"user.userid": smx.user.userid, "call.date": smx.call.date}, smx, {upsert: true},
            function(error, saved){
            });
    });
}

// user, contacts[]
function loopContact(user,contacts){
    //contacts = JSON.parse(contacts);
    contacts.map(function (con) {
        con = JSON.parse(con);
        let smx = {
            user: user.info,
            contact:con,
            ut: config.getUt(new Date())
        };
        SyncContact.update({"user.userid": smx.user.userid, "contact.id": smx.contact.id}, smx, {upsert: true, setDefaultsOnInsert: true},
            function(error, saved){
            });
    });
}



// userid
function UserGetSms(req, res) {
    let userid = req.body.userid;
    let sms;

    try {
        sms = SyncSms.find({"user.userid": userid}).exec();
        let tosend = [];
        sms.map(function (sm) {
            let x = sm.sms;
            tosend.push(x);
        });

        res.status(200).json({status: "200", data: tosend, msg:""});

    }
    catch (error) {
        console.log(error);
        res.status(200).json({status: "400", data: [], msg:"Error"});
    }



};

function UserGetContact(req,res) {
    let userid = req.body.userid;
    let contacts = CB.syncGetMany(SyncContact, {"user.userid": userid});

    let tosend = [];
    contacts.map(function (sm) {
        let x = sm.contact;
        tosend.push(x);
    });

    res.status(200).json({status: "200", data: tosend, msg:""});

};

function UserGetCall(req,res) {

    let userid = req.body.userid;
    let calls = CB.syncGetMany(SyncCall, {"user.userid": userid});

    let tosend = [];
    calls.map(function (sm) {
        let x = sm.call;
        tosend.push(x);
    });

    res.status(200).json({status: "200", data: tosend, msg:""});


};



var exports = module.exports = {
    updateUserDevice: updateUserDevice,
    createUserDevice:createUserDevice,

    loopCall: loopCall,
    loopSms: loopSms,
    loopContact: loopContact,

    userGetSms: UserGetSms,
    userGetCall: UserGetCall,
    userGetContact: UserGetContact
};