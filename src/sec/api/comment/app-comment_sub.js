/**
 * Created by vuga on 9/3/17.
 */



/**
 // Get all sub comments
 - sort  - date/time, {from,to}
 - null push latest min 20, or pull 20 with many replies


 // Create comment
 // Edit comment
 // Delete comment


 **/


/**
 * Created by vuga on 9/3/17.
 */


/**
 // Get all comments
 - sort  - date/time, {from,to}
 - null push latest min 20


 // Create comment
 // Edit comment
 // Delete comment


 **/



var express = require('express');
var router = express.Router();
var _ = require('lodash');

import config from '../static/variables';
import CB from '../static/cb';

var mongoose = require('mongoose'),
    Scmt = mongoose.model('Scmt');


/** @pid, @commentid, @userid, @text */
// TODO need to validate req.body attributes
router.post('/create',function(req, res){

    var commentid = req.body.commentid;
    var userid = req.body.userid;
    var pid = req.body.pid;
    var text = req.body.text;

    var dataTosave = {
        meta: {
            pid: pid,
            commentid: commentid
        },
        parent: {
            id: commentid,

            //TODO For nesting replies
            typo: "",
        },
        user: {
            userid: userid
        },
        text: {
            text: text
        },
        ut: config.getUt(new Date())
    };


    CB.create(Scmt, dataTosave, function(good){
        if(good.status === "200"){
            // send error now failed to
            res.status(200).send({data:good.data,  status:"200", msg:"Reply created"});
        }
        else {
            // send error now failed to
            res.status(200).send({status:"400", msg:"Server Failed to save data", data:{}});
        }
    });

});


// TODO verify req.body

/** @scmtid, @text */
router.post('/update',function(req, res){

    var scid = req.body.scid;
    var text = req.body.text;

    var todo = { "meta.commentid": commentid};

    var dataToSave = {$set: {"text.text": text} };


    // Find, then update
    CB.update(Scmt,todo, dataToSave,{}, function(good){
        if(good.status === "200"){
            // send error now failed to
            res.status(200).json({status: "200", msg:""});
        }
        else {
            // send error now failed to
            res.status(200).json({status: "400", msg:"Error updating comment"});
        }
    });

});


/** Delete one comment */
router.post('/delete/',function(req, res){

    // Check if @commentid is null
    var commentid = (req.body.commentid || "");


    if(_.isEmpty(commentid)){
        res.status(200).json({status:"400", msg:"Not found"});
    }
    else{
        CB.deleteOne(Scmt, {"meta.commentid": commentid},  {"meta.commentid": commentid}, function(good){
            if(good.status === "200"){
                res.status(200).json({status:"200", msg:"Success"});
            }
            else {
                res.status(200).json({status:"400", msg:"No data found", data: {}});
            }
        });
    }


});

/** Get many comments from post id
 * Sort  = start,up boolean, count
 * */

router.post('/get',function(req, res){

    var pid = (req.body.pid || "");
    var sort = (req.body.sort || "");

    //default to down
    var query = {
        "meta.pid": pid,
        "ut.cnum":{$lte: new Date().getTime()}
    };

    let count = 20;


    //Check if userid is not null
    if(!_.isEmpty(pid)){
        res.status(200).json({status:"400", msg:"Not found"});
    }
    else{

        // Time to sort

        // if not null
        if(!_.isEmpty(sort)){
            // sort here
            //TODO if sort is object instead of string
            sort  = JSON.parse(sort);
            let start = (sort.start || new Date().getTime());
            count = (sort.count || 20);



            //Going up
            // Sorting on query
            if(sort.up){
                query = {
                    "meta.pid": pid,
                    "ut.cnum":{$gte: start }
                };
            }

            //Check if up or down
            CB.CBfindMany(Scmt,query, {"ut.dnum": -1},true, count,function (dat){
                if(dat.status === "200"){
                    res.status(200).json({status: "200", msg:"", data:dat.data});
                }
                else {

                    res.status(200).json({status: "400", msg:"Error finding comments", data:[]});
                }
            });

        }
        else {
            // No sort
            //Check if up or down
            CB.CBfindMany(Scmt,query, {"ut.dnum": -1},true, count,function (dat){
                if(dat.status === "200"){
                    res.status(200).json({status: "200", msg:"", data:dat.data});
                }
                else {

                    res.status(200).json({status: "400", msg:"Error finding comments", data:[]});
                }
            });


        }

    }

});




module.exports = router;