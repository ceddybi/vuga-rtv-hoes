/**
 * Created by vuga on 9/4/17.
 */


/**
 * *********Actions,
 * user, user_msg, view, reply,comment,vote_up,vote_down
 *
 *
 * ON////////
 * eg Reply ON comment or sub_comment
 * ..
 *
 * ALL
 * user = user_msg, view, reply,comment,vote_up,vote_down
 *
 * view = user, user_msg, post
 *
 * Reply = comment, sub_comment
 *
 * comment = post
 *
 * vote_* = post, comment
 *
 * ..
 *
 * *********Subscribers = notifiyers
 * user,
 *
 * ON///////
 * eg user ON post, comment, sub_comment
 *
 * ALL
 * user = user, view, post, comment, sub_comment
 *
 *...
 *
 *
 * ********Notifications
 *
 * user,user_msg, post, comment, sub_comment
 *
 * ON////////
 *
 * user = views
 *
 *
 *
 ******* Followups on any
 *
 *      ||
 *     \  /        Duration = 10 min
 *      \/
 *
 ******* Shoutout once done
 * */