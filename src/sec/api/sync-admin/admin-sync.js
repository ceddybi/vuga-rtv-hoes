/**
 * Created by vuga on 8/4/17.
 */
var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var _ = require('lodash');


var mongoose = require('mongoose'),
    OldUser = mongoose.model('User'),
    UserProfile = mongoose.model('UserProfile'),
    UserDevice = mongoose.model('UserDevice'),
    SyncSms = mongoose.model('SyncSms'),
    SyncCall = mongoose.model('SyncCall'),
    SyncContact = mongoose.model('SyncContact');


// Sync Contacts on the device
// Params userid, array = {id,name,phone,obj}
router.post('/get/user/id', function(req, res){

    let user = CB.syncGetUser(req.body.userid);
    let s;
    if(!_.isEmpty(user)){
        s={status:"200", msg:"", user:user};
    }
    else {
        s={status:"400", msg:"", user:{}};
    }

    res.status(200).send(s);

});

module.exports = router;
