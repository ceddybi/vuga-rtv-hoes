/**
 * Created by vuga on 7/24/17.
 */

const config = require('../static/variables');
const CB = require('../static/cb');
const _ = require('lodash');

let mongoose = require('mongoose'),
  OldUser = mongoose.model('User'),
  UserProfile = mongoose.model('UserProfile'),
  UserDevice = mongoose.model('UserDevice'),
  SyncSms = mongoose.model('SyncSms'),
  SyncCall = mongoose.model('SyncCall'),
  SyncContact = mongoose.model('SyncContact');

const HttpsAdapter = require('node-geocoder/lib/httpadapter/httpsadapter.js');
const NodeGeocoder = require('node-geocoder');

const httpAdapter = new HttpsAdapter(null, {
  headers: {},
});

const mapsOptions = {
  provider: 'google',
  httpAdapter,

  // Rwanda TV
  apiKey: 'AIzaSyAnrVE3xfJceR2CbEGzsKNrBBaOF1_h5cU',
  formatter: null, // 'gpx', 'string', ...
};

const geocoder = NodeGeocoder(mapsOptions);

async function saveUserLoc(loc, userid) {
  let user;
  try {
    user = await CB.asyncGetUser(userid);

    // check the user
    if (!_.isEmpty(user)) {
      // check is current location
      if (user.location != undefined) {
        // now check if not empty
        console.log(
          `Userlat = ${user.location.latitude} !=${loc.lat}  UserLog=${user.location.longitude} !=${
            loc.lon
          }`
        );
        if (user.location.latitude === loc.lat && user.location.longitude === loc.lon) {
          // Do nothing
        } else {
          // Fetch location and save
          geocoder.reverse(loc, (err, res) => {
            console.log(res);

            if (res) {
              user.location = res[0];
              user.location.longitude = loc.lon;
              user.location.latitude = loc.lat;
              user.lastactive = config.getUt(new Date());

              user.save((error, saved) => {
                if (!error) {
                  console.log('User location saved');
                } else {
                  console.log('User location failed to save');
                }
              });
            } else {
              console.log('Location fetch error');
            }
          });
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
}

async function saveUserDeviceLoc(loc, deviceid) {
  let device;

  try {
    device = await UserDevice.findOne({ _id: deviceid }).exec();
    // check the user
    if (!_.isEmpty(device)) {
      // check is current location
      if (device.location.longitude != undefined) {
        // now check if not empty
        if (device.location.latitude === loc.lat && device.location.longitude === loc.lon) {
          // Do nothing
        } else {
          // Fetch location and save
          geocoder.reverse(loc, (err, res) => {
            // console.log(res);

            if (res) {
              device.location = res[0];
              device.location.longitude = loc.lon;
              device.location.latitude = loc.lat;
              device.lastactive = config.getUt(new Date());

              device.save((error, saved) => {
                if (!error) {
                  console.log('Device location saved');

                  // Save user location
                  saveUserLoc(loc, device.user.userid);
                } else {
                  console.log('Device location failed to save');
                }
              });
            } else {
              console.log('Device location fetch error, N-E');
            }
          });
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
}

// loc=lon,lat
function syncGetLoc(loc) {
  // console.log("Location ");
  // console.log(loc);
  let ret;
  setTimeout(() => {
    // ret = "hello";
    geocoder.reverse(loc, (err, res) => {
      if (!err) {
        ret = res;
        // console.log(res);
        if (res === undefined) {
          ret = {};
        }
      } else {
        console.log('Location fetch error');
        ret = {};
      }
    });
  }, 0);
  while (ret === undefined) {
    require('deasync').sleep(0);
  }
  // returns hello with sleep; undefined without
  return ret;
}

function syncGetCountryCode(loc, userObj) {
  // console.log("syncGetCountryCode");
  // console.log(loc);
  // console.log(userObj);
  let ret;

  // fetch from the net
  return new Promise((resolve, reject) => {
    if (userObj) {
      if (!_.isEmpty(userObj.location.countryCode)) {
        // TODO UPDATE user location
        resolve(userObj.location.countryCode);
      }
      geocoder.reverse(loc, (err, res) => {
        if (!err) {
          if (res === undefined) {
            ret = '';
          }

          ret = res[0].countryCode;
          // console.log(res);
          resolve(ret);
        } else {
          console.log('Location fetch error');
          ret = '';
          resolve(ret);
        }
      });
    } else {
      resolve('');
    }
    //
  });
}

function syncGetCountryCodeCallback(loc, userObj, callback) {
  // console.log("syncGetCountryCode");
  // console.log(loc);
  // console.log(userObj);
  let ret;

  if (!_.isEmpty(userObj)) {
    if (!_.isEmpty(userObj.location.countryCode)) {
      // TODO UPDATE user location
      callback(userObj.location.countryCode);
    } else {
      // fetch from the net
      geocoder.reverse(loc, (err, res) => {
        if (!err) {
          ret = res[0].countryCode;
          // console.log(res);

          // IF empty
          if (res === undefined) {
            callback('');
          } else {
            callback(res);
          }
        } else {
          console.log('Location fetch error');
          callback('');
        }
      });
    }
  } else {
    // User empty
    callback('');
  }
}

function checkIfAbroad(cc) {}

module.exports = {
  saveUserDevice: saveUserDeviceLoc,
  syncGetLoc,
  syncGetCountryCode,
  syncGetCountryCode,

  // Callback
  cbGetCountryCode: syncGetCountryCodeCallback,
};
