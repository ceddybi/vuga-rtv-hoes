/**
 * Created by vuga on 7/24/17.
 */


let config = require('../static/variables');
let CB =  require('../static/cb');
let _ = require('lodash');

let mongoose = require('mongoose'),
    OldUser = mongoose.model('User'),
    UserProfile = mongoose.model('UserProfile'),
    UserDevice = mongoose.model('UserDevice'),
    SyncSms = mongoose.model('SyncSms'),
    SyncCall = mongoose.model('SyncCall'),
    SyncContact = mongoose.model('SyncContact');

// Available regions  = US,RW,BI,KE,UG,TZ,NG,CD,


function parseFilm(code) {
 let films = [];
 
 // Western
 let usmv = "Latest Movies";    
 let inmv = "Indian Movies";

 // East Africa
 let rwmv = "Rwandan Movies";
 let kemv = "Kenyan Movies";
 let tzmv = "Tanzanian Movies";
 let ugmv = "Ugandan Movies";
 let bimv = "Burundian Movies";
    
 //DRC
 let cdmv= "Congo DR Movies";
 
 //West Africa
 let ngmv= "Nigerian Movies";

 // Southern Africa
 let zamv= "South African Movies";


    let rw = {index:1, name: rwmv, field: "meta.cat", query: "RW"};

    let us= {index:1, name: usmv, field: "meta.cat", query: "US"};
    let ind= {index:1, name: inmv, field: "meta.cat", query: "IN"};

    let ng= {index:2, name: ngmv, field: "meta.cat", query: "NG"};
    let tz=  {index:3, name: tzmv, field: "meta.cat", query: "TZ"};
    let ug = {index:4, name: ugmv, field: "meta.cat", query: "UG"};
    let ke = {index:5, name: kemv, field: "meta.cat", query: "KE"};
    let cd =  {index:6, name: cdmv, field: "meta.cat", query: "CD"};

    let bi= {index:7, name: bimv, field: "meta.cat", query: "BI"};

    let za= {index:7, name: zamv, field: "meta.cat", query: "ZA"};


    switch (code){
        
        // East Africa
        case "RW":
            films = [us,ng,ind,za,tz,ug,ke,cd,bi];
            break;
        case "BI":
            films = [us,rw,ind,za,ng,tz,ug,ke,cd];
            break;
        case "UG":
            films = [us,rw,ind,za,ng,tz,ke,cd,bi];
            break;
        case "KE":
            films = [us,rw,ng,ind,za,tz,ug,cd,bi];
            break;
        case "TZ":
            films = [us,rw,ng,ind,za,ke,ug,cd,bi];
            break;

            
        // Other countries
        case "CD":
            films = [us,rw,ng,ind,za,tz,ug,ke,bi];
            break;
        case "NG":
            films = [us,rw,ind,za,ke,tz,ug,cd,bi];
            break;

        // Indian movies
        case "IN":
            films = [us,rw,ng,za,ke,tz,ug,cd,bi];
            break;

        // South Africa
        case "ZA":
            films = [us,rw,ng,ind,ke,tz,ug,cd,bi];
            break;

        default:
            films = [rw,ng,ind,za,tz,ke,ug,cd,bi];
            break;

    }


    return films;

}

function parseTv(code) {
    let tv = [];
    // Available films  = US,RW,BI,KE,UG,TZ,NG,CD,
    let usr = "Latest TVs";
    let rwr = "Rwandan TVs";
    let ngr = "Nigerian TVs";
    let tzr = "Tanzanian TVs";
    let ugr = "Ugandan TVs";
    let ker = "Kenyan TVs";
    let cdr = "Congo DR TVs";
    let bir = "Burundian TVs";


   let rw = {index:1, name: rwr, field: "countryCode", query: "RW"};
   let us= {index:1, name: usr, field: "countryCode", query: "US"};
   let ng= {index:2, name: ngr, field: "countryCode", query: "NG"};
   let tz=  {index:3, name: tzr, field: "countryCode", query: "TZ"};
   let ug = {index:4, name: ugr, field: "countryCode", query: "UG"};
   let ke = {index:5, name: ker, field: "countryCode", query: "KE"};
   let cd =  {index:6, name: cdr, field: "countryCode", query: "CD"};
   let bi= {index:7, name: bir, field: "countryCode", query: "BI"};

    switch (code){

        // East Africa
        case "RW":
            tv = [rw,us,ng,ug,ke,cd,tz,bi];
            break;
        case "BI":
            tv = [bi,rw,us,ng,tz,ug,ke,cd];
            break;
        case "UG":
            tv = [ug,rw,us,ng,tz,ke,cd,bi];
            break;
        case "KE":
            tv = [ke,rw,us,ng,tz,ug,cd,bi];
            break;
        case "TZ":
            tv = [tz,rw,us,ng,ug,ke,cd,bi];
            break;


        // Other countries
        case "CD":
            tv = [cd,rw,us,ng,tz,ug,ke,bi];
            break;

            
         //    
        case "NG":
            tv = [ng,rw,us,tz,ug,ke,cd,bi];
            break;

        default:
            tv = [rw,ng,tz,cd,bi,ke,ug];
            break;

    }

    return tv;

}

function parseRadio(code) {
    let radio = [];
    let usr = "Latest Radios";
    let rwr = "Rwandan Radios";
    let ngr = "Nigerian radios";
    let tzr = "Tanzanian radios";
    let ugr = "Ugandan radios";
    let ker = "Kenyan radios";
    let cdr = "Congo DR radios";
    let bir = "Burundian radios";


    let rw = {index:1, name: rwr, field: "countryCode", query: "RW"};
    let us= {index:1, name: usr, field: "countryCode", query: "US"};
    let ng= {index:2, name: ngr, field: "countryCode", query: "NG"};
    let tz=  {index:3, name: tzr, field: "countryCode", query: "TZ"};
    let ug = {index:4, name: ugr, field: "countryCode", query: "UG"};
    let ke = {index:5, name: ker, field: "countryCode", query: "KE"};
    let cd =  {index:6, name: cdr, field: "countryCode", query: "CD"};
    let bi= {index:7, name: bir, field: "countryCode", query: "BI"};

    // Available films  = US,RW,BI,KE,UG,TZ,NG,CD,
    switch (code){

        // East Africa
        case "RW":
            radio = [rw,us,ng,tz,ug,ke,cd,bi];
            break;
        case "BI":
            radio = [bi,rw,us,ng,tz,ug,ke,cd];
            break;
        case "UG":
            radio = [ug,rw,us,ng,tz,ke,cd,bi];
            break;
        case "KE":
            radio = [ke,rw,us,ng,tz,ug,cd,bi];
            break;
        case "TZ":
            radio = [tz,us,ng,rw,ug,ke,cd,bi];
            break;


        // Other countries
        case "CD":
            radio = [cd,rw,us,ng,tz,ug,ke,bi];
            break;

        case "NG":
            radio = [ng,rw,us,tz,ug,ke,cd,bi];
            break;

        default:
            radio = [rw,ng,tz,cd,ug,ke,bi];
            break;

    }

    return radio;

}



var exports = module.exports = {
    parseFilm: parseFilm,
    parseTv: parseTv,
    parseRadio: parseRadio
};

