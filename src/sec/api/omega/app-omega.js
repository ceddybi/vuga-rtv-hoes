const express = require('express');

const router = express.Router();
const CB = require('../static/cb');
let mongoose = require('mongoose');
let PostModel = mongoose.model('Post');
let FilmModel = mongoose.model('Film');
let TvModel = mongoose.model('Tv');
let Ad = mongoose.model('Ad');
let UserProfile = mongoose.model('UserProfile');
let RadioModel = mongoose.model('Radio');
const _ = require('lodash');

const config = require('../static/variables');

const locationUtils = require('../location/location-util');
const locationParse = require('../location/location-parse');

const countries = require('../util/country/countries');
const countryUtil = require('../util/country/country-utils');

/**
 *
 * Get country code, typo and default country
 *
 * */
function getIndex(code, typo, def) {
  // let current = (def || "");
  let bonus = 0;

  // Check if it's not null
  if (def !== undefined && !_.isEmpty(def)) {
    // Check if it's not from the same region
    if (_.eq(def, code)) {
      bonus = 20;
    }
  }
  // Loop thru three typos
  switch (typo) {
    case 'tv':
      return countryUtil.getIndex(code) + bonus;

    case 'radio':
      return countryUtil.getIndex(code) + bonus;

    case 'film':
      return countryUtil.getIndex(code) + bonus;
    default:
  }
}

function getCatelogName(country, typo) {
  let ex = '';
  let nat = '';

  // Extention
  switch (typo) {
    case 'tv':
      ex = 'TVs';
      break;
    case 'film':
      ex = 'Movies';
      break;
    case 'radio':
      ex = 'Radios';
      break;
    default:
  }

  // Cast out American
  if (country.alpha_2_code === 'US') {
    nat = 'Latest';
  } else {
    // else defualt nationality
    nat = country.nationality;
  }

  return `${nat} ${ex}`;
}

function mapWorld(mycode, res) {
  // One map function
  const cats = [];

  const myloc = mycode.toLowerCase() || '';

  countries.countries.map((country, index) => {
    // Radio, TV, film
    const lower = country.alpha_2_code.toLowerCase();
    const sptd = countryUtil.supported[lower];

    // Check if supported country
    if (sptd !== undefined) {
      const query_film = 'meta.cat';
      const query_tv = 'countryCode';
      const query_radio = 'countryCode';

      const mv = {
        id: `${lower}mv`,
        name: getCatelogName(country, 'film'), // country.nationality + " Movies",
        typo: 'film',
        countryCode: lower,
        index: getIndex(lower, 'film', myloc),
        query: { field: query_film, value: country.alpha_2_code },
      };

      // Push movie

      // Check if not from same location
      // TODO BAN SAME REGION MOVIES
      if (!_.eq(myloc, lower)) {
        cats.push(mv);
      }

      const tv = {
        id: `${lower}tv`,
        name: getCatelogName(country, 'tv'), // country.nationality + " TVs",
        typo: 'tv',
        countryCode: lower,
        index: getIndex(lower, 'tv', myloc),
        query: { field: query_tv, value: country.alpha_2_code },
      };

      // Push movie
      cats.push(tv);

      const radio = {
        id: `${lower}rd`,
        name: getCatelogName(country, 'radio'), // country.nationality + " Radios",
        typo: 'radio',
        countryCode: lower,
        index: getIndex(lower, 'radio', myloc),
        query: { field: query_radio, value: country.alpha_2_code },
      };

      // Push movie
      cats.push(radio);

      //
    }
  });

  // sort the catelog
  cats.sort((a, b) => {
    // return the difference of `msg_count` property to sort based on them
    // if they are equal then sort based on unix_time prop
    return b.index - a.index || b.index - a.index;
  });

  // check if location null
  if (_.isEmpty(myloc)) {
    const bannedAnony = { us: '', gb: '' };
    cats.map((cat, index) => {
      // Check if is valid ban
      if (bannedAnony[cat.countryCode] !== undefined) {
        cats.splice(index, 1);
      }
    });
  }

  // console.log(roughSizeOfObject(cats));
  res.status(200).json(cats);
  // res.status(200).json([]);
}

function roughSizeOfObject(object) {
  const objectList = [];
  const stack = [object];
  let bytes = 0;

  while (stack.length) {
    const value = stack.pop();

    if (typeof value === 'boolean') {
      bytes += 4;
    } else if (typeof value === 'string') {
      bytes += value.length * 2;
    } else if (typeof value === 'number') {
      bytes += 8;
    } else if (typeof value === 'object' && objectList.indexOf(value) === -1) {
      objectList.push(value);

      for (const i in value) {
        stack.push(value[i]);
      }
    }
  }
  return bytes;
}

function mapCat(films, tvs, radios, res) {
  // console.log(films);
  // console.log(tvs);
  // console.log(radios);
  const cats = [];

  films.map(fil => {
    const lower = fil.query.toLowerCase();
    const x = {
      id: `${lower  }mv`,
      name: fil.name,
      typo: 'film',
      countryCode: lower,
      index: fil.index,
      query: { field: fil.field, value: fil.query },
    };
    cats.push(x);
  });

  tvs.map(tv => {
    const lower = tv.query.toLowerCase();
    const x = {
      id: `${lower  }tv`,
      name: tv.name,
      typo: 'tv',
      countryCode: lower,
      index: tv.index,
      query: { field: tv.field, value: tv.query },
    };
    cats.push(x);
  });

  radios.map(radio => {
    const lower = radio.query.toLowerCase();
    const x = {
      id: `${lower  }rd`,
      name: radio.name,
      typo: 'radio',
      countryCode: lower,
      index: radio.index,
      query: { field: radio.field, value: radio.query },
    };
    cats.push(x);
  });

  res.status(200).json(cats);
}

function sendNow(code, res) {
  console.log(`code = ${code}`);
  // New world
  mapWorld(code, res);
}

// Get ads
/**
 * @param userid,location
 */

// return catelog, tv,film,radio
router.post('/get/catelog', async (req, res) => {
  let userCode;
  let user;
  console.log('App get catelog-------------------------------------------START');
  console.log(req.body);
  console.log('App get catelog-------------------------------------------END');
  // const userid = req.body.userid;
  let { location, userid } = req.body;

  const cats = [];

  try {
    if (_.isEmpty(userid)) {
      throw new Error('Userid empty get cat');
    }

    user = await UserProfile.findOne({ 'info.userid': userid }).exec();
    // Check user

    if (_.isEmpty(user)) {
      throw new Error('User not found get cat');
    }

    // check if location is available
    if (_.isEmpty(req.body.location)) {
      throw new Error('Usercode No location but user found');
      // No location but user found
    }

    // check if location is available
    if (_.isEmpty(req.body.location)) {
      throw new Error('Usercode location but userCode empty');
    }

    location = JSON.parse(req.body.location);
    userCode = await locationUtils.syncGetCountryCode(location, user);
    if (!_.isEmpty(userCode)) {
      console.log('User got code');
      // console.log(userCode);
      return sendNow(userCode, res);
    }

    return sendNow('', res);
  } catch (error) {
    console.log(error);
    return sendNow('', res);
  }
});

module.exports = router;
