/**
 * Created by vuga on 9/15/17.
 */



var express = require('express');
var router = express.Router();
var CB =  require('../static/cb');
var mongoose = require('mongoose'),
    PostModel = mongoose.model('Post'),
    FilmModel = mongoose.model('Film'),
TvModel = mongoose.model('Tv'),
Ad = mongoose.model('Ad'),
RadioModel = mongoose.model('Radio');
var _ = require('lodash');

var config = require('../static/variables');

let locationUtils = require('../location/location-util');
let locationParse = require('../location/location-parse');


let countries  = require('../util/country/countries');
let countryUtil  = require('../util/country/country-utils');


function getIndex(code, typo, def){

    //let current = (def || "");
    let bonus = 0;

    //Check if it's not null
    if(def !== undefined && !_.isEmpty(def)){
        //Check if it's not from the same region
        if(_.eq(def,code)) {
            bonus = 20;
        }
    }
    // Loop thru three typos
    switch (typo){

        case "tv":
            return  (countryUtil.getIndex(code) + bonus);
            break;

        case "radio":
            return  (countryUtil.getIndex(code) + bonus);
            break;

        case "film":
            return  (countryUtil.getIndex(code) + bonus);
            break;
    }

}


function getCatelogName(country, typo){
    let ex = "";
    let nat = "";

    //Extention
    switch(typo){
        case "tv":
            ex = "TVs";
            break;
        case "film":
            ex = "Movies";
            break;
        case "radio":
            ex = "Radios";
            break;
    }

    //Cast out American
    if(country.alpha_2_code === "US"){
        nat = "Latest"
    }
    else{
        //else defualt nationality
        nat  = country.nationality;
    }

    return nat+" "+ex;
}


function mapWorld(mycode, res){
    // One map function
    var cats = [];

    let myloc = (mycode.toLowerCase() || "");


    countries.countries.map(function(country, index){
        //Radio, TV, film
        let lower = country.alpha_2_code.toLowerCase();
        let sptd = countryUtil.supported[lower];

        //Check if supported country
        if(sptd!== undefined) {

            let query_film = "meta.cat";
            let query_tv = "countryCode";
            let query_radio = "countryCode";

            var mv = {
                id: lower + "mv",
                name: getCatelogName(country, "film"), //country.nationality + " Movies",
                typo: "film",
                countryCode: lower,
                index: getIndex(lower, "film",myloc),
                query: {field: query_film, value: country.alpha_2_code}
            };

            //Push movie

            // Check if not from same location
            // TODO BAN SAME REGION MOVIES
            if (!_.eq(myloc, lower)) {
                cats.push(mv);
            }


            var tv = {
                id: lower + "tv",
                name: getCatelogName(country, "tv"),  //country.nationality + " TVs",
                typo: "tv",
                countryCode: lower,
                index: getIndex(lower, "tv",myloc),
                query: {field: query_tv, value: country.alpha_2_code}
            };

            //Push movie
            cats.push(tv);


            var radio = {
                id: lower + "rd",
                name: getCatelogName(country, "radio"), //country.nationality + " Radios",
                typo: "radio",
                countryCode: lower,
                index: getIndex(lower, "radio",myloc),
                query: {field: query_radio, value: country.alpha_2_code}
            };

            //Push movie
            cats.push(radio);

            //
        }

    });

    //sort the catelog
    cats.sort(function(a, b) {
        // return the difference of `msg_count` property to sort based on them
        // if they are equal then sort based on unix_time prop
        return b.index - a.index ||
            b.index - a.index;
    });

    //check if location null
    if(_.isEmpty(myloc)){
        let bannedAnony = {us:"", gb:""};
        cats.map(function(cat,index){
            //Check if is valid ban
            if(bannedAnony[cat.countryCode]!==undefined) {
                cats.splice(index, 1);
            }
        });
    }

    //console.log(roughSizeOfObject(cats));
    res.status(200).json(cats);
    //res.status(200).json([]);
}


module.exports  = {
    mapWorld: mapWorld,
};