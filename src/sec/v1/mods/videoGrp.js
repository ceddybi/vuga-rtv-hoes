var mongoose = require( 'mongoose' );

var VideoGroupSchema = new mongoose.Schema({
    gid: {type: String , default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    typo: {type: String, default: ""}, 
    meta: {type: String, default: ""},
    featured: {type: Number, default: 1},
    int_date: {type: Number, default: 0},
    updated_at: {type: Number, default: 0},
    available: {type: Boolean, default: true},

    lock: {type: String, default: ""},
    lockCode: {type: String, default: ""},
    youtubeChannel: {type: String, default: ""},
    image_path: {type: String, default: ""},
    created_at: {type: Date, default: Date.now},
 
    

});

var videoGrp = mongoose.model('VideoGrp', VideoGroupSchema);
videoGrp.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = videoGrp;