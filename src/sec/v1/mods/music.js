var mongoose = require( 'mongoose' );

var MusicSchema = new mongoose.Schema({
    mid: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    typo: {type: String, default: ""}, 
    meta: {type: String, default: ""},
    featured: {type: Number, default: 1},
    int_date: {type: Number, default: 0},
    updated_at: {type: Number, default: 0},
    artistId: {type: String, default: ""},
    artistName: {type: String, default: ""},

    lock: {type: String, default: ""},
    lockCode: {type: String, default: ""},
    youtubeUrl: {type: String, default: ""},
    
    image_path: {type: String, default: ""},
    audio_path: {type: String, default: ""},
    created_at: {type: Date, default: Date.now},
    available: {type: Boolean, default: true},

    

});

var music = mongoose.model('Music', MusicSchema);
music.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = music;