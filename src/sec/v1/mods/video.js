var mongoose = require( 'mongoose' );

var VideoSchema = new mongoose.Schema({
    vid: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    typo: {type: String, default: ""}, 
    meta: {type: String, default: ""},
    featured: {type: Number, default: 1},
    int_date: {type: Number, default: 0},
    updated_at: {type: Number, default: 0},
    groupId: {type: String, default: ""},
    groupName: {type: String, default: ""},
    lock: {type: String, default: ""},
    lockCode: {type: String, default: ""},
    isYoutube: {type: Boolean, default: true},
    youtubeUrl: {type: String, default: ""},
    available: {type: Boolean, default: true},
    image_path: {type: String, default: ""},
    video_path: {type: String, default: ""},
    created_at: {type: Date, default: Date.now}
    

});

var video = mongoose.model('Video', VideoSchema);
video.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = video;