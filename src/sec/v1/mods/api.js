var mongoose = require( 'mongoose' );

var ArtistSchema = new mongoose.Schema({
    aid: {type: String},
    title: {type: String},
    description: {type: String},
    meta: {type: String},
    featured: {type: Number},
    int_date: {type: Number},
    updated_at: {type: Number},
    

    lock: {type: Boolean},
    lockCode: {type: String},
    youtubeChannel: {type: String},
    image_path: {type: String},
 
    

});

var api = mongoose.model('Blog', ArtistSchema);
api.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = api;