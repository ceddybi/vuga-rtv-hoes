var mongoose = require( 'mongoose' );

var DaytoSchema = new mongoose.Schema({
    dtid: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    typo: {type: String, default: ""},
    meta: {type: String, default: ""},
    isToday: {type: Boolean, default: true},

    created_at: {type: Date, default: Date.now},
    unix_time: {type: String, default: ""+ new Date().getTime()},

    all_count_int: {type: Number, default: 1},
    all_count_str: {type: String, default: ""},

    user_new_count_int: {type: Number, default: 1},
    user_new_count_str: {type: String, default: ""},

    user_active_count_int: {type: Number, default: 1},
    user_active_count_str: {type: String, default: ""},

    video_count_int: {type: Number, default: 1},
    video_count_str: {type: String, default: ""},

    videogrp_count_int: {type: Number, default: 1},
    videogrp_count_str: {type: String, default: ""},

    tv_count_int: {type: Number, default: 1},
    tv_count_str: {type: String, default: ""},

    radio_count_int: {type: Number, default: 1},
    radio_count_str: {type: String, default: ""},

    music_count_int: {type: Number, default: 1},
    music_count_str: {type: String, default: ""},

    artist_count_int: {type: Number, default: 1},
    artist_count_str: {type: String, default: ""},

    web_count_int: {type: Number, default: 1},
    web_count_str: {type: String, default: ""},

    other_count_int: {type: Number, default: 1},
    other_count_str: {type: String, default: ""},
    

    

});

var ads = mongoose.model('Dayto', DaytoSchema);
ads.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = ads;