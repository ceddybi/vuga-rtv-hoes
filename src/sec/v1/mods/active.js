var mongoose = require( 'mongoose' );
var randtoken = require('rand-token');

function rand() {
    return randtoken.generate(20); 
  };

var activeSchema = new mongoose.Schema({

    activeid: {type: String, default: rand()},
    created_at: {type: Date, default: Date.now},
    unix_time: {type: String, default: ""+ new Date().getTime()},
    timestamp: {type: Date, default: new Date},
    typo: {type: String, default: ""},
    grp: {type: String, default: ""},
    grpid: {type: String, default: ""},
    unitid: {type: String, default: ""},
    time: {type: Number, default: 1},

    

});

var active = mongoose.model('Active', activeSchema);
active.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

