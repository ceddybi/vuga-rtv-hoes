var mongoose = require( 'mongoose' );

var pushSchema = new mongoose.Schema({
    title: {type: String, default: ""},
    message: {type: String, default: ""},
    sound: {type: String, default: ""},
    click_action: {type: String, default: ""},
    data: {},
    country:[],
    all: {type: Boolean, default: true},
    created_at: {type: Date, default: Date.now},

    

});

var push = mongoose.model('Push', pushSchema);
push.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = push;