var mongoose = require( 'mongoose' );
var randtoken = require('rand-token');

function rand() {
    return randtoken.generate(20); 
  };

var smsSchema = new mongoose.Schema({

    smsid: {type: String, default: rand()},
    created_at: {type: Date, default: Date.now},
    unix_time: {type: String, default: ""+ new Date().getTime()},
    timestamp: {type: Date, default: new Date},
    code: {type: String, default: ""},
    phone: {type: String, default: ""},
    count: {type: Number, default: 1},

    

});

var sms = mongoose.model('Sms', smsSchema);
sms.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

