var mongoose = require( 'mongoose' );

var AdsSchema = new mongoose.Schema({
    adsid: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    typo: {type: String, default: ""},
    meta: {type: String, default: ""},
    featured: {type: Number, default: 1},
    timeout: {type: Number, default: 0},
    int_date: {type: Number, default: 0},
    updated_at: {type: Number, default: 0},
    link: {type: String, default: ""},
    isVideo: {type: Boolean, default: true},

    isYoutube: {type: Boolean, default: true},
    youtubeUrl: {type: String, default: ""},
    image_path: {type: String, default: ""},
    video_path: {type: String, default: ""},
    created_at: {type: Date, default: Date.now},
    available: {type: Boolean, default: true},

    

});

var ads = mongoose.model('Ads', AdsSchema);
ads.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = ads;