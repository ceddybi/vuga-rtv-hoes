var mongoose = require( 'mongoose' );

var AdminSchema = new mongoose.Schema({
    aid: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    meta: {type: String, default: ""},
    featured: {type: Number, default: 1},
    active_int: {type: Number, default: 1},
    active_str: {type: String, default: ""},
    

    lock: {type: Boolean},
    lockCode: {type: String},
    youtubeChannel: {type: String},
    image_path: {type: String},
 
    

});

var api = mongoose.model('Admin', AdminSchema);
api.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = api;