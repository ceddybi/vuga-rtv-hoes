var mongoose = require( 'mongoose' );

var TvSchema = new mongoose.Schema({
    tvid: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    typo: {type: String, default: ""},
    meta: {type: String, default: ""},
    link: {type: String, default: ""},
    featured: {type: Number, default: 1},
    int_date: {type: Number, default: 0},
    updated_at: {type: Number, default: 0},
    lock: {type: String, default: ""},
    lockCode: {type: String, default: ""},
    youtubeChannel: {type: String, default: ""},
    image_path: {type: String, default: ""},
    created_at: {type: Date, default: Date.now},
    available: {type: Boolean, default: true},
    

});

var tv = mongoose.model('Tv', TvSchema);
tv.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = tv;