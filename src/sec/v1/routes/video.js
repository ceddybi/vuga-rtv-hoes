
var express = require('express');
var router = express.Router();
var upl = require('./upload.js');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + '/../public/uploads/'
});
var randtoken = require('rand-token');


function rand() {
    return randtoken.generate(10); 
  };

var fs = require('fs');
var md5 = require('md5');

var mongoose = require('mongoose'),
    Video = mongoose.model('Video'),
    VideoGrp = mongoose.model('VideoGrp');

    var d = new Date();
    var currentTime = d.getTime();
    var image_path = "";
    var video_path = "";

function getGroupName(aId, callback){
   VideoGrp.findOne({'gid': aId}).exec(function (error, vG) {        
        var art = vG.title;
         callback(art);

         });
} 

/* GET All videos listing. */
router.get('/', function(req, res, next) {
    Video.find({}).sort({created_at: -1}).exec(function (error, result) {
        if (error){ throw error;}else
        {
        res.render('video/index', {items: JSON.stringify(result)});
         
           }
    });
});

router.get('/create', function(req, res, next) {

        VideoGrp.find({}).sort({created_at: -1}).exec(function (error, vG) {        
        res.render('video/form', {videoGrp: JSON.stringify(vG)});
         
         });
    //res.render('video/form', { title: 'Videos' });
});

router.post('/create', upload.any(), function (req, res, next) {


    //console.log("is Youtube = "+req.body.isYoutube);
    getGroupName(req.body.groupId, function (nam){

    var video = new Video({


    vid: rand(),
    title: req.body.title,
    description: req.body.description,
    typo: "video",
    meta: req.body.meta,
    int_date: currentTime,
    updated_at: 0,
    groupId: req.body.groupId,
    groupName: nam,
    lock: req.body.lock,
    lockCode: req.body.lockCode,
    isYoutube: req.body.isYoutube,
    youtubeUrl: req.body.youtubeUrl,
    available: req.body.available,
    
    created_at: d,

    });
    
    req.files.forEach(function (value, index) {
        if (value.fieldname == 'image_file') {
            var splittedName = value.originalname.split('.');
            var image_name = req.body.title + '_image_' + currentTime + '.' + splittedName[splittedName.length - 1];
            image_path = __dirname + '/../public/data/images/' + image_name;
            fs.rename(value.path, image_path);
            video.image_path = "https://s3.amazonaws.com/rtv01/images/"+image_name.replace(/ /g,"%20");
            upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');


        } else if (value.fieldname == 'video_file') {
            var splittedName = value.originalname.split('.');
            var video_name = req.body.title + '_video_'+currentTime + '.' + splittedName[splittedName.length - 1];
            video_path = __dirname + '/../public/data/videos/' + video_name;
            //data.video_path = '/data/videos/' + video_name;
             video.video_path = "https://s3.amazonaws.com/rtv01/videos/" + video_name.replace(/ /g,"%20");
             fs.rename(value.path, video_path);           
            upl.saveFile(video_path, video_name, splittedName[splittedName.length - 1], 'video');

      



        }

    });






    video.save(function (error, result) {
        if (error) {
            console.error(error);
            throw error;
        }
    });

    res.redirect('/video');
     });
});

router.get('/:id/delete',function(req, res, next){
    Video.findOneAndRemove({_id:req.params.id}, function (error, item) {
        if (error){ throw error;}
        else{
            //console.log(item);
        res.redirect('/video');}
    })
});

router.get('/:id/edit',function(req, res, next){
VideoGrp.find({}).sort({created_at: -1}).exec(function (error, vG) {        
                       
    Video.findById(req.params.id, function (error, item) {
        if (error) throw error;
        res.render('video/edit', {item: JSON.stringify(item), videoGrp: JSON.stringify(vG) });
    });

});
});

router.post('/:id/edit', upload.any(),  function (req, res, next) {
    var d = new Date();
    var currentTime = d.getTime();

    getGroupName(req.body.groupId, function (nam){

    var data = {
    title: req.body.title,
    description: req.body.description,
    meta: req.body.meta,
    featured: req.body.featured,
    updated_at: currentTime,
    groupId: req.body.groupId,
    groupName: nam,
    lock: req.body.lock,
    lockCode: req.body.lockCode,
    isYoutube: req.body.isYoutube,
    youtubeUrl: req.body.youtubeUrl,
    available: req.body.available,

    };

    req.files.forEach(function (value, index) {
        if (value.fieldname == 'image_file') {
            var splittedName = value.originalname.split('.');
            var image_name = req.body.title + '_image_' + currentTime + '.' + splittedName[splittedName.length - 1];
            image_path = __dirname + '/../public/data/images/' + image_name;
            fs.rename(value.path, image_path);
            data.image_path = "https://s3.amazonaws.com/rtv01/images/"+image_name.replace(/ /g,"%20");
            upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');


        } else if (value.fieldname == 'video_file') {
            var splittedName = value.originalname.split('.');
            var video_name = req.body.title + '_video_'+currentTime + '.' + splittedName[splittedName.length - 1];
            video_path = __dirname + '/../public/data/videos/' + video_name;
            //data.video_path = '/data/videos/' + video_name;
             data.video_path = "https://s3.amazonaws.com/rtv01/videos/" + video_name.replace(/ /g,"%20");
            fs.rename(value.path, video_path);           
            upl.saveFile(video_path, video_name, splittedName[splittedName.length - 1], 'video');

      



        }

    });


    Video.findByIdAndUpdate(req.params.id, data, function(error,result){
        if (error) throw error;
        res.redirect('/video');
    });

});
});

module.exports = router;
