var express = require('express');
var router = express.Router();

var mailgun = require('mailgun-js')({apiKey: "key-221954a88b1022ec333cc775908f360a", domain: "mail.vugapay.com"});
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var wellcome_templateDir = path.join(__dirname, 'email_templates', 'wellcome');
var wellcome_email = new EmailTemplate(wellcome_templateDir);

var version = 19;
var expire =  "1471039588584";
var bugfix = "New features";
var expe = true;
var _ = require('lodash');

var mongoose = require('mongoose'),
    Video = mongoose.model('Video'),
    VideoGrp = mongoose.model('VideoGrp'),
    Artist = mongoose.model('Artist'),
    Music = mongoose.model('Music'),
    Advert = mongoose.model('Ads'),
    TV = mongoose.model('Tv'),
    Radios = mongoose.model('Radio'),
    User = mongoose.model('User');

var randtoken = require('rand-token');


function rand() {
    return randtoken.generate(13); 
  };

function getArtistName(aId){
   Artist.findOne({aid: aId}), (function (error, vG) {        
        
         return vG.title;

         });
} 

function newUser(req,res){
    var vg = new User({
    userid: rand(),
    loc: req.body.loc,
    wlan: req.body.wlan,
    device: req.body.device,
    androidId: req.body.androidId,
    fcmToken: req.body.fcmToken,
    unique: req.body.unique,
    appversion: req.body.appversion,
    gmt: req.body.gmt,
    });
    
    vg.save(function (error, result) {
        if (error) {
            console.error(error);
            throw error;
        }
        res.status(200).send(result);
    });
} 

function updateUser(req, _id){
            // User found so Just update
        var d = {
        loc: req.body.loc,
        wlan: req.body.wlan,
        device: req.body.device,
        androidId: req.body.androidId,
        fcmToken: req.body.fcmToken,
        unique: req.body.unique,
        appversion: req.body.appversion,
        phone: ""+req.body.phone,
        updated_at: new Date().getTime(),
        gmt: req.body.gmt,
        };

           User.findByIdAndUpdate(_id, {$set:{d}}, function(error,resu){
                if (error) throw error;
              //  res.status(200).send(resu);
            });   
}

function isEmpty(str) {
    return (!str || 0 === str.length);
} 

function getEmail(user){
wellcome_email.render({
        name: user.phone,
        msg: user.msg
      }, function(err, result){
        if(err){console.log(err)}
          else{
        console.log("sucess "+result);
        sendMsg(result.html);

        };
      })
    };




 function sendMsg(html){
      mailgun.messages().send({
        from: 'VugaTV <noreply@vugapay.com>',
        to: 'VugaPay Official <radrwinc@gmail.com>',
        subject:'VugaTV',
        html: html
      },   function(error, body){
    
  if(error) console.log(error); 
  
  else console.log(body);
    
        });
    };  

// Videos
router.post('/video/id',function(req, res){
        Video.findOne({vid: req.body.vid}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/video/all',function(req, res){
        Video.find({ 'available': { $ne: false } }).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/video/group',function(req, res){
        Video.find({groupId: req.body.groupId, 'available': { $ne: false }}).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});


// Video groups
router.post('/videogroup/all',function(req, res){
    VideoGrp.find({ 'available': { $ne: false } }).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/videogroup/id',function(req, res){
    VideoGrp.findOne({gid: req.body.gid}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});


// Music
router.post('/music/id',function(req, res){
        Music.findOne({mid: req.body.mid}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/music/all',function(req, res){
        Music.find({ 'available': { $ne: false } }).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/music/artist',function(req, res){
        Music.find({artistId: req.body.artistId, 'available': { $ne: false } }).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});


// Music Artists
router.post('/artist/all',function(req, res){
    Artist.find({ 'available': { $ne: false } }).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/artist/id',function(req, res){
    Artist.findOne({gid: req.body.gid}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});


// Ad's
router.post('/ads/all',function(req, res){
    Advert.find({}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/ads/id',function(req, res){
    Advert.findOne({adsid: req.body.adsid}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});



// TV's
router.post('/tv/all',function(req, res){
    TV.find({ 'available': { $ne: false } }).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/tv/id',function(req, res){
    TV.findOne({tvid: req.body.tvid}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});


// Radios
router.post('/radios/all',function(req, res){
    Radios.find({ 'available': { $ne: false } }).sort({featured: -1}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});

router.post('/radios/id',function(req, res){
    Radios.findOne({gid: req.body.gid}).exec(function (error, vG) {        
        res.status(200).send(vG);      
         });
});


router.post('/gcm', function (req, res) {

  console.log("GCM from "+ req.body.loc + " user FCM = " + req.body.fcmToken);
    //Looper over here
    User.findOne({unique: req.body.unique}).exec(function (error, result) {
        if (result)
        {

        // User found so Just update
        var d = {
        loc: req.body.loc,
        wlan: req.body.wlan,
        device: req.body.device,
        androidId: req.body.androidId,
        fcmToken: req.body.fcmToken,
        unique: req.body.unique,
        appversion: req.body.appversion,
        gmt: req.body.gmt,
        updated_at: new Date().getTime(),
        };

           User.findByIdAndUpdate(result._id, d, function(error,resu){
                if (error) throw error;
                res.status(200).send(resu);
            });            

            
        }
        else
        {
            // Check whether imei is invalid
        if(req.body.device != undefined && !isEmpty(req.body.device)){
         // Unique not found so loop through device
        User.findOne({device: req.body.device}).exec(function (error, resul) {
        if (resul){
            // User found by device so just update
        var da = {
        loc: req.body.loc,
        wlan: req.body.wlan,
        device: req.body.device,
        androidId: req.body.androidId,
        fcmToken: req.body.fcmToken,
        unique: req.body.unique,
        appversion: req.body.appversion,
        gmt: req.body.gmt,
        updated_at: new Date().getTime(),
        };

           User.findByIdAndUpdate(resul._id, da, function(error,resu){
                if (error) throw error;
                res.status(200).send(resu);
            }); 

        }else{

            // Ooops user must be new to the platform
            // Register the new user
            newUser(req, res);
        }

          });

         }else{

            // Ooops new user but Imei is empty
            // Register this user
             newUser(req, res);

         }

           }

    });


     
     //send results
    //res.redirect('/advert');
});


router.post('/user/msg',function(req, res){
    // Radios.findOne({gid: req.body.gid}).exec(function (error, vG) {        
    // res.status(200).send(vG);      
    // });
    console.log("Msg from "+ req.body.loc + " user Phone = " + req.body.phone);
    var cht = {
            msg: req.body.msg,
            phone: ""+req.body.phone,
        };
    getEmail(cht);
        //Looper over here
    User.findOne({unique: req.body.unique}).exec(function (error, result) {
        if (result)
        {
            
         // User found so Just update
        var chat = {
            title: "Hello",
            typo: "user",
            msg: req.body.msg,
            phone: ""+req.body.phone,
        };

           User.findByIdAndUpdate(result._id,{$push:{"chat":chat}}, function(error,resu){
                if (error) throw error;
                updateUser(req, result._id);
                res.status(200).send(resu);
            }); 
            
        }
        else
        {
            // Check whether imei is invalid
        if(req.body.device != undefined && !isEmpty(req.body.device)){
         // Unique not found so loop through device
        User.findOne({device: req.body.device}).exec(function (error, resul) {
        if (resul){

        var chat = {
            title: "Hello",
            typo: "user",
            msg: req.body.msg,
            phone: ""+req.body.phone,
        };

           User.findByIdAndUpdate(result._id,{$push:{"chat":chat}}, function(error,resu){
                if (error) throw error;
                updateUser(req, resul._id);
                res.status(200).send(resu);
            }); 


        }else{

            // Ooops user must be new to the platform
            // Register the new user
            newUser(req, res);
        }

          });

         }else{

            // Ooops new user but Imei is empty
            // Register this user
             newUser(req, res);

         }

           }

    });
});

router.post('/app/version',function(req, res){
    //Radios.findOne({gid: req.body.gid}).exec(function (error, vG) { \\

    if(parseInt(req.body.vcode) < version){       
        res.status(200).send({"version":version, "expire":expire, "bugfix":bugfix, "expe":false}); 
        console.log("Dead version = "+ req.body.vcode);
         }
        else{
         res.status(200).send({"version":version, "expire":expire, "bugfix":bugfix, "expe":expe});  
        }    
   //      });
});

function getTypo(argument, id, callback) {

  switch (argument) {
    case 'video':
     Video.findOne({_id: id}, function(error,resu){
          if (error) throw error;
          callback(resu);
      });
    break;

    case 'videoGrp':
     VideoGrp.findOne({_id: id}, function(error,resu){
          if (error) throw error;
          callback(resu);
      });
    break;

    case 'music':
     Music.findOne({_id: id}, function(error,resu){
          if (error) throw error;
          callback(resu);
      });
    break;

    case 'artist':
     Artist.findOne({_id: id}, function(error,resu){
          if (error) throw error;
          callback(resu);
      });
    break;

    case 'tv':
     TV.findOne({_id: id}, function(error,resu){
          if (error) throw error;
          callback(resu);
      });
    break;

    case 'radios':
    Radios.findOne({_id: id}, function(error,resu){
          if (error) throw error;
          callback(resu);
      });
    break;       
  }
}

function getDummy(argument, callback) {

  switch (argument) {
    case 'video':
     Video.find({}, function(error,resu){
          if (error) throw error;
         return callback(resu);
      });
    break;

    case 'videoGrp':
     VideoGrp.find({}, function(error,resu){
          if (error) throw error;
        return callback(resu);
      });
    break;

    case 'music':
     Music.find({}, function(error,resu){
          if (error) throw error;
         return callback(resu);
      });
    break;

    case 'artist':
     Artist.find({}, function(error,resu){
          if (error) throw error;
         return callback(resu);
      });
    break;

    case 'tv':
     TV.find({}, function(error,resu){
          if (error) throw error;
         return callback(resu);
      });
    break;

    case 'radios':
    Radios.find({}, function(error,resu){
          if (error) throw error;
         return callback(resu);
      });
    break;       
  }
}

router.post('/push/dummy',function(req, res){
   getDummy(req.body.typo, function(data){
        if(data){
          var c = _.map(data, _.partialRight(_.pick, 'title','_id'));
          c.typo = req.body.typo;
          console.log(req.body.typo+" ="+JSON.stringify(c));
        res.status(200).send(JSON.stringify(c));

        }else{}


});     
});

router.post('/push/typo',function(req, res){

var c = _.map(dummy, _.partialRight(_.pick, 'device','_id'));
        res.status(200).send(JSON.stringify(c));
        
});




module.exports = router;
