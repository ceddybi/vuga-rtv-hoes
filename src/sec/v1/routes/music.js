
var express = require('express');
var router = express.Router();
var upl = require('./upload.js');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + '/../public/uploads/'
});
var randtoken = require('rand-token');


function rand() {
    return randtoken.generate(10); 
  };



var fs = require('fs');
var md5 = require('md5');

var mongoose = require('mongoose'),
    Music = mongoose.model('Music'),
    Artist = mongoose.model('Artist');

    var d = new Date();
    var currentTime = d.getTime();
    var image_path = "";
    var audio_path = "";


function getArtistName(aId, callback){
   Artist.findOne({'aid': aId}).exec(function (error, vG) {        
        //var art = vG.title;
         callback(vG);

         });
}  
/* GET All videos listing. */
router.get('/', function(req, res, next) {
    Music.find({}).sort({created_at: -1}).exec(function (error, result) {
        if (error){ throw error;}else
        {
        res.render('music/index', {items: JSON.stringify(result)});
         
           }
    });
});

router.get('/create', function(req, res, next) {

        Artist.find({}).sort({created_at: -1}).exec(function (error, vG) {        
        res.render('music/form', {artist: JSON.stringify(vG)});
         
         });
    //res.render('music/form', { title: 'Musics' });
});

router.post('/create', upload.any(), function (req, res, next) {

   var art = '';
   getArtistName(req.body.artistId, function (nam){
        
    
    //console.log("is Youtube = "+req.body.isYoutube);
    var mus = new Music({


    mid: rand(),
    title: req.body.title,
    description: req.body.description,
    typo: "music",
    meta: req.body.meta,
    int_date: currentTime,
    updated_at: 0,
    artistId: req.body.artistId,
    artistName: nam.title,
    image_path: nam.image_path,

    lock: req.body.lock,
    lockCode: req.body.lockCode,
    youtubeUrl: req.body.youtubeUrl,
    available: req.body.available,
    
    created_at: d,

    });
    
    req.files.forEach(function (value, index) {
        if (value.fieldname == 'image_file') {
            var splittedName = value.originalname.split('.');
            var image_name = req.body.title + '_image_' + currentTime + '.' + splittedName[splittedName.length - 1];
            image_path = __dirname + '/../public/data/images/' + image_name;
            fs.rename(value.path, image_path);
            mus.image_path = "https://s3.amazonaws.com/rtv01/images/"+image_name.replace(/ /g,"%20");
            upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');


        } else if (value.fieldname == 'audio_file') {
            var splittedName = value.originalname.split('.');
            var audio_name = req.body.title + '_audio_'+currentTime + '.' + splittedName[splittedName.length - 1];
            audio_path = __dirname + '/../public/data/audios/' + audio_name;
            //data.audio_path = '/data/videos/' + audio_name;
             mus.audio_path = "https://s3.amazonaws.com/rtv01/audios/" + audio_name.replace(/ /g,"%20");
             fs.rename(value.path, audio_path);           
             upl.saveFile(audio_path, audio_name, splittedName[splittedName.length - 1], 'audio');

      



        }

    });




    mus.save(function (error, result) {
        if (error) {
            console.error(error);
            throw error;
        }
    });

    res.redirect('/music');
    });
});

router.get('/:id/delete',function(req, res, next){
    Music.findOneAndRemove({_id:req.params.id}, function (error, item) {
        if (error){ throw error;}
        else{
            //console.log(item);
        res.redirect('/music');}
    })
});

router.get('/:id/edit',function(req, res, next){
Artist.find({}).sort({created_at: -1}).exec(function (error, vG) {        
                       
    Music.findById(req.params.id, function (error, item) {
        if (error) throw error;
        res.render('music/edit', {item: JSON.stringify(item), artist: JSON.stringify(vG) });
    });

});
});

router.post('/:id/edit', upload.any(),  function (req, res, next) {
    var d = new Date();
    var currentTime = d.getTime();

    getArtistName(req.body.artistId, function (nam){
    var data = {
    title: req.body.title,
    description: req.body.description,
    meta: req.body.meta,
    featured: req.body.featured,
    updated_at: currentTime,
    artistId: req.body.artistId,
    artistName: nam.title,
    image_path: nam.image_path,

    lock: req.body.lock,
    lockCode: req.body.lockCode,
    youtubeUrl: req.body.youtubeUrl,
    available: req.body.available,

    };

    req.files.forEach(function (value, index) {
        if (value.fieldname == 'image_file') {
            var splittedName = value.originalname.split('.');
            var image_name = req.body.title + '_image_' + currentTime + '.' + splittedName[splittedName.length - 1];
            image_path = __dirname + '/../public/data/images/' + image_name;
            fs.rename(value.path, image_path);
            data.image_path = "https://s3.amazonaws.com/rtv01/images/"+image_name.replace(/ /g,"%20");
            upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');


        } else if (value.fieldname == 'audio_file') {
            var splittedName = value.originalname.split('.');
            var audio_name = req.body.title + '_audio_'+currentTime + '.' + splittedName[splittedName.length - 1];
            audio_path = __dirname + '/../public/data/audios/' + audio_name;
            //data.audio_path = '/data/videos/' + audio_name;
             data.audio_path = "https://s3.amazonaws.com/rtv01/audios/" + audio_name.replace(/ /g,"%20");
            fs.rename(value.path, audio_path);           
            upl.saveFile(audio_path, audio_name, splittedName[splittedName.length - 1], 'audio');

      



        }

    });


    Music.findByIdAndUpdate(req.params.id, data, function(error,result){
        if (error) throw error;
        res.redirect('/music');
    });
 });
});

router.post('/api',function(req, res, next){
   

   console.log("Yes nice");
    
});


module.exports = router;
