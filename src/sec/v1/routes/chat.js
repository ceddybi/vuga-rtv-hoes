var express = require('express');
var _ = require('lodash');
var router = express.Router();
var CB = require('../../api/static/cb');

var mailgun = require('mailgun-js')({apiKey: "key-221954a88b1022ec333cc775908f360a", domain: "mail.vugapay.com"});
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var wellcome_templateDir = path.join(__dirname, 'email_templates', 'wellcome');
var wellcome_email = new EmailTemplate(wellcome_templateDir);

var mongoose = require('mongoose'),
   Chat = mongoose.model('Chat'),
   User = mongoose.model('User'),
   Admin = mongoose.model('Admin');


function ActivatorX(uid, req){

var tt = ""+ new Date().getTime();
var activity_to = {
    ip : "",
    agent_type : "",
    agent_info : req.headers['user-agent'],
    unix_time: tt,
    ses: req.body.ses,
    sessub: req.body.sessub,
}
 
User.find({userid: uid}, function (err, doc) {
          if (doc){
            // update activity of user
            
            User.update({userid: uid},
                    { $push: { "active": activity_to }, $set: {"unix_time": tt} }, 
                    {}, 
                    function(err, doci) {
                      if(err){
                        console.log({error: "Error updating Activity = " + err});
                      }else{
                        console.log({Success: "Success updating User Activity = "+ activity_to});
                      }
                    }); 
                    
          }
          else{
            // Show error
            console.log({error: "Error updating Activity = User not found, userid invalid"});
          }
   
   
 }); 
}


function getEmail(user){
wellcome_email.render({
        name: user.phone,
        msg: user.msg
      }, function(err, result){
        if(err){console.log(err)}
          else{
        console.log("sucess "+result);
        sendMsg(result.html);

        };
      })
    };




 function sendMsg(html){
      mailgun.messages().send({
        from: 'VugaTV <noreply@vugapay.com>',
        to: 'VugaPay Official <radrwinc@gmail.com>',
        subject:'VugaTV',
        html: html
      },   function(error, body){
    
  if(error) console.log(error); 
  
  else console.log(body);
    
        });
    };  

function newUser(req,res){
    var vg = new User({
    userid: rand(),
    loc: req.body.loc,
    wlan: req.body.wlan,
    device: req.body.device,
    androidId: req.body.androidId,
    fcmToken: req.body.fcmToken,
    unique: req.body.unique,
    appversion: req.body.appversion,
    gmt: req.body.gmt,
    });
    
    vg.save(function (error, result) {
        if (error) {
            console.error(error);
            throw error;
        }
        res.status(200).send(result);
    });
} 

function newUserChat(req,res){
    var vg = new User({
    userid: rand(),
    loc: req.body.loc,
    phone: req.body.phone,
    firstname: req.body.firstname,
    wlan: req.body.wlan,
    device: req.body.device,
    androidId: req.body.androidId,
    fcmToken: req.body.fcmToken,
    unique: req.body.unique,
    appversion: req.body.appversion,
    gmt: req.body.gmt,
    });
    
    vg.save(function (error, result) {
        if (error) {
            console.error(error);
            throw error;
        }
        res.status(200).send(result);
    });
}

function updateUser(req,res,_id){
            // User found so Just update
        var d = {
	    loc: req.body.loc,
	    wlan: req.body.wlan,
	    device: req.body.device,
	    androidId: req.body.androidId,
	    fcmToken: req.body.fcmToken,
	    unique: req.body.unique,
	    appversion: req.body.appversion,
        updated_at: new Date().getTime(),
        gmt: req.body.gmt,
        unix_time: ""+new Date().getTime(),
        };

       User.findByIdAndUpdate(_id, {$set:{d}}, function(error,resu){
            if (error) throw error;
            res.status(200).send("Success");
        });   
}

function updateUserChat(req, _id){
            // User found so Just update
        var d = {
	    loc: req.body.loc,
	    phone: req.body.phone,
	    firstname: req.body.firstname,
	    wlan: req.body.wlan,
	    device: req.body.device,
	    androidId: req.body.androidId,
	    fcmToken: req.body.fcmToken,
	    unique: req.body.unique,
	    appversion: req.body.appversion,
        updated_at: new Date().getTime(),
        gmt: req.body.gmt,
        };

       User.findByIdAndUpdate(_id, {$set:{d}}, function(error,resu){
            if (error) throw error;
          //  res.status(200).send(resu);
        });   
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function updateAdmin(){
  Admin.findOne({aid: "b"}).exec(function (error, result) {
          if (!result){ 
            console.log(error)}
          else{
          //

             result.active_int = 0,
             result.active_str = "0",

            result.save(function (error, result) {
                  if (error) {
                      console.error(error);
                      throw error;
                  }
                  else{
                   //console.log("Saved"); 
                  }
                  //res.status(200).send(result);
              });

          }

      });
}

function updateCountUser(uid){
  User.findOne({userid: uid}).exec(function (error, result) {
          if (!result){ 
            console.log(error)}
          else{
          //

             result.chat_int = 0;
             //result.chat_str = "0";
             result.read = true;

            result.save(function (error, result) {
                  if (error) {
                      console.error(error);
                      throw error;
                  }
                  else{
                   //console.log("Saved"); 
                  }
                  //res.status(200).send(result);
              });

          }

      });
}

function getUser(uid, callback){
      var u = {};
      User.findOne({userid: uid}).exec(function (error, result) {
        if (result)
        {
            
        u = result;
        //console.log(u);
        callback(u);
            
        }
        else
        {

        u.error = {error: "user not found"};
        callback(u);
           
        }

    });
}


router.get('/xxxxxxxxxx', function(req, res, next) {
User.remove({userid: "I059PMnOql"}, function(error, doc){
if(!error){
res.status(200).send("Deleted");
}
else{
 res.status(200).send("Not deleted"); 
}
});
});


router.get('/', function(req, res, next) {
  updateAdmin();
  var uid = req.query.id;
  var items = {};
    var da = [
      {time: "2  min", msg:"Sample User message", admin: false},
      {time: "5 min", msg:"Sample Admin message", admin: true},
      {time: "9 min", msg:"Sample User message", admin: false},
      {time: "30 sec", msg:"Sample User message", admin: false},
      {time: "50 sec", msg:"Sample User message", admin: false},
      ];

   var ua = {
    phone: "Sample phone",
    useid: "Sample useid", 
    loc: "Sample Location",
    firstname: "Sample Firstname",
    lastname: "Sample Lastname",
    fcmToken: "Sample fcmToken",
    un: "Sample username", 
    unix_time: "" + new Date().getTime(),
   };   

   if(uid!=undefined && !_.isEmpty(uid)){
       // Grap chats
       let chatsU = CB.syncGetManyLean(Chat, {userid: uid},{created_at: -1});
       if(!_.isEmpty(chatsU)){
           // Chats not empty
           items.user = chatsU;
       }
       else{
           //TODO fallback to default
           items.user = da;
       }

       //({firstname: {$ne: ""}}).sort({chat_at: 1})
       let allConvo = CB.syncGetManyLean(User, {firstname: {$ne: ""}}, {chat_at: 1});

       if(!_.isEmpty(allConvo)){
           items.allchats = _.map(allConvo,_.partialRight(_.pick, ['userid','phone','username','firstname','lastname','unix_time','loc','gmt', 'chat_int', 'chat_str', 'chat_at']));
       }
       else{
           // TODO Failback to drak shit
           items.allchats = [];
       }

       let ogU = CB.syncGetOne(User, {"userid": uid});
       if(!_.isEmpty(ogU)){
           items.og = ogU;

           //Check username
           let un = "";
           if(ogU.firstname != "" && ogU.firstname != undefined){
               un = ogU.firstname + " " + ogU.lastname;
           }
           else{
               un = ogU.userid;
           }

           items.og.un = un;
       }
       else{
           //TODO Fallback to shit
           items.og = ua;
       }

       // Sedn the the shit
       res.render('chat/chat', {items:JSON.stringify(items)});
   }
   else{
       // user not defined just send the convo and dummy data
       let allConvo = CB.syncGetManyLean(User, {firstname: {$ne: ""}}, {chat_at: 1});
       items.user = da;
       items.allchats = _.map(allConvo,_.partialRight(_.pick, ['userid','phone','username','firstname','lastname','unix_time','loc','gmt', 'chat_int', 'chat_str', 'chat_at']));


       items.allchats.sort(function (a, b) {
           return a.read - b.read || parseInt(a.unix_time) - parseInt(b.unix_time);
       });

       items.og = {};
       items.og = ua;
       //console.log(items.og);
       var fff = JSON.stringify(items);
       res.render('chat/chat',{items: fff});
   }

});



module.exports = router;
