var express = require('express');
var _ = require('lodash');
var router = express.Router();

var mailgun = require('mailgun-js')({apiKey: "key-221954a88b1022ec333cc775908f360a", domain: "mail.vugapay.com"});
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var wellcome_templateDir = path.join(__dirname, 'email_templates', 'wellcome');
var wellcome_email = new EmailTemplate(wellcome_templateDir);

var mongoose = require('mongoose'),
   Chat = mongoose.model('Chat'),
   User = mongoose.model('User'),
   Dayto = mongoose.model('Dayto');

function ax(number) {
    // 2 decimal places => 100, 3 => 1000, etc
    //number = parseInt(parseInt(number)/60);
    var decPlaces = 0;
    decPlaces = Math.pow(10,decPlaces);

    // Enumerate number abbreviations
    var abbrev = [ "k", "m", "b", "t" ];

    // Go through the array backwards, so we do the largest first
    for (var i=abbrev.length-1; i>=0; i--) {

        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10,(i+1)*3);

        // If the number is bigger or equal do the abbreviation
        if(size <= number) {
             // Here, we multiply by decPlaces, round, and then divide by decPlaces.
             // This gives us nice rounding to a particular decimal place.
             number = Math.round(number*decPlaces/size)/decPlaces;

             // Handle special case where we round up to the next abbreviation
             if((number == 1000) && (i < abbrev.length - 1)) {
                 number = 1;
                 i++;
             }

             // Add the letter for the abbreviation
             number += abbrev[i];

             // We are done... stop
             break;
        }
    }

    return number;
}

function aM(n){
	n = parseInt(parseInt(n)/60);
    d = 2;
	x=(''+n).length,p=Math.pow,d=p(10,d);
	x-=x%3;return Math.round(n*d/p(10,x))/d+" kMGTPE"[x/3]
}

function a(n){
    d = 0;
	x=(''+n).length,p=Math.pow,d=p(10,d);
	x-=x%3;return Math.round(n*d/p(10,x))/d+" kMGTPE"[x/3]
}



router.get('/', function(req, res) {
Dayto.findOne({typo: "t"}).exec(function (error, result) {

var data = {};
    if(error){
      // New DayTo
  }

  else {
    // Found DayTo
    data = {

    all_count_int: aM(result.all_count_int),
    all_count_str: result.all_count_str,

    user_new_count_int: ax(result.user_new_count_int),
    user_new_count_str: result.user_new_count_str,

    user_active_count_int: ax(result.user_active_count_int),
    user_active_count_str: result.user_active_count_str,

    video_count_int: aM(result.video_count_int),
    video_count_str: result.video_count_str,

    videogrp_count_int: aM(result.videogrp_count_int),
    videogrp_count_str: result.videogrp_count_str,

    tv_count_int: aM(result.tv_count_int),
    tv_count_str: result.tv_count_str,

    radio_count_int: aM(result.radio_count_int),
    radio_count_str: result.radio_count_str,

    music_count_int: aM(result.music_count_int),
    music_count_str: result.music_count_str,

    artist_count_int: aM(result.artist_count_int),
    artist_count_str: result.artist_count_str,

    web_count_int: aM(result.web_count_int),
    web_count_str: result.web_count_str,

    other_count_int: aM(result.other_count_int),
    other_count_str: result.other_count_str,
    };

    Admin.findOne({aid: "b"}).exec(function (error, resu) {

    if(error){
    	// Admin Error
      data.count_msg = 0;
      res.render('dash/dash', {"data":JSON.stringify(data)});
    }
    else{
      // Admin Okay
      data.count_msg = resu.active_int;
      res.render('dash/dash', {"data":JSON.stringify(data)});

    }

     }); 




    
  }
}); 


	
});


module.exports = router;