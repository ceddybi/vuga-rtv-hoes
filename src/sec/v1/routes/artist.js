
var express = require('express');
var router = express.Router();
var upl = require('./upload.js');

var multer  = require('multer');
var upload = multer({
    dest: __dirname + '/../public/uploads/'
});
var randtoken = require('rand-token');


function rand() {
    return randtoken.generate(10); 
  };

var fs = require('fs');
var md5 = require('md5');

var mongoose = require('mongoose'),
   Artist = mongoose.model('Artist');

    var d = new Date();
    var currentTime = d.getTime();
    var image_path = "";
    var video_path = "";

/* GET All videos listing. */
router.get('/', function(req, res, next) {
   Artist.find({}).sort({created_at: -1}).exec(function (error, result) {
        if (error){ throw error;}
        {
        res.render('artist/index', {items: JSON.stringify(result)});
         
           }
    });
});

router.get('/create', function(req, res, next) {
    res.render('artist/form', { title: 'Create Artist' });
});

router.post('/create', upload.any(), function (req, res, next) {



    var vg = new Artist({


    aid: rand(),
    title: req.body.title,
    description: req.body.description,
    typo: "artist",
    meta: req.body.meta,
    int_date: currentTime,
    updated_at: 0,
 

    lock: req.body.lock,
    lockCode: req.body.lockCode,
    youtubeChannel: req.body.youtubeChannel,
    created_at: d,
    available: req.body.available,
 

    });
    
    req.files.forEach(function (value, index) {
        if (value.fieldname == 'image_file') {
            var splittedName = value.originalname.split('.');
            var image_name = req.body.title + '_image_' + currentTime + '.' + splittedName[splittedName.length - 1];
            image_path = __dirname + '/../public/data/images/' + image_name;
            fs.rename(value.path, image_path);
            vg.image_path = "https://s3.amazonaws.com/rtv01/images/"+image_name.replace(/ /g,"%20");
            upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');


        } else if (value.fieldname == 'video_file') {
            var splittedName = value.originalname.split('.');
            var video_name = req.body.title + '_video_'+currentTime + '.' + splittedName[splittedName.length - 1];
            video_path = __dirname + '/../public/data/videos/' + video_name;
            //data.video_path = '/data/videos/' + video_name;
             vg.video_path = "https://s3.amazonaws.com/rtv01/videos/" + video_name.replace(/ /g,"%20");
             fs.rename(value.path, video_path);           
            upl.saveFile(video_path, video_name, splittedName[splittedName.length - 1], 'video');

      



        }

    });




    vg.save(function (error, result) {
        if (error) {
            console.error(error);
            throw error;
        }
    });

    res.redirect('/artist');
});

router.get('/:id/delete',function(req, res, next){
   Artist.findOneAndRemove({_id:req.params.id}, function (error, item) {
        if (error){ throw error;}
        else{
            //console.log(item);
        res.redirect('/artist');}
    })
});

router.get('/:id/edit',function(req, res, next){
   Artist.findById(req.params.id, function (error, item) {
        if (error) throw error;
        res.render('artist/edit', {item: JSON.stringify(item) });
    })
});

router.post('/:id/edit', upload.any(),  function (req, res, next) {
    var d = new Date();
    var currentTime = d.getTime();

    var data = {
    title: req.body.title,
    description: req.body.description,
    meta: req.body.meta,
    featured: req.body.featured,
    updated_at: currentTime,

    lock: req.body.lock,
    lockCode: req.lockCode,
    youtubeChannel: req.body.youtubeChannel,
    available: req.body.available,

    };

    req.files.forEach(function (value, index) {
        if (value.fieldname == 'image_file') {
            var splittedName = value.originalname.split('.');
            var image_name = req.body.title + '_image_' + currentTime + '.' + splittedName[splittedName.length - 1];
            image_path = __dirname + '/../public/data/images/' + image_name;
            fs.rename(value.path, image_path);
            data.image_path = "https://s3.amazonaws.com/rtv01/images/"+image_name.replace(/ /g,"%20");
            upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');


        } else if (value.fieldname == 'video_file') {
            var splittedName = value.originalname.split('.');
            var video_name = req.body.title + '_video_'+currentTime + '.' + splittedName[splittedName.length - 1];
            video_path = __dirname + '/../public/data/videos/' + video_name;
            //data.video_path = '/data/videos/' + video_name;
             data.video_path = "https://s3.amazonaws.com/rtv01/videos/" + video_name.replace(/ /g,"%20");
            fs.rename(value.path, video_path);           
            upl.saveFile(video_path, video_name, splittedName[splittedName.length - 1], 'video');

      



        }

    });


   Artist.findByIdAndUpdate(req.params.id, data, function(error,result){
        if (error) throw error;
        res.redirect('/artist');
    });

});


module.exports = router;
