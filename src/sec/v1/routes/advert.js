const express = require('express');

const router = express.Router();
const upl = require('./upload.js');

const multer = require('multer');

const upload = multer({
  dest: `${__dirname}/../public/uploads/`,
});
const randtoken = require('rand-token');

function rand() {
  return randtoken.generate(10);
}

const fs = require('fs');
const md5 = require('md5');

let mongoose = require('mongoose'),
  Ads = mongoose.model('Ads');

const d = new Date();
const currentTime = d.getTime();
let image_path = '';
let video_path = '';

/* GET All videos listing. */
router.get('/', (req, res, next) => {
  Ads.find({})
    .sort({ created_at: -1 })
    .limit(20)
    .exec((error, result) => {
      if (error) {
        throw error;
      }
      {
        res.render('advert/index', { items: result });
      }
    });
});

router.get('/create', (req, res, next) => {
  res.render('advert/form', { title: 'Adverts' });
});

router.post('/create', upload.any(), (req, res, next) => {
  const vg = new Ads({
    adsid: rand(),
    title: req.body.title,
    description: req.body.description,
    typo: 'ad',
    meta: req.body.meta,
    timeout: req.body.timeout,
    int_date: currentTime,
    updated_at: 0,
    link: req.body.link,
    isVideo: req.body.isVideo,

    isYoutube: req.body.isYoutube,
    youtubeUrl: req.body.youtubeUrl,
    created_at: d,
    available: req.body.available,
  });

  req.files.forEach((value, index) => {
    if (value.fieldname == 'image_file') {
      var splittedName = value.originalname.split('.');
      const image_name = `${req.body.title}_image_${currentTime}.${
        splittedName[splittedName.length - 1]
      }`;
      image_path = `${__dirname}/../public/data/images/${image_name}`;
      fs.rename(value.path, image_path);
      vg.image_path = `https://s3.amazonaws.com/rtv01/images/${image_name.replace(/ /g, '%20')}`;
      upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');
    } else if (value.fieldname == 'video_file') {
      var splittedName = value.originalname.split('.');
      const video_name = `${req.body.title}_video_${currentTime}.${
        splittedName[splittedName.length - 1]
      }`;
      video_path = `${__dirname}/../public/data/videos/${video_name}`;
      // data.video_path = '/data/videos/' + video_name;
      vg.video_path = `https://s3.amazonaws.com/rtv01/videos/${video_name.replace(/ /g, '%20')}`;
      fs.rename(value.path, video_path);
      upl.saveFile(video_path, video_name, splittedName[splittedName.length - 1], 'video');
    }
  });

  vg.save((error, result) => {
    if (error) {
      console.error(error);
      throw error;
    }
  });

  res.redirect('/advert');
});

router.get('/:id/delete', (req, res, next) => {
  Ads.findOneAndRemove({ _id: req.params.id }, (error, item) => {
    if (error) {
      throw error;
    } else {
      // console.log(item);
      res.redirect('/advert');
    }
  });
});

router.get('/:id/edit', (req, res, next) => {
  Ads.findById(req.params.id, (error, item) => {
    if (error) throw error;
    res.render('advert/edit', { item: JSON.stringify(item) });
  });
});

router.post('/:id/edit', upload.any(), (req, res, next) => {
  const d = new Date();
  const currentTime = d.getTime();

  const data = {
    title: req.body.title,
    description: req.body.description,
    meta: req.body.meta,
    featured: req.body.featured,
    timeout: req.body.timeout,
    updated_at: currentTime,
    link: req.body.link,
    isVideo: req.body.isVideo,

    isYoutube: req.body.isYoutube,
    youtubeUrl: req.body.youtubeUrl,
    available: req.body.available,
  };

  req.files.forEach((value, index) => {
    if (value.fieldname == 'image_file') {
      var splittedName = value.originalname.split('.');
      const image_name = `${req.body.title}_image_${currentTime}.${
        splittedName[splittedName.length - 1]
      }`;
      image_path = `${__dirname}/../public/data/images/${image_name}`;
      fs.rename(value.path, image_path);
      data.image_path = `https://s3.amazonaws.com/rtv01/images/${image_name.replace(/ /g, '%20')}`;
      upl.saveFile(image_path, image_name, splittedName[splittedName.length - 1], 'image');
    } else if (value.fieldname == 'video_file') {
      var splittedName = value.originalname.split('.');
      const video_name = `${req.body.title}_video_${currentTime}.${
        splittedName[splittedName.length - 1]
      }`;
      video_path = `${__dirname}/../public/data/videos/${video_name}`;
      // data.video_path = '/data/videos/' + video_name;
      data.video_path = `https://s3.amazonaws.com/rtv01/videos/${video_name.replace(/ /g, '%20')}`;
      fs.rename(value.path, video_path);
      upl.saveFile(video_path, video_name, splittedName[splittedName.length - 1], 'video');
    }
  });

  Ads.findByIdAndUpdate(req.params.id, data, (error, result) => {
    if (error) throw error;
    res.redirect('/advert');
  });
});

module.exports = router;
