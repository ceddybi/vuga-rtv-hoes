var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.get('Authorization') !== undefined) {
    res.redirect('/video');
  }
  res.render('index');
});

module.exports = router;