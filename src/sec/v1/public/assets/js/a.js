var DURATION_IN_SECONDS = {
  epochs: ['year', 'month', 'day', 'hour', 'minute'],
  year:   31536000,
  month:  2592000,
  day:    86400,
  hour:   3600,
  minute: 60
};

function getDuration(seconds) {
  var epoch, interval;

  for (var i = 0; i < DURATION_IN_SECONDS.epochs.length; i++) {
    epoch = DURATION_IN_SECONDS.epochs[i];
    interval = Math.floor(seconds / DURATION_IN_SECONDS[epoch]);
    if (interval >= 1) {
      return { interval: interval, epoch: epoch };
    }
  }

};

/**function timeSince(date) {
  var seconds = Math.floor((new Date() - new Date(date)) / 1000);
  var duration = getDuration(seconds);
  var suffix  = (duration.interval > 1 || duration.interval === 0) ? 's' : '';
  return duration.interval + ' ' + duration.epoch + suffix;
}; **/

function timeSince(date) {

    var seconds = Math.floor(((new Date().getTime()- date) /1000 )),
    interval = Math.floor(seconds / 31536000);

    if (interval > 1) return interval + " yrs";

    interval = Math.floor(seconds / 2592000);
    if (interval > 1) return interval + " mon";

    interval = Math.floor(seconds / 86400);
    if (interval >= 1) return interval + " dys";

    interval = Math.floor(seconds / 3600);
    if (interval >= 1) return interval + " hrs";

    interval = Math.floor(seconds / 60);
    if (interval > 1) return interval + " min";

    return Math.floor(seconds) + " sec";
}



function compare(a,b) {
  if (a.created_at < b.created_at)
    return -1;
  if (a.created_at > b.created_at)
    return 1;
  return 0;
} 

function com(array) {
array.sort(function (a, b) {
return a.chat_int - b.chat_int;
});

}     


    


function sendMsg(ts, callback){
    var success = {};
    success.success = "Great";
    $.ajax(
    '/user/msg/send/admin', {
    type: 'POST',
    data: ts,
    statusCode: {
    200: function (response) {
    callback(success);
    },
    403: function (response) {
    callback(response);
    },
    406: function (response) {
    callback(response);
    },
    }, success: function (data, textStatus, jqXHR) {
    callback(success);


    },
    error: function (response) {
    callback(response);
    },
    });
}


function sendDate(ts, callback){
    $.ajax(
    '/user/dash/date', {
    type: 'POST',
    data: ts,
    statusCode: {
    200: function (response) {
    callback(response);
    },
    401: function (response) {
    callback(response);
    },
    406: function (response) {
    callback(response);
    },
    }, success: function (data, textStatus, jqXHR) {
    callback(data);

    },
    error: function (response) {
    callback(response);
    },
    });
}

function getUrlVars()
    {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
    }
    return vars;
  }