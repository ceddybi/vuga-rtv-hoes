// Bring Mongoose into the app
var mongoose = require( 'mongoose' );
var config = require('./config');

// Build the connection string
var dbURI = config.db['production'];

// Create the database connection
mongoose.connect(dbURI);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbURI);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});


// BRING IN YOUR SCHEMAS & MODELS
// For example
/** 
require('./api');
require('./advert');
require('./artist');
require('./music');
require('./radios');
require('./tv');
require('./video');
require('./videoGrp');
require('./user');
require('./push');
require('./chat');
require('./dayto');
require('./day');
require('./active');
require('./admin');
require('./sms');
**/

// For posts
require('./mods/mod-post')
require('./mods/mod-post-view')
require('./mods/mod-post-share')
require('./mods/mod-post-like')
require('./mods/mod-post-view')
require('./mods/mod-post-comment')
require('./mods/media/mod-film')


// For users
require('./mods/mod-user-profile')

