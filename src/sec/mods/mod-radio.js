var mongoose = require( 'mongoose' );

var RadioSchema = new mongoose.Schema({
    rid: {type: String, default: ""},
    title: {type: String, default: ""},
    description: {type: String, default: ""},
    meta: {type: String, default: ""},
    typo: {type: String, default: ""},

    //target category
    countryCode: {type: String, default: ""},

    //isWebView
     isWebView: {type: Boolean, default: false},
     
    veri: {type: Number, default: 1},
    featured: {type: Number, default: 1},
    int_date: {type: Number, default: 0},
    updated_at: {type: Number, default: 0},
    link: {type: String, default: ""},
    lock: {type: String, default: ""},
    lockCode: {type: String, default: ""},
    youtubeChannel: {type: String, default: ""},
    image_path: {type: String, default: ""},
    created_at: {type: Date, default: Date.now},
    available: {type: Boolean, default: true},
    

});

var radio = mongoose.model('Radio', RadioSchema);
radio.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = radio;