var mongoose = require( 'mongoose' );
var randtoken = require('rand-token');

function rand() {
    return randtoken.generate(20); 
  };

var chatSchema = new mongoose.Schema({
    chatid: {type: String, default: rand()},
    userid: {type: String, default: ""},
    username: {type: String, default: ""},
    firstname : {type: String, default: ""},
    lastname : {type: String, default: ""},
    phone : {type: String, default: ""},
    unique: {type: String, default: ""},
    device: {type: String, default: ""},
    meta : {type: String, default: ""},
    msg : {type: String, default: ""},
    title : {type: String, default: ""},
    all: {type: Boolean, default: true},
    created_at: {type: Date, default: Date.now},
    typo : {type: String, default: ""},
    isAdmin : {type: Boolean, default: false},
    unix_time: {type: String, default: ""+ new Date().getTime()},
    timestamp: {type: Date, default: new Date},
    read : {type: Boolean, default: false},


    

});

var chat = mongoose.model('Chat', chatSchema);
chat.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

