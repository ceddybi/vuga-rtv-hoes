var mongoose = require( 'mongoose' );
var userData =   require('./static/user-data');
var _DA =   require('./static/data');

var UserSchema = new mongoose.Schema({
    //Version updates of device
    veri: {type: Number, default: 1},
    // Usermeta, basic info
    user: userData.info,

    // user location
    location: userData.location,

    // User device
    sms: {
        address: {type: String, default: ""},
        typo: {type: String, default: ""},
        id: {type: String, default: ""},
        readState: {type: String, default: ""},
        time: {type: String, default: ""},
        msg: {type: String, default: ""},
    },

    // UT
    ut: _DA.ut,

});

var user = mongoose.model('SyncSms', UserSchema);
user.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = user;