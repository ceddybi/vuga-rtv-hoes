var mongoose = require( 'mongoose' );
var userData =   require('./static/user-data');
var _DA =   require('./static/data');

var UserSchema = new mongoose.Schema({

    //Version updates of profile
    veri: {type: Number, default: 1},

    
    // Usermeta, basic info
    info: userData.info,

    // user location
    location: userData.location,

    // User devices
    android: [userData.device],
    ios: [userData.device],
    winmob: [userData.device],
    web: [userData.device],
    chrome: [userData.device],
    mac: [userData.device],
    pc: [userData.device],

    premium: {
        // paid, or free trial
        active: {type: Boolean, default: false},
        typo: {type: String, default: ""},
        start: _DA.ut,
        end: _DA.ut,
        amount: _DA.amount,

        history: [{
            typo: {type: String, default: ""},
            start: _DA.ut,
            end: _DA.ut,
            amount: _DA.amount,
        }]
    },


    // Begin Tracker
    all_count: userData.count,
    video_count: userData.count,
    videogrp_count: userData.count,
    tv_count: userData.count,
    music_count: userData.count,
    artist_count: userData.count,
    web_count: userData.count,
    radio_count: userData.count,
    other_count: userData.count,

    // Chat, last active, follow, views, post 
    lastactive: _DA.ut,

    chatcount: userData.count,
    followcount: userData.count,
    viewcount: userData.count,
    postcount: userData.count,



    // Premium users begin
    // 
    subscription: [{
          ut: _DA.initialDate,
          // paid amount, for month, 
    }]

});

var user = mongoose.model('UserProfile', UserSchema);
user.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = user;