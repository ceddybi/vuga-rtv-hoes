var mongoose = require( 'mongoose' );
var _DA =  require('./static/data');

var ViewSchema = new mongoose.Schema({

    meta: _DA.post_viewmeta,
    ut:  _DA.ut,
    user: _DA.user,
    update: [{
        ut: _DA.ut
    }]

});

var v = mongoose.model('Postview', ViewSchema);
v.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = v;