var mongoose = require( 'mongoose' );
var _DA =  require('./static/data');

var LikeSchema = new mongoose.Schema({

    meta: _DA.likemeta,
    ut:  _DA.ut,
    user: _DA.user,

});

var like = mongoose.model('Like', LikeSchema);
like.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = like;