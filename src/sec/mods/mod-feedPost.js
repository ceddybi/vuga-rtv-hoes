var mongoose = require( 'mongoose' );
var _DA =   require('./static/data');


var PostSchema = new mongoose.Schema({

     // Type of post wys, video, image, music, dato
    typo: {type: String, default: ""}, // wys => [blog/text/image], video, music, meta => [data/music/tv/video/artist/radio]

    // Post like, comment, share, null

    contain: {type: String, default: ""},

    // A list of people engaging with this
    friendid: [{type: String, default: ""}],

    attribute: _DA.postmeta,



    /**
     * 
     * TO send as normal post but with contain and friendid
     */
   


});

var post = mongoose.model('FeedPost', PostSchema);
post.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = post;
