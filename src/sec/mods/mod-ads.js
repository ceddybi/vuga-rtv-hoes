var mongoose = require( 'mongoose' );

var _DA = require('./static/data');

var chatSchema = new mongoose.Schema({

    aid: {type: String, default: ""},

    veri: {type: Number, default: 1},

    //INDEXING the bitch
    featured: {type: Number, default: 1},

    isFeatured: {type: Boolean, default: false},

    // pushback, admob, content,local, video, audio
    typo : {type: String, default: ""},

    // Country code
    cc: {type: String, default: "ALL"},

    title : {type: String, default: ""},
    des : {type: String, default: ""},

    // Slider tv, film, radio
    slider : {type: String, default: ""},


    admob: {

        isVideo : {type: Boolean, default: false},

    },

    // for content ads
    content: {
      typo : {type: String, default: ""},
      id : {type: String, default: ""},
      banner_url_rec: {type: String, default: ""},
      obj: {},
    },

    // for local ads
    local: {
      id : {type: String, default: ""},
      link: {type: String, default: ""},
      banner_url_rec: {type: String, default: ""},
      obj: {},

    },

    audio: {
      link: {type: String, default: ""},
      banner_url_rec: {type: String, default: ""},
      banner_url_sq: {type: String, default: ""},
      url: {type: String, default: ""},
    },

    video: {
      isAdmob: {type: Boolean, default: false},
      link: {type: String, default: ""},
      banner_url_rec: {type: String, default: ""},
      banner_url_sq: {type: String, default: ""},
      url: {type: String, default: ""},
    },

    premium: {
      video_url: {type: String, default: ""},
      audio_url: {type: String, default: ""},
      banner_url_rec: {type: String, default: ""},
      banner_url_sq: {type: String, default: ""},
    },

    pushback: {
        duration: {type: Number, default: 0},
        isAdmob: {type: Boolean, default: false},
        banner_top: {type: String, default: ""},
        banner_bottom: {type: String, default: ""},
        link: {type: String, default: ""},
    },




    views: _DA.count,
    clicks: _DA.count,
    clicks_user:[{

        user: _DA.user,
        ut: _DA.initialDate

    }],
    

    ut: _DA.initialDate,
    

    
       

});

var chat = mongoose.model('Ad', chatSchema);
chat.on('index', function (err) {
    if (err) {
        throw  err;
    }
});