var mongoose = require( 'mongoose' );
var _DA =  require('./static/data');

var ViewSchema = new mongoose.Schema({

    meta: _DA.post_sharemeta,
    ut:  _DA.ut,
    user: _DA.user,

});

var v = mongoose.model('Postshare', ViewSchema);
v.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = v;