

var exports = module.exports =  {

    device: {
        wlan: {type: String, default: ""},
        imei: {type: String, default: ""},
        osid: {type: String, default: ""},
        pushtoken: {type: String, default: ""},
        unique: {type: String, default: ""},
        appversion: {type: String, default: "13"},
        active: {
           last: {type: String, default: ""},
           logged: {type: String, default: ""}, 
        },
        extra: {}
    },

    meta: {
        //title, title_o, publisher Id,
        title: {type: String, default: ""},
        title_o: {type: String, default: ""},


        // Publisher ID
        pubid: {type: String, default: ""},
    },


    info: {

        // top level
        a: {type: String, default: ""},

        d: {type: Boolean, default: false},
        dd: {type: String, default: ""},

        // Init data
        isVerify: {type: String, default: ""},
        reg: {type: Date, default: new Date},
        reg_unix_int: {type: String, default: ""+ new Date().getTime()},
        reg_unix_int: {type: Number, default: new Date().getTime()},


        //basic info
        userid: {type: String, default: ""},
        username: {type: String, default: ""},
        firstname: {type: String, default: ""},
        lastname: {type: String, default: ""},
        sex: {type: String, default: ""},
        email: {type: String, default: ""},
        phone: {type: String, default: ""},
        image_path: {type: String, default: ""},

        gmt: {type: String, default: "no"},
        ethic: {type: String, default: ""},
        interest: [{type: String, default: ""}],

        // Birthday
        bday: {
             day: {type: String, default: ""},
             month: {type: String, default: ""},
             year: {type: String, default: ""},
        },

        
        
    },

    // location
    location: {
            // GEO
            longitude: {type: Number, default: 0},
            latitude: {type: Number, default: 0},

            // ADDRESS
            formattedAddress: {type: String, default: ""},
            streetName: {type: String, default: ""},
            city: {type: String, default: ""},
            country: {type: String, default: ""},
            countryCode: {type: String, default: ""},
            
            extra: {},
            administrativeLevels: {}

    },

    // Universal counter
    count: {
          cstr: {type: String, default: "0"},
          cnum: {type: Number, default: 0},
    },

}