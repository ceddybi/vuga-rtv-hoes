/** *This for the initial time data was created */
const initialDate = {
  d: { type: Date, default: Date.now },
  dstr: { type: String, default: `${new Date().getTime()}` },
  dnum: { type: Number, default: new Date().getTime() },
};

module.exports = {
  /** INITIAL DATE */

  initialDate,

  /** META DATA FOR Posts */

  postmeta: {
    pid: { type: String, default: '' },
    seo: { type: String, default: '' },
    private: { type: Boolean, default: false },
    veri: { type: Number, default: 1 },

    create_on: initialDate,
  },

  post_likemeta: {
    pid: { type: String, default: '' },
    unlike: { type: Boolean, default: false },
    veri: { type: Number, default: 1 },

    create_on: initialDate,
  },

  post_viewmeta: {
    pid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate,
  },

  post_sharemeta: {
    pid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate,
  },

  post_commentmeta: {
    pid: { type: String, default: '' },
    commentid: { type: String, default: '' },
    veri: { type: Number, default: 1 },
    create_on: initialDate,
  },

  /** META DATA FOR Profile */
  profile_viewmeta: {
    userid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate,
  },

  profile_followmeta: {
    userid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate,
  },

  /** DATE & COUNTER & AMOUNT */
  ut: {
    dstr: { type: String, default: `${new Date().getTime()}` },
    dnum: { type: Number, default: new Date().getTime() },
  },

  count: {
    cstr: { type: String, default: '0' },
    cnum: { type: Number, default: 0 },
  },

  amount: {
    cur: { type: String, default: '0' },
    cstr: { type: String, default: '0' },
    cnum: { type: Number, default: 0 },
  },

  /** UNIVERSAL USER */

  user: {
    userid: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
    username: { type: String, default: '' },
    lastseen: { type: String, default: '' },
    a: { type: Boolean, default: false },
  },

  /** BEGIN FOR TYPO DATA */

  text: {
    title: { type: String, default: '' },
    text: { type: String, default: '' },
    raw: { type: String, default: '' },
    veri: { type: Number, default: 1 },
  },

  video: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' },
  },

  image: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' },
  },

  audio: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' },
  },

  dato: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [''],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' },
  },

  wys: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' },
  },
};
