var _DA = require('./data');

    var filmimage = {

        // For Movie Viewer
        cover: {type: String, default: ""},

        //item logo
        logo: {type: String, default: ""},

        // For list views
        banner: {type: String, default: ""},
        
        // For slider
        ad: {type: String, default: ""},

    };

    // For Movie Tracking of time viewed
    var filmlength = {
        time_total: {type: Number, default: 0},
        time_watched: {type: Number, default: 0},
        watched: {type: Boolean, default: false},
    };

var exports = module.exports =  {
    filmimage: filmimage,
    filmlength: filmlength,

    filmmeta: {
        
        lang: [""],
        isTranslated: {type: Boolean, default: false},
        // Two typos only, movie or serie
        typo: {type: String, default: ""},

        //Only if movie
        movie_url: {type: String, default: ""},

        // Normal data
        name: {type: String, default: ""},
        des: {type: String, default: ""},
        year: {type: String, default: ""},
        age_rating: {type: String, default: ""},
        dir: {type: String, default: ""},
        cast: {type: String, default: ""},

        // Category
        cat: {type: String, default: ""},

        // For matching
        // Values are other categories
        sub_cat: [""],

        // SEO search results and matching
        tags: [""],


        //Countries Available
        countries: [""],

        available: {type: Boolean, default: true}, 
    },



    // Rating counter
    filmrating: {
        good: [{
            userid: {type: String, default: ""},
            ut: _DA.ut
        }],

        bad: [{
            userid: {type: String, default: ""},
            ut: _DA.ut
        }],   
    },


    episode: {

        episode_position: {type: Number, default: 1},
        watched: {type: Boolean, default: false},
        title: {type: String, default: ""},
        des: {type: String, default: ""},
        url: {type: String, default: ""},
        image: filmimage,
        length: filmlength, 
    },


    season: {
        index_num: {type: Number, default: 1}, 
        watched: {type: Boolean, default: false}, 
    }




}