const mongoose = require('mongoose');
const randtoken = require('rand-token');

function rand() {
  return randtoken.generate(20);
}

const chatSchema = new mongoose.Schema({
  sid: { type: String, default: '' },
  typo: { type: String, default: '' },
  active: { type: Boolean, default: true },
  isAudio: { type: Boolean, default: true },
  isBanner: { type: Boolean, default: true },
  isVideo: { type: Boolean, default: true },
  //
  count: { type: Number, default: 0 },

  // Breaks between
  breaks: { type: Number, default: 0 },

  pushback: {
    active: { type: Boolean, default: true },

    // Number of ads
    count: { type: Number, default: 0 },

    // Time in seconds
    duration: { type: Number, default: 0 },

    // Time Between
    breaks: { type: Number, default: 0 },
  },
});

const chat = mongoose.model('AdSettings', chatSchema);
chat.on('index', err => {
  if (err) {
    throw err;
  }
});