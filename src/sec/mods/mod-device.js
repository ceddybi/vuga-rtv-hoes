const mongoose = require('mongoose');
const userData = require('./static/user-data');
const _DA = require('./static/data');

const UserSchema = new mongoose.Schema({
  // Version updates of device
  veri: { type: Number, default: 1 },

  // Android, ios, win, pc, chrome
  typo: { type: String, default: '' },

  // Usermeta, basic info
  user: userData.info,

  // user location
  location: userData.location,

  // User device
  info: {
    wlan: { type: String, default: '' },
    imei: { type: String, default: '' },
    osid: { type: String, default: '' },
    pushtoken: { type: String, default: '' },
    unique: { type: String, default: '' },
    appversion: { type: String, default: '13' },
    active: {
      last: { type: String, default: '' },
      logged: { type: String, default: '' },
    },
    extra: {},
  },

  // UT
  ut: _DA.ut,

  premium: {
    // paid, or free trial
    active: { type: Boolean, default: false },
    typo: { type: String, default: '' },
    start: _DA.ut,
    end: _DA.ut,
    amount: _DA.amount,

    history: [
      {
        typo: { type: String, default: '' },
        start: _DA.ut,
        end: _DA.ut,
        amount: _DA.amount,
      },
    ],
  },

  // Premium users begin
  //
  subscription: [
    {
      ut: _DA.initialDate,
      // paid amount, for month,
    },
  ],
});

const user = mongoose.model('UserDevice', UserSchema);
user.on('index', err => {
  if (err) {
    throw err;
  }
});

module.exports = user;
