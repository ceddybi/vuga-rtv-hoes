var mongoose = require( 'mongoose' );
var _DA =   require('./static/data');


var PostSchema = new mongoose.Schema({

     // Type of post wys, video, image, music, dato
    typo: {type: String, default: ""}, // wys => [blog/text/image], video, music, meta => [data/music/tv/video/artist/radio]

    // Post Meta, initial date, post id
    meta: _DA.postmeta,
   
    // Post owner
    user: _DA.user,

    // Post last active, by comment Activity
    active: _DA.ut,
 
    // Category of post
    cat: [""], // Tech, ict, life, blogging, e.t.c

   
    // Contents of the post, can either be one
    content: {
            wys:  _DA.wys,
            video:   _DA.video, 
            image: _DA.image,
            audio:   _DA.audio,
            dato:   _DA.dato,
    },

    
    // Comments dynamic reference
    comment: {
              last: {
                       userid: {type: String, default: ""},
                       text: {type: String, default: ""},
                       ut: _DA.ut,    
              },

              count: _DA.count,

              // for app
              comment_data: [{}]
    },




    /// TODO 
    // Views dynamic reference
    view: {
              count: _DA.count,

              // for app
              view_data: [{}]
    },


    // Dynamic likes reference
    like: {
            count: _DA.count,
            // for app
            like_data: [{}]
    },


    // Dynamic likes reference
    share: {
            count: _DA.count,
            // for app
              share_data: [{}]
    },



});

var post = mongoose.model('Post', PostSchema);
post.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = post;
