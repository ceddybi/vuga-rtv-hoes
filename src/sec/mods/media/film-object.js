var _DMEDIA =  require('../static/media-data');
var _DA =  require('../static/data');

var rn = require('random-number');
var gen = rn.generator({
  min:  10
, max:  1000000
, integer: true
});

var exports = module.exports =  {

film: {



    // Basic data
    id_num: {type: Number, default: gen()},

    //Todo deleted data
    isDeleted: {type: Boolean, default: false},


    // Indexing
    featured: {type: Number, default: 1},

    // Version
    veri:  {type: Number, default: 1},

    meta: _DMEDIA.filmmeta,
    image: _DMEDIA.filmimage,
    rating: _DMEDIA.filmrating,
    
    // Basic info 
    // If serie
    serie: [{
            season: _DMEDIA.season,
            episodes: [_DMEDIA.episode]
        }],

    // if normal movie
    movie: {
        movie_url: {type: String, default: ""},
        url_large: {type: String, default: ""},
        url_medium:  {type: String, default: ""},
        url_small:  {type: String, default: ""},

        // Length used user
        length: _DMEDIA.filmlength,

    },    


    // Below are some other stuff for server
    plays: [{
        userid:  {type: String, default: ""},
        ut_array: [{ut: _DA.ut}]
    }] 

},

filmserie: {
    film_id: {type: String, default: ""},
    film_name: {type: String, default: ""},

    seasons:[{
                season_position: {type: Number, default: 0}, 
                watched: {type: Boolean, default: false},
                episodes: [_DMEDIA.episode]
           }],
},


myfilm: {
    film_id: {type: String, default: ""},
    num_id: {type: String, default: ""},

    typo: {type: String, default: ""},
    finished: {type: Boolean, default: false},

    movie: {
        // Length used user
        time_total: {type: Number, default: 0},
    }, 

    serie: {
        latest:{
            season_num: {type: Number, default: 0},
            episode_num: {type: Number, default: 0}
        },

        all:
        [{
            season: _DMEDIA.season,
            episodes: [_DMEDIA.episode]
        }]
    },
}

}