var mongoose = require( 'mongoose' );
var film =  require('./film-object');
        
var filmSchema = new mongoose.Schema(film.film);

var film = mongoose.model('Film', filmSchema);
film.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = film;