var mongoose = require( 'mongoose' );
var film =  require('./film-object');
var _DMEDIA =  require('../static/media-data');
        
var filmSchema = new mongoose.Schema({

    film_id: {type: String, default: ""},
    film_name: {type: String, default: ""},
    veri: {type: Number, default: 1}, 

    seasons:[{
                season_position: {type: Number, default: 0}, 
                watched: {type: Boolean, default: false},
                episodes: [_DMEDIA.episode]
           }]
});

var film = mongoose.model('FilmSerie', filmSchema);
film.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = film;