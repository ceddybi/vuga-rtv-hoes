var mongoose = require( 'mongoose' );

var _DA = require('./static/data');

var chatSchema = new mongoose.Schema({

    chatid: {type: String, default: ""},
    convoid: {type: String, default: ""},
    veri: {type: Number, default: 1},
    title : {type: String, default: ""},
    
    msg : {type: String, default: ""},
    sub : {type: String, default: ""},
    typo : {type: String, default: ""},
    
    read: {type: Boolean, default: true},
    sent: {type: Boolean, default: true},
    
    
    from: _DA.user,
    to: _DA.user,

    ut: _DA.initialDate,
    

    
       

});

var chat = mongoose.model('ChatItem', chatSchema);
chat.on('index', function (err) {
    if (err) {
        throw  err;
    }
});