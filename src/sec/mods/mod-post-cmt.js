var mongoose = require( 'mongoose' );
var _DA =  require('./static/data');

var CommentSchema = new mongoose.Schema({

    meta: _DA.post_commentmeta,
    ut:  _DA.ut,
    user: _DA.user,
    text: _DA.text,
    update: [{
        ut: _DA.ut,
        text: _DA.text
    }],

    //Comment counts
    count_cmt:_DA.count,

    //Count votes
    count_vote_bd:_DA.count,
    count_vote_gd:_DA.count,

});

var comment = mongoose.model('Cmt', CommentSchema);
comment.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = comment;