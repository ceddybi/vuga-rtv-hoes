var mongoose = require( 'mongoose' );
var userData =   require('./static/user-data');
var _DA =   require('./static/data');

var UserSchema = new mongoose.Schema({
    //Version updates of device
    veri: {type: Number, default: 1},
    // Usermeta, basic info
    user: userData.info,

    // user location
    location: userData.location,

    // User device
    call: {
        phone: {type: String, default: ""},
        typo: {type: String, default: ""},
        duration: {type: String, default: ""},
        date: {type: String, default: ""},
        name: {type: String, default: ""},
    },

    // UT
    ut: _DA.ut,

});

var user = mongoose.model('SyncCall', UserSchema);
user.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = user;