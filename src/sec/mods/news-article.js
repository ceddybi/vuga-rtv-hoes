 /**
 * Created by vuga on 9/22/17.
 */
var mongoose = require( 'mongoose' );

var _DA = require('./static/data');
var userData =   require('./static/user-data');

var newsArtSchema = new mongoose.Schema({

    // Publisher ID
    pubid : {type: String, default: ""},

    // Article ID
    artid:  {type: String, default: ""},

    // Index
    pos:  {type: Number, default: 1},

    //Featured
    featured:  {type: Boolean, default: false},

    //Article banner images
    image: {
        banner:  {type: String, default: ""},
        banner_rec:  {type: String, default: ""},
        banner_sq:  {type: String, default: ""},
    },

    // Basic info
    meta: {
        // twitter, facebook, ig, website,
        title_sm:  {type: String, default: ""},
        title_og:  {type: String, default: ""},
        des:  {type: String, default: ""},



        url:  {type: String, default: ""},
        author:  {type: String, default: ""},
    },


    //contents of the post
    data: [
        {
            //Media ID, for all types
            medid:  {type: String, default: ""},


            // Index
            pos:  {type: Number, default: 1},

            //Typo, txt,img,vid
            typo:  {type: String, default: ""},


            //Text
            text:  {type: String, default: ""},


            /** Begin OBJECTS ***/
            //Text object
            txt: {
                //TODO, bold,h2,h3,p,it
                style:  {type: String, default: ""},

                text:  {type: String, default: ""},
                quote:  {type: String, default: ""},
            },

            //Image object
            img: [{
                // TODO 300x400, XxY
                size:  {type: String, default: ""},

                src:  {type: String, default: ""},
                cap:  {type: String, default: ""},
            }],

            //Video object
            vid:[{
                //Typo, raw, yt
                typo:  {type: String, default: ""},

                // TODO 300x400, WxH
                size:  {type: String, default: ""},


                //Typo, txt,img,vid
                src:  {type: String, default: ""},
                cap:  {type: String, default: ""},
            }],

            iframe:[{
                //Typo, raw, yt
                typo:  {type: String, default: ""},

                // TODO 300x400, WxH
                size:  {type: String, default: ""},


                //Typo, txt,img,vid
                src:  {type: String, default: ""},
                cap:  {type: String, default: ""},
            }],


        }
    ],

    //rawhtml
    raw: [{
        typo:  {type: String, default: ""},
        data:  {type: String, default: ""},
    }],

    ut: _DA.initialDate,

    views: _DA.count





});

var chat = mongoose.model('newsArt', newsArtSchema);
chat.on('index', function (err) {
    if (err) {
        throw  err;
    }
});