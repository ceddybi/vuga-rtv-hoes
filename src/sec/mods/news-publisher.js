/**
 * Created by vuga on 9/22/17.
 */
const mongoose = require('mongoose');

const _DA = require('./static/data');
const userData = require('./static/user-data');

const newsPubSchema = new mongoose.Schema({
  // Publisher ID
  pubid: { type: String, default: '' },

  // Country Code
  countryCode: { type: String, default: '' },

  // Index
  pos: { type: Number, default: 1 },

  available: { type: Boolean, default: true },

  logo_white: { type: String, default: '' },
  logo_black: { type: String, default: '' },
  logo_color: { type: String, default: '' },

  // twitter, facebook, ig, website,
  name: { type: String, default: '' },
  slogan: { type: String, default: '' },
  des: { type: String, default: '' },
  website: { type: String, default: '' },
  username: { type: String, default: '' },
  xpath_latest: { type: String, default: '' },
  xpath_featured: { type: String, default: '' },
  xpath_des: { type: String, default: '' },
  xpath_author: { type: String, default: '' },
  xpath_fimg: { type: String, default: '' },

  // Social Media
  social: {
    tw: { type: String, default: '' },
    fb: { type: String, default: '' },
    ig: { type: String, default: '' },
    sn: { type: String, default: '' },
  },

  ut: _DA.initialDate,
});

const chat = mongoose.model('newsPub', newsPubSchema);
chat.on('index', err => {
  if (err) {
    throw err;
  }
});
