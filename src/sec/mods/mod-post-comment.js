var mongoose = require( 'mongoose' );
var _DA =  require('./static/data');

var CommentSchema = new mongoose.Schema({

    meta: _DA.post_commentmeta,
    ut:  _DA.ut,
    user: _DA.user,
    text: _DA.text,
    update: [{
        ut: _DA.ut,
        text: _DA.text
    }]

});

var comment = mongoose.model('Postcomment', CommentSchema);
comment.on('index', function (err) {
    if (err) {
        throw  err;
    }
});

module.exports = comment;