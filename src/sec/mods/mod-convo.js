var mongoose = require( 'mongoose' );
var _DA = require('./static/data');

var chatSchema = new mongoose.Schema({



    convoid: {type: String, default: ""},

    user_id     : [],
    allowed     : [],
    banned      : [],

    // Time updates
    ut: _DA.ut,

    // Last message
    last: {
        userid: {type: String, default: ""},
        msg: {type: String, default: ""},
    },
    
    creator: {
            userid: {type: String, default: ""},
            firstname: {type: String, default: ""},
            lastname: {type: String, default: ""},
            username: {type: String, default: ""},
            lastseen: {type: String, default: ""},
            image_path: {type: String, default: ""},
            a: {type: Boolean, default: false},
            obj: {},
            count: _DA.count
        },

    operator: {
            userid: {type: String, default: ""},
            firstname: {type: String, default: ""},
            lastname: {type: String, default: ""},
            username: {type: String, default: ""},
            lastseen: {type: String, default: ""},
            image_path: {type: String, default: ""},
            a: {type: Boolean, default: false},
            obj: {},
            count: _DA.count
        },

   
    
       

});

var chat = mongoose.model('Convo', chatSchema);
chat.on('index', function (err) {
    if (err) {
        throw  err;
    }
});