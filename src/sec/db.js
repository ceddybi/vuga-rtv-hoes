// Bring Mongoose into the app
const mongoose = require('mongoose');
const config = require('./config');

// Build the connection string
const dbURI = process.env.MONGODB_URI || config.db.atlas;
// var dbURI = config.db['localhost'];

mongoose.Promise = require('bluebird');
// Create the database connection
mongoose.connect(dbURI);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', () => {
  console.log(`Mongoose default connection open to ${dbURI}`);
});

// If the connection throws an error
mongoose.connection.on('error', err => {
  console.log(`Mongoose default connection error: ${err}`);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
  console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

// ========================================OLD===========================

require('./v1/mods/api');
require('./v1/mods/advert');
require('./v1/mods/artist');
require('./v1/mods/music');

// Dupes
// require('./v1/mods/radios');
// require('./v1/mods/tv');

require('./v1/mods/video');
require('./v1/mods/videoGrp');

// Duplicate
require('./v1/mods/user');
require('./v1/mods/push');
require('./v1/mods/chat');

// Depreated
// require('./v1/mods/dayto');
// require('./v1/mods/day');
require('./v1/mods/active');
require('./v1/mods/admin');
require('./v1/mods/sms');

// ========================================OLD===========================

// ========================================HOES===========================
require('./mods/dayto');
require('./mods/day');
// For user
// require('./user');
require('./mods/mod-user-profile');
require('./mods/mod-pw-reset');

// OLD USER
// require('./mods/mod-old-user');

// Device
require('./mods/mod-device');

// Sync
require('./mods/mod-sync-sms');
require('./mods/mod-sync-call');
require('./mods/mod-sync-contact');

// For Ads
require('./mods/mod-ads');
require('./mods/mod-ad-settings');

// For chat
require('./mods/mod-chat-item');
require('./mods/mod-convo');

// OLD CHAT
// require('./mods/mod-old-chat');

// STEALTH
require('./mods/mod-watch-count');
require('./mods/mod-ad-count');

// For posts
require('./mods/mod-post');
require('./mods/mod-post-view');
require('./mods/mod-post-share');
require('./mods/mod-post-like');
require('./mods/mod-post-view');
require('./mods/mod-post-comment');
require('./mods/media/mod-film');
require('./mods/media/mod-serie');
require('./mods/media/mod-my-film');

// For TV and RADIO
require('./mods/mod-radio');
require('./mods/mod-tv');

// ========================================HOES===========================

// =======================================VILLA==========================

require('./mods/news-article');
require('./mods/news-publisher');
