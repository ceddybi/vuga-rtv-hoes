// only ES5 is allowed in this file
require("babel/register");

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const db = require('./src/sec/db');
const utils = require('./src/sec/auth');
const config = require('./src/sec/config');
const debug = require('debug')('server');
const http = require('http');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, './src/sec/v1/views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, './src/sec/v1/public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, './src/sec/v1/public')));

app.options('/*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization, Content-Length, X-Requested-With'
  );
  // res.send(200);
  res.status(200).json({});
});

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization, Content-Length, X-Requested-With'
  );

  // intercepts OPTIONS method
  if (req.method === 'OPTIONS') {
    // respond with 200
    // res.send(200);
    res.status(200).json({});
  } else {
    // move on
    next();
  }
});

// Newer routers for app
const apiPOST = require('./src/sec/api/post/post-index');
const apiUser = require('./src/sec/api/user/user-index');

// app.use('/v2/post', apiPOST);
// app.use('/v2/user', apiUser);

// v2 App
const appAPI = require('./src/sec/api/appAPI');

app.use('/v2', appAPI);

// For Admin 2 UI, content
const adminAPI = require('./src/sec/api/adminAPI');

app.use('/v2admin', adminAPI);

// For Admin V2
const adminV2 = require('./src/sec/api/adminV2');

app.use('/v2/admin', adminV2);

// OLD ROUTES
const Oindex = require('./src/sec/v1/routes/index');
const Oapi = require('./src/sec/v1/routes/api');
const Oads = require('./src/sec/v1/routes/advert');
const Oartist = require('./src/sec/v1/routes/artist');
const Omusic = require('./src/sec/v1/routes/music');
const Oradios = require('./src/sec/v1/routes/radios');
const Otv = require('./src/sec/v1/routes/tv');
const Ovideo = require('./src/sec/v1/routes/video');
const OvideoGrp = require('./src/sec/v1/routes/videoGrp');
const Ouser = require('./src/sec/v1/routes/user');
const Opush = require('./src/sec/v1/routes/push');
const Odash = require('./src/sec/v1/routes/dash');
const Ochat = require('./src/sec/v1/routes/chat');

// Routes
app.use('/v1', Oapi);
app.use('/user', Ouser);
app.use('/push', Opush);
app.use('/', Oindex, utils.basicAuth(config.user.username, config.user.password));
app.use('/advert', Oads, utils.basicAuth(config.user.username, config.user.password));
app.use('/artist', Oartist, utils.basicAuth(config.user.username, config.user.password));
app.use('/music', Omusic, utils.basicAuth(config.user.username, config.user.password));
app.use('/radios', Oradios, utils.basicAuth(config.user.username, config.user.password));
app.use('/tv', Otv, utils.basicAuth(config.user.username, config.user.password));
app.use('/video', Ovideo, utils.basicAuth(config.user.username, config.user.password));
app.use('/videoGrp', OvideoGrp, utils.basicAuth(config.user.username, config.user.password));
// app.use('/push', Opush,  utils.basicAuth(config.user['username'], config.user['password']));
app.use('/chat', Ochat, utils.basicAuth(config.user.username, config.user.password));
app.use('/dash', Odash, utils.basicAuth(config.user.username, config.user.password));

// OLD END==============================

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
/** 
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
* */

if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    console.log(err);
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: err,
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  console.log(err);
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
  });
});

const port = normalizePort(process.env.PORT || '3050');
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${  port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${  addr}` : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
}
