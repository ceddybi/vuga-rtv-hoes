FROM node:9.4.0
WORKDIR /opt/vtv
ENV PORT=3050
EXPOSE 3050
COPY . /opt/vtv
RUN rm -rf node_modules && npm i
CMD node index
